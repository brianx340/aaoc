# Api Aaoc



## Montar el servidor

Para correr el servidor debe primero que nada copiar el archivo .env.example a un nuevo .env

Instalamos tambien Babel
```
npm install --save-dev @babel/cli @babel/core @babel/preset-env
```

```
LOCAL_MONGO         --- Si la colocamos en "true" debemos proveer la uri local
MONGO_URI_LOCAL     --- Aquí la uri local por defecto "mongodb://localhost:27017/DB_NAME"
MONGO_URI_CLOUD     --- Aquí la uri remota, solo si LOCAL_MONGO = false
DB_NAME             --- El nombre de la DB a la que queremos conectarnos
PUBLIC_DATA_URL     --- URL del almacenamiento de los archivos públicos
DEV_MODE            --- Poner en true al momento de desarrollar implementaciones, este evita el envio de email, eliminacion de ciertos archivos entre otros
JWT_TOKEN_SECRET    --- Clave que JWT utilizara para encriptar los tokens
```

Bien ya tenemos todo listo, vamos al directorio donde queremos almacenar nuestro proyecto y hacemos (Si no tienes llaves ssh puedes revisar [Configurar llaves ssh](https://www.notion.so/Configurar-llaves-SSH-f0b331af74ac491dab58f2af2a5b3c8e))
```
git clone git@gitlab.com:brian-dev/apiaaoc.git
cd apiaaoc
npm i
npm start
```
Listo! ya tienes el backend corriendo!