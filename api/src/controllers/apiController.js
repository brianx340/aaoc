const { DynamicDatas, ServerConfiguration, Roles } = require('../models');
const { ActivitiesModes, ActivitiesStatuses, ActivitiesTypes } = require('../models/activities');
const { Countries, Genres } = require('../models/commons');

module.exports = {
    getCountries: async (_, res) => {
        try {
            const countries = ( await Countries.find({}) )
            return res.status(200).json({ status: 'success', data: { countries } })
        }
        catch(e){
            console.error(e)
            return res.json({ 'status': 'error', 'message': 'Error en la solicitud.', 'errorCode': 'API_001' })
        }
    },
    deleteCountry: async (req, res) => {
        try {
            const { countryId } = req.body
            if(!countryId){
                throw new Error('No se ha recibido el id del país.')
            }
            await Countries.findOneAndDelete({_id:countryId})
            return res.status(200).json({ status: 'success' })
        }
        catch(e){
            console.error(e)
            return res.json({ 'status': 'error', 'message': e.message, 'errorCode': 'API_002' })
        }
    },
    getSubscriptionData: async (req, res) => {
        try {
            let paymentInfo;
            let { user } = req;
            const actualSubscription = (await ServerConfiguration.findOne({ name: 'aaoc' }).populate('actualSubscription')).actualSubscription
            let data = {
                price: actualSubscription.preferences.auto_recurring.transaction_amount,
                moneyType: actualSubscription.preferences.auto_recurring.currency_id,
                paymentInfo: null
            }
            
            if(user.role.type !== 'paidUser'){
                await user.paymentChecker()
                paymentInfo = await user.getPaymentInfo()
                data.paymentInfo = paymentInfo
            }

            return res.status(200).json({ status: 'success', data })
        }
        catch(e){
            console.error(e)
            return res.json({ 'status': 'error', 'message': 'Error en la solicitud.', 'errorCode': 'API_003' })
        }
    },
    getActivityTypes: async (_, res) => {
        try{
            let activityTypes = await ActivitiesTypes.find({})
            return res.status(200).json({ status: 'success', data: { activityTypes } })
        }
        catch(e){
            console.error(e)
            return res.json({ 'status': 'error', 'message': 'Error en la solicitud.', 'errorCode': 'API_004' })
        }
    },
    getActivityModes: async (_, res) => {
        try{
            let activityModes = await ActivitiesModes.find({})
            return res.status(200).json({ status: 'success', data: { activityModes } })
        }
        catch(e){
            console.error(e)
            return res.json({ 'status': 'error', 'message': 'Error en la solicitud.', 'errorCode': 'API_005' })
        }
    },
    getActivityStatuses: async (_, res) => {
        try{
            let activityStatuses = await ActivitiesStatuses.find({})
            return res.status(200).json({ status: 'success', data: { activityStatuses } })
        }
        catch(e){
            console.error(e)
            return res.json({ 'status': 'error', 'message': 'Error en la solicitud.', 'errorCode': 'API_006' })
        }
    },
    getRoles: async (_, res) => {
        try{
            let roles = await Roles.find({ type: { $ne: 'admin' } })
            return res.status(200).json({ status: 'success', data: { roles } })
        }
        catch(e){
            console.error(e)
            return res.json({ 'status': 'error', 'message': 'Error en la solicitud.', 'errorCode': 'API_007' })
        }
    },
    getGenres: async (_, res) => {
        try {
            const genres = ( await Genres.find({}) )
            return res.status(200).json({ status: 'success', data: { genres } })
        }
        catch(e){
            console.error(e)
            return res.json({ 'status': 'error', 'message': 'Error en la solicitud.', 'errorCode': 'API_008' })
        }
    },
}