const { getActivitiesViewData, getNextActivities, getNewsData } = require('../database/functions');
const { Users, ServerConfiguration, Activities } = require('../models');

module.exports = {
    getFeaturedActivityProcess: async (req, res) => {
        try {
            const featuredActivity = await ServerConfiguration.getFeaturedActivity();
            return res.status(200).json({ status: 'success', featuredActivity })
        }
        catch(e){
            console.error(e)
            return res.json({ 'status': 'error', 'message': 'Error en la solicitud.', 'errorCode': 'HOME_001' })
        }
    },
    suscribeToActivity: async (req, res) => {
        try {
            const userId = req.user._id
            const { activityId } = req.body
            if(!activityId){
                throw new Error('No se ha recibido el id de la actividad.')
            }
            let user = await Users.findById(userId)
            if(!user){
                throw new Error('El usuario no existe.')
            }
            await user.suscribeToActivity(activityId)
            return res.status(200).json({ status: 'success' })
        }
        catch(e){
            console.error(e)
            return res.json({ 'status': 'error', 'message': e.message, 'errorCode': 'SUSCRIBE_001' })
        }
    },
    getNextActivitiesProcess: async (req, res) => {
        try {
            const nextActivities = await getNextActivities(req.user)
            return res.status(200).json({ status: 'success', nextActivities })
        }
        catch(e){
            console.error(e)
            return res.json({ 'status': 'error', 'message': 'Error en la solicitud.', 'errorCode': 'HOME_001' })
        }
    },
    getNewsDataProcess: async (req, res) => {
        try {
            const newsData = await getNewsData()
            return res.status(200).json({ status: 'success', newsData })
        }
        catch(e){
            console.error(e)
            return res.json({ 'status': 'error', 'message': 'Error en la solicitud.', 'errorCode': 'USER_002' })
        }
    },
    getUser: async (req, res) => {
        try {
            const user = req.user
            await user.paymentChecker()
            let actualUser = (await Users.findById(user._id))
            return res.status(200).json({ status: 'success', user:actualUser })
        }
        catch(e){
            console.error(e)
            return res.json({ 'status': 'error', 'message': 'Error en la solicitud.', 'errorCode': 'USER_003' })
        }
    },
    getUserActivities: async (req, res) => {
        try {
            const user = req.user
            const userAactivities = await user.getActivities()
            return res.status(200).json({ status: 'success', userAactivities })
        }
        catch(e){
            console.error(e)
            return res.json({ 'status': 'error', 'message': 'Error en la solicitud.', 'errorCode': 'USER_004' })
        }
    },
    getUserActivitiesIsNot: async (req, res) => {
        try {
            const user = req.user
            const otherActivities = await Activities.getUserActivitiesIsNot(user)
            return res.status(200).json({ status: 'success', otherActivities })
        }
        catch(e){
            console.error(e)
            return res.json({ 'status': 'error', 'message': 'Error en la solicitud.', 'errorCode': 'USER_005' })
        }
    }
}