const { validationResult } = require('express-validator')
const {
        getNewsData,
        putBanner,
        createActivityProcess,
        getActivities,
        setFeaturedActivityProcess,
        cancelActivityProcess,
     } = require('../database/functions')
const { Users, Roles } = require('../models')
const { errorMessageHandler } = require('../utils/functions')

module.exports = {
    getBanner: async (req, res) => {
        try{
            const newsData = await getNewsData()
            return res.json({status:'success', newsData})
        }
        catch(e){
            console.error(e)
            return res.status(200).json({status: 'error', message: 'Error obteniendo datos del banner'})
        }
    },
    modifyBanner: async (req, res) => {
        try{
            const { title1, title2, title3, description, url } = req.body
            let imgPath = req.file ? [req.file.filename] : null
            if(!imgPath){
                throw new Error('No se subió ninguna imagen')
            }

            await putBanner(title1, title2, title3, description, url, imgPath)

            return res.status(200).json({ status:'success' })
        }
        catch(e){
            console.error(e)
            return res.json({status: 'error', message: 'Error modificando banner'})
        }
    },
    createActivity: async (req, res) => {
        try{
            let { title, subtitle, description, startDate, finishDate, address, country, province, postalCode, doctors, type, mode, access, visibility, status, price, discount, accessDiscount, quotas } = req.body
            let errorsOfValidation = validationResult(req)
            if (!errorsOfValidation.isEmpty()) {
                throw new Error(errorsOfValidation.errors.shift().msg)
            }
            let imgPath = req.file ? req.file.filename : null
            if(!imgPath){
                throw new Error('No se subió ninguna imagen')
            }
            //Debemos agregar al modelo de Activities una funcion que valide los roles de visibilidad, acceso, modo, estado, acceso de descuento
            await createActivityProcess(title, subtitle, description, startDate, finishDate, address, country, province, postalCode, doctors, type, mode, access, visibility, status, price, discount, accessDiscount, quotas, imgPath)

            return res.status(200).json({ status:'success' })
        }
        catch(e){
            console.error(e)
            return res.json({status: 'error', message: errorMessageHandler(e), errorCode: 'CREATE_ACTIVITY_001'})
        }
    },
    getAllActivities: async (req, res) => {
        try{
            let activities = await getActivities();


            return res.status(200).json({ status:'success', activities })
        }
        catch(e){
            console.error(e)
            return res.json({status: 'error', message: errorMessageHandler(e), errorCode: 'GET_ACTIVITIES_001'})
        }
    },
    setFeaturedActivity: async (req, res) => {
        try{
            let { activityId } = req.body
            if(!activityId){
                throw new Error('Debe proveer un id de actividad')
            }
            await setFeaturedActivityProcess(activityId)
            return res.status(200).json({ status:'success' })
        }
        catch(e){
            console.error(e)
            return res.json({status: 'error', message: errorMessageHandler(e), errorCode: 'SET_FEATURED_ACTIVITY_001'})
        }
    },
    cancelActivity: async (req, res) => {
        try{
            let { activityId } = req.body
            if(!activityId){
                throw new Error('Debe proveer un id de actividad')
            }
            await cancelActivityProcess(activityId)
            return res.status(200).json({ status:'success' })
        }
        catch(e){
            console.error(e)
            return res.json({status: 'error', message: errorMessageHandler(e), errorCode: 'CANCEL_ACTIVITY_001'})
        } 
    },
    getAllUsers: async (req, res) => {
        try{
            let adminRoleId = (await Roles.findOne({type: 'admin'}))._id
            let users = await Users.find({ role: { $nin: [ adminRoleId ] } })
            return res.status(200).json({ status:'success', users })
        }
        catch(e){
            console.error(e)
            return res.json({status: 'error', message: errorMessageHandler(e), errorCode: 'GET_ALL_USERS_001'})
        }
    }
}