const { Users } = require('../models');

module.exports = {
    subscriptionProcess: async (req, res) => {
        try {
            let { user } = req;
            let { linkCheckout } = await user.prepareToSubscribePlatform();
            return res.status(200).json({ status: 'success', linkCheckout })
        }
        catch(e){
            console.error(e)
            return res.json({ 'status': 'error', 'message': e.message, 'errorCode': 'API_001' })
        }
    }
}