const { errorMessageHandler } = require('../utils/functions')
import { createDb } from '../database/functions'
import { checkSubscriptionStatus } from '../services/MpSubscriptionService'
const { ServerConfiguration, Subscriptions } = require('../models')
const { ActivitiesStatuses } = require('../models/activities')

module.exports = {
    makeDb: async (req, res) => {
        try {
            await createDb()
            return res.status(200).send({ 'status': 'success' })
        }
        catch(e){
            console.error(e)
            return res.send({ 'status': 'error', 'message': errorMessageHandler(e), 'errorCode': 'MAKE_DB_001' })
        }
    },
    test : async (req, res) => {
        //let sub = await Subscriptions.validate('628fe3cc452e5412d0c96e00') 
        //console.log(sub);
    }
}