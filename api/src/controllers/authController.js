const { validationResult } = require('express-validator')
const { errorMessageHandler } = require('../utils/functions')
const { getUserByKeyValue } = require('../database/functions')
const { Users } = require('../models')

module.exports = {
    login: async(req, res) => {
        try{
            var { email, password } = req.body;
            email = email.toLowerCase()

            const user = await getUserByKeyValue('email',email)

            if (!user) {
                throw new Error('Email inexistente.')
            }

            let userLoged = await user.login(password)
            if (!userLoged) {
                throw new Error('Contraseña incorrecta.')
            }

            return res.status(200).send({
                'status': 'success',
                'user': userLoged.user,
                'token': userLoged.token,
                'role': userLoged.user.role
            })
        }
        catch(e){
            return res.send({ 'status': 'error', 'message': errorMessageHandler(e), 'errorCode': 'LOGIN_001' })
        }
    },

    register: async (req, res) => {
        try {
            let findedUser;
            let user;
            const { name, lastName, email, dni, password, genre, phone, address, country, province, title, profession, specialty, jobAddress } = req.body;
            let errorsOfValidation = validationResult(req)
            if (!errorsOfValidation.isEmpty()) {
                throw new Error(errorsOfValidation.errors.shift().msg)
            }

            findedUser = await Users.findOne({ email:email })
            if (findedUser){
                throw new Error('El email ya existe!')
            }
            user = await Users.create({ name, lastName, email, dni, password, phone })
            await user.addJob(title, profession, specialty, jobAddress)
            await user.setGenre(genre)
            
            await user.setCountry(country);
            await user.setAddress(address);
            await user.setProvince(province);
            
            return res.status(200).json({ status: 'success', user });
        } catch (e) {
            console.error(e)
            return res.json({ 'status': 'error', 'message': errorMessageHandler(e), 'errorCode': 'REGISTER_001' });
        }
    },
};