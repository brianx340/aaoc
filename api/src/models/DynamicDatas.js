const mongoose = require('mongoose');

const DynamicDatas = new mongoose.Schema({
    name: String,
    jsonData: {}
}, {
    timestamps: true
})

module.exports = mongoose.model('DynamicDatas', DynamicDatas)