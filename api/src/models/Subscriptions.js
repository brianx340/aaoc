const mongoose = require('mongoose');

const Subscriptions = new mongoose.Schema({
    name: String,
    preferences: {
        payer_email: String,
        reason: String,
        external_reference: String,
        back_url: String,
        auto_recurring: {
            frequency: Number,
            frequency_type: String,
            transaction_amount: Number,
            currency_id: String
        }
    }
}, {
    timestamps: true
})

Subscriptions.statics.validate = async function (subscriptionId){
    try {
        let subscription = await this.findOne({
            _id: subscriptionId
        });
        if (!subscription) {
            throw new Error('No se encontró la suscripción.');
        }
        return subscription;
    } catch (e) {
        console.error(e);
        return e;
    }
};

module.exports = mongoose.model('Subscriptions', Subscriptions)