const mongoose = require('mongoose');

const ActivitiesTypes = new mongoose.Schema({
    type: String, //free , paid
}, {
    timestamps: true
})

module.exports = mongoose.model('ActivitiesTypes', ActivitiesTypes)