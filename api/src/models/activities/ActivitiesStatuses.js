const ServerConfiguration = require('../ServerConfiguration')
const mongoose = require('mongoose');

const ActivitiesStatuses = new mongoose.Schema({
    status: String,
    style: String,
}, {
    timestamps: true
})

ActivitiesStatuses.pre('save', async function (next) {
    try{
        let newStatus = this;
        let serverConfiguration = await ServerConfiguration.findOne({name:'aaoc'});
        await serverConfiguration.prepareNewStatus(newStatus._id)
        next();
    }
    catch(e){
        console.error(e);
        return e;
    }
})

module.exports = mongoose.model('ActivitiesStatuses', ActivitiesStatuses)