const mongoose = require('mongoose');

const ActivitiesModes = new mongoose.Schema({
    mode: String,
}, {
    timestamps: true
})

module.exports = mongoose.model('ActivitiesModes', ActivitiesModes)