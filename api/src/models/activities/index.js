const ActivitiesModes = require('../activities/ActivitiesModes')
const ActivitiesStatuses = require('../activities/ActivitiesStatuses')
const ActivitiesTypes = require('../activities/ActivitiesTypes')

module.exports = {
    ActivitiesModes,
    ActivitiesStatuses,
    ActivitiesTypes
}