const mongoose = require('mongoose');
import bcrypt from "bcryptjs";
const jwt = require("jsonwebtoken");
const { tokenExpire } = require('../config')
const Activities = require('./Activities');
const Roles = require('./Roles');
const PaymentInformations = require('./PaymentInformations');
const { Genres, Countries } = require('./commons');
//const Genres = require('./commons/Genres');
const { getSubscriptionLink } = require('../services/MpSubscriptionService')

const Users = new mongoose.Schema({
    name: String,
    lastName: String,
    email: {
        type: String,
        unique: true,
        required: true,
    },
    dni: Number,
    password: String,
    genre: {
        type: mongoose.Schema.Types.ObjectId,
        ref: "Genres"
    },
    phone: Number,
    paymentInfo: {
        type: mongoose.Schema.Types.ObjectId,
        ref: "PaymentInformations"
    },
    role: {
        type: mongoose.Schema.Types.ObjectId,
        ref: "Roles"
    },
    birthDate: Date,
    address: String,
    country: {
        type: mongoose.Schema.Types.ObjectId,
        ref: "Countries"
    },
    province: String,
    activities:[
        {
            type: mongoose.Schema.Types.ObjectId,
            ref: "Activities"
        }
    ],
    title: String,
    specialty: String,
    profession: String,
    jobAddress: String,
}, {
    timestamps: true
})

function autoPopulateUser(next){
    this.populate('role');
    this.populate('genre');
    this.populate('country');
    this.populate('paymentInfo');
    next();
};

Users.pre('find', autoPopulateUser)
Users.pre('findById', autoPopulateUser)
Users.pre('findOne', autoPopulateUser)

Users.pre('save', async function (next) {
    try{
        let user = this;
        if (!user.isModified("password")) {
            return next();
        }
        const salt = await bcrypt.genSalt(10);
        let encryptedPass =  await bcrypt.hash(user.password, salt);
        let role = (await Roles.findOne({type: 'user'}))._id;

        user.email = user.email.toLowerCase();
        user.password = encryptedPass;
        user.role = role;
        next();
    }
    catch(e){
        console.error(e);
        return e;
    }
})

Users.methods.login = async function(candidatePassword) {
    try{
        let user = this;
        let isValid = await bcrypt.compare(candidatePassword, user.password)
        if(!isValid){
            return false;
        }
        const token = jwt.sign({
            id: user._id,
            user,
            role: user.role
        },process.env.JWT_TOKEN_SECRET, { expiresIn: tokenExpire })
        return { user, token }
    }
    catch(e){
        console.error(e)
        return false
    }
};

Users.methods.addJob = async function(title, profession, specialty, address) {
    try{
        let user = this;
        user.title = title;
        user.profession = profession;
        user.specialty = specialty;
        user.jobAddress = address;
        await user.save()
        return true;
    }
    catch(e){
        console.error(e)
        return false
    }
};

Users.methods.setRole = async function(roleId) {
    try{
        let user = this;
        let role = await Roles.findById(roleId)
        if(!role){
            throw new Error('El rol no existe')
        }
        user.role = roleId;
        await user.save()
        return true;
    }
    catch(e){
        console.error(e)
        return e
    }
};

Users.methods.suscribeToActivity = async function(activityId) {
    let user = this;
    let activity = await Activities.findById(activityId)

    if(!activity){
        throw new Error('La actividad no existe')
    }

    let userHavePermission = activity.roleCanSuscribe(user.role._id)
    if(!userHavePermission){
        throw new Error('El usuario no tiene permiso para inscribirse a esta actividad')
    }

    let userInActivity = activity.users.includes(user._id)
    if(userInActivity){
        throw new Error('El usuario ya se encuentra inscrito en esta actividad')
    }

    activity.validateSubscription(user)

    user.activities.push(activityId);
    activity.users.push(user._id);
    await user.save()
    await activity.save()
    return true;
};

Users.methods.prepareToSubscribePlatform = async function() {
    let user = this;
    if(!user.email){
        throw new Error('El usuario no tiene un email')
    }
    if(user.paymentInfo){
        let { linkCheckout } = user.paymentInfo
        return { linkCheckout }
    }
    
    let data = await getSubscriptionLink(user.email)
    if(!data){
        throw new Error('No se pudo generar el link para el pago')
    }
    let { linkCheckout, id, status } = data

    let paymentInfo = await PaymentInformations.create({
        mpId: id,
        status,
        linkCheckout,
    })

    user.paymentInfo = paymentInfo._id;
    await user.save()
    return { linkCheckout };
};

Users.methods.paymentChecker = async function() {
    let user = this;
    if(!user.paymentInfo){
        return
    }
    let paymentInformation = await PaymentInformations.findById(user.paymentInfo._id)
    let status = await paymentInformation.checkStatus()
    await user.checkPayRole(status)
    return ;
};

Users.methods.getPaymentInfo = async function() {
    let user = this;
    if(!user.paymentInfo){
        return null
    }
    
    let paymentInformation = await PaymentInformations.findById(user.paymentInfo._id)

    return paymentInformation;
};

Users.methods.checkPayRole = async function(status) {
    let user = this;
    let role;
    if(!user.paymentInfo){
        return null
    }

    switch (status) {
        case 'approved':
            if (user.role.type != 'paidUser') {
                role = await Roles.findOne({type: 'paidUser'})
                await user.setRole(role._id);
                return true;
            }
            break;

        case 'rejected':
            if (user.role.type != 'defaulter') {
                //role = await Roles.findOne({type: 'defaulter'})
                //await user.setRole(role._id);
                return true;
            }
            break;
    
        default:
            return false;
    }
    return false;
};

Users.methods.setGenre = async function(genreId) {
    let user = this;
    let genre = await Genres.findById(genreId)
    if(!genre){
        throw new Error('No se ha indicado un genero valido.')
    }
    
    user.genre = genre._id
    await user.save()
    return true;
};

Users.methods.setCountry = async function(countryId) {
    let user = this;
    let country = await Countries.findById(countryId)
    if(!country){
        throw new Error('No se ha indicado un país valido.')
    }
    
    user.country = country._id
    await user.save()
    return true;
};

Users.methods.setAddress = async function(address) {
    let user = this;
    if(!address){
        throw new Error('No se ha indicado una direccion.')
    }
    
    user.address = address
    await user.save()
    return true;
};

Users.methods.setProvince = async function(province) {
    let user = this;
    if(!province){
        throw new Error('No se ha indicado una provincia.')
    }
    
    user.province = province
    await user.save()
    return true;
};

Users.methods.getActivities = async function() {
    let user = this;
    let userActivities = (await user.populate('activities')).activities;
    return userActivities;
};

module.exports = mongoose.model('Users', Users)