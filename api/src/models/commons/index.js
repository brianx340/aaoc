const Countries = require('../commons/Countries')
const Genres = require('../commons/Genres')

module.exports = {
    Countries,
    Genres,
}