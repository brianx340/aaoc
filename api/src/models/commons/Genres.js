const mongoose = require('mongoose');

const Genres = new mongoose.Schema({
    name: String,
}, {
    timestamps: true
})

module.exports = mongoose.model('Genres', Genres)