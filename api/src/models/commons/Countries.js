const mongoose = require('mongoose');

const Countries = new mongoose.Schema({
    name: String,
}, {
    timestamps: true
})

module.exports = mongoose.model('Countries', Countries)