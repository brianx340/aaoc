const mongoose = require('mongoose');
const Subscriptions = require('./Subscriptions');

const ServerConfiguration = new mongoose.Schema({
    name: String,
    featuredActivity: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'Activities'
    },
    activitiesStatusesCanCancel: [
        {
            type: mongoose.Schema.Types.ObjectId,
            ref: 'ActivitiesStatuses'
        }
    ],
    activitiesStatusesCanNotSuscribe: [
        {
            type: mongoose.Schema.Types.ObjectId,
            ref: 'ActivitiesStatuses'
        }
    ],
    actualSubscription: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'Subscriptions'
    }
}, {
    timestamps: true
})

ServerConfiguration.methods.prepareNewStatus = async function(statusId) {
    try{
        let serverConfiguration = this;
        let { activitiesStatusesCanCancel, activitiesStatusesCanNotSuscribe } = serverConfiguration;

        if(activitiesStatusesCanCancel.includes(statusId)){
            throw new Error('El estado ya se encuentra en la lista de estados que pueden cancelar actividades.')
        }
        activitiesStatusesCanCancel.push(statusId);

        if(activitiesStatusesCanNotSuscribe.includes(statusId)){
            throw new Error('El estado ya se encuentra en la lista de estados que no pueden suscribirse.')
        }
        activitiesStatusesCanNotSuscribe.push(statusId);

        await serverConfiguration.save();
        return true
    }
    catch(e){
        console.error(e)
        return e
    }
};

ServerConfiguration.methods.setStatusToSuscribe = async function(statusId) {
    try{
        let serverConfiguration = this;

        serverConfiguration.activitiesStatusesCanNotSuscribe = serverConfiguration.activitiesStatusesCanNotSuscribe.filter(status => {
            if(status.toString() != statusId.toString()){
                return status
            }
        })
        
        await serverConfiguration.save();
        return true
    }
    catch(e){
        console.error(e)
        return e
    }
};


ServerConfiguration.methods.setActualSubscription = async function(subscriptionId) {
    try{
        let serverConfiguration = this;

        await Subscriptions.validate(subscriptionId);

        serverConfiguration.actualSubscription = subscriptionId;
        await serverConfiguration.save();
        return true
    }
    catch(e){
        console.error(e)
        return false
    }
};

ServerConfiguration.methods.setFeaturedActivity = async function(activityId) {
    try{
        let serverConfiguration = this;
        serverConfiguration.featuredActivity = activityId;
        await serverConfiguration.save();
        return true
    }
    catch(e){
        console.error(e)
        return false
    }
};

ServerConfiguration.statics.getFeaturedActivity = async function (){
    try {
        let serverConfiguration = await this.findOne({ name: 'aaoc' }).populate([
            {
                path: 'featuredActivity',
                populate: [
                    {
                        path: 'users'
                    }
                ]
            }
        ]);
        return serverConfiguration.featuredActivity;
    } catch (e) {
        console.error(e);
        return e;
    }
};

module.exports = mongoose.model('ServerConfiguration', ServerConfiguration)