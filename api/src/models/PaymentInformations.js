const mongoose = require('mongoose');
const { checkSubscriptionStatus } = require('../services/MpSubscriptionService');

const PaymentInformations = new mongoose.Schema({
    mpId: String,
    status: String,
    linkCheckout: String,
}, {
    timestamps: true
})

PaymentInformations.methods.checkStatus = async function() {
    let paymentInformation = this;
    if(paymentInformation.status == 'approved'){
        return paymentInformation.status;
    }
    let status = await checkSubscriptionStatus(paymentInformation.mpId);
    if(!status){
        return false;
    }
    paymentInformation.status = status
    await paymentInformation.save()
    return status 
};

module.exports = mongoose.model('PaymentInformations', PaymentInformations)