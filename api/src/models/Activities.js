const { capitalize } = require('../utils/functions')
const mongoose = require('mongoose');
const ServerConfiguration = require('./ServerConfiguration');
const Roles = require('./Roles');

const Activities = new mongoose.Schema({
    title: String,
    subtitle: String,
    description: String,
    startDate: Date,
    finishDate: Date,
    address: String,
    country: String,
    province: String,
    postalCode: String,
    doctors: [ String ],
    type: String,
    mode: String,
    access: [ 
        {
            type: mongoose.Schema.Types.ObjectId,
            ref: 'Roles'
        }
     ],
    visibility: [
        {
            type: mongoose.Schema.Types.ObjectId,
            ref: 'Roles'
        }
    ],
    status: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'ActivitiesStatuses'
    },
    price: Number,
    discount: Number,
    accessDiscount: [
        {
            type: mongoose.Schema.Types.ObjectId,
            ref: 'Roles'
        }
    ],
    quotas: Number,
    imgPath: String,
    users: [
        {
            type: mongoose.Schema.Types.ObjectId,
            ref: "Users"
        }
    ]
}, {
    timestamps: true
})

var autoPopulateActivity = function(next) {
    this.populate('access');
    this.populate('visibility');
    this.populate('accessDiscount');
    this.populate('status');
    next();
  };


Activities.pre('save', async function (next) {
    try{
        let activity = this;
        activity.title = capitalize(activity.title);
        activity.subtitle = capitalize(activity.subtitle);
        activity.description = capitalize(activity.description);
        activity.startDate = new Date(activity.startDate);
        activity.finishDate = new Date(activity.finishDate);
        activity.address = capitalize(activity.address);
        activity.country = capitalize(activity.country);
        activity.province = capitalize(activity.province);
        activity.doctors = activity.doctors[0].split(',').map(doctor => capitalize(doctor))
        
        // Adding visibility to access roles
        activity.visibility = new Array(...new Set([ ...activity.visibility, ...activity.access ]))

        next();
    }
    catch(e){
        console.error(e);
        return e;
    }
})

Activities.pre('find', autoPopulateActivity)
Activities.pre('findById', autoPopulateActivity)
Activities.pre('findOne', autoPopulateActivity)

Activities.methods.haveQuotas = async function(role) {
    try{
        let { quotas, users } = this;
        if(quotas === 0) return true;
        if(quotas > 0){
            if(! (users.length < quotas) ){
                throw new Error('La actividad no tiene cupos')
            }
        };
        return true;
    }
    catch(e){
        console.error(e)
        return false
    }
};

Activities.methods.roleCanSuscribe = async function(roleId) {
    try{
        let activity = this;
        let { withPremissionOfActivities } = (await Roles.findById(roleId));
        let roleAccess = [ roleId, ...withPremissionOfActivities ];
        activity.access.map(role=>{
            if(roleAccess.includes(role)){
                return true
            }
        })
        return false
    }
    catch(e){
        console.error(e)
        return false
    }
};

Activities.methods.statusCanSuscribe = async function(role) {
    try{
        let activity = this;
        let { activitiesStatusesCanNotSuscribe } = await ServerConfiguration.findOne({ name: 'aaoc' });
        if(activitiesStatusesCanNotSuscribe.includes(activity.status)){
            throw new Error('La actividad no esta disponible para inscripciones')
        }
        return true
    }
    catch(e){
        console.error(e)
        return false
    }
};

Activities.methods.validateSubscription = async function(user) {
    let activity = this;
    await activity.roleCanSuscribe(user.role._id)
    await activity.haveQuotas()
    await activity.statusCanSuscribe()
    return true
};

Activities.statics.validate = async function (activityId){
    try {
        let activity = await this.findOne({
            _id: activityId
        });
        if (!activity) {
            throw new Error('No se encontró la suscripción.');
        }
        return activity;
    } catch (e) {
        console.error(e);
        return e;
    }
};

Activities.statics.getFeaturedActivity = async function (){
    try {
        let serverConfiguration = await ServerConfiguration.findOne({ name: 'aaoc' }).populate([
            {
                path: 'featuredActivity',
                populate: [
                    {
                        path: 'users',
                    },
                    {
                        path: 'access',
                    }
                ]
            }
        ]);
        return serverConfiguration.featuredActivity;
    } catch (e) {
        console.error(e);
        return e;
    }
};

Activities.methods.setAccess = async function(roleId) {
    let activity = this;
    let { withPremissionOfActivities } = (await Roles.findById(roleId));
    activity.access = [ roleId ];
    if(withPremissionOfActivities){
        activity.access = [ roleId, ...withPremissionOfActivities ];
    }
    await activity.save();
    return true
};

Activities.statics.getUserActivitiesIsNot = async function(user){
    try{
        let userActivities = user.activities
        let { activitiesStatusesCanNotSuscribe } = await ServerConfiguration.findOne({ name: 'aaoc' });

        let nextActivities = await this.find({
            _id: { $nin: userActivities.map(activity => activity._id) },
            status: { $nin: activitiesStatusesCanNotSuscribe },
            visibility: user.role._id
        }).sort('startDate')
        return nextActivities
    }
    catch(e){
        console.error(e)
        return e
    }
}

module.exports = mongoose.model('Activities', Activities)


