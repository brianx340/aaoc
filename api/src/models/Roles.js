const mongoose = require('mongoose');

const Roles = new mongoose.Schema({
    type: String,
    label: String,
    visibilityActivityTypePermission:[
        {
            type: mongoose.Schema.Types.ObjectId,
            ref: 'ActivitiesTypes'
        }
    ],
    visibilityActivityStatusPermission:[
        {
            type: mongoose.Schema.Types.ObjectId,
            ref: 'ActivitiesStatuses'
        }
    ],
    withPremissionOfActivities:[
        {
            type: mongoose.Schema.Types.ObjectId,
            ref: 'Roles'
        }
    ]
}, {
    timestamps: true
})

module.exports = mongoose.model('Roles', Roles)