const tk = require("jsonwebtoken");
const { getUserByKeyValue } = require("../database/functions");

module.exports = async (req, res, next) => {
    let token = req.headers["access-token"];
    if (!token) return res.status(401).json({ 'status':'error', error: "Access denied" });
    try {
        let userEmail = tk.verify(token, process.env.JWT_TOKEN_SECRET).user.email;
        let user = await getUserByKeyValue('email', userEmail);
        req.user = user;
        return next();
    } catch (error) {
        console.error(error)
        return res.status(400).json({ 'status':'error', error: "Token is not valid " });
    }
};