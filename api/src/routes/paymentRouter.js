const router = require('express').Router();
const { subscriptionProcess } = require('../controllers/subscriptionController');
const checkToken = require("../middlewares/checkToken");

router.get('/getSubscription', checkToken, subscriptionProcess);

module.exports = router;