const router = require("express").Router();
const { makeDb, test } = require("../controllers/devController");

router.get('/makeDb', makeDb);
router.get('/test', test)

module.exports = router;