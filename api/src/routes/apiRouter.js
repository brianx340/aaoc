const router = require('express').Router();
const {
        getCountries, 
        deleteCountry, 
        getSubscriptionData, 
        getActivityTypes, 
        getActivityModes, 
        getActivityStatuses,
        getRoles,
        getGenres,
     } = require('../controllers/apiController');
const checkAdminToken = require("../middlewares/checkAdminToken");
const checkToken = require("../middlewares/checkToken");

router.get('/getCountries', getCountries);
router.get('/getSubscriptionData', checkToken, getSubscriptionData)
router.get('/getActivityTypes', getActivityTypes)
router.get('/getActivityModes', getActivityModes)
router.get('/getActivityStatuses', getActivityStatuses)
router.get('/getRoles', getRoles)
router.get('/getGenres', getGenres)

router.delete('/deleteCountry', checkAdminToken, deleteCountry);

module.exports = router;