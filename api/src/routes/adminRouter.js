const router = require("express").Router();
const { 
    modifyBanner,
    getBanner,
    createActivity,
    getAllActivities,
    setFeaturedActivity,
    cancelActivity,
    getAllUsers,
    } = require("../controllers/adminController");
let uploadImage = require('../middlewares/uploadImage')
const checkAdminToken = require("../middlewares/checkAdminToken");
const createActivityValidator = require('../validations/admin/createActivityValidator')


router.get('/getBanner', getBanner);
router.get('/getAllActivities', checkAdminToken, getAllActivities);
router.get('/getAllUsers', checkAdminToken, getAllUsers);

router.put('/modifyBanner', uploadImage.single("image"), checkAdminToken, modifyBanner)
router.put('/cancelActivity', cancelActivity)

router.post('/createActivity', uploadImage.single("image"), createActivityValidator, checkAdminToken, createActivity)
router.post('/setFeaturedActivity', checkAdminToken, setFeaturedActivity)

module.exports = router;