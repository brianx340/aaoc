const router = require("express").Router();
const checkToken = require("../middlewares/checkToken");
const {
    getFeaturedActivityProcess,
    getNextActivitiesProcess,
    getUserActivitiesIsNot,
    suscribeToActivity,
    getNewsDataProcess,
    getUserActivities,
    getUser,
} = require("../controllers/userController");

router.get('/getFeaturedActivity', checkToken, getFeaturedActivityProcess);
router.get('/getUserActivitiesIsNot', checkToken, getUserActivitiesIsNot);
router.get('/getNextActivities', checkToken, getNextActivitiesProcess);
router.get('/getUserActivities', checkToken, getUserActivities);
router.get('/getNewsData', checkToken, getNewsDataProcess);
router.get('/getUser', checkToken, getUser);

router.post('/suscribe', checkToken, suscribeToActivity);

module.exports = router;