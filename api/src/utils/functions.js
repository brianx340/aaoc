
export const  errorMessageHandler = (e) => {
    switch (e.name){
        case 'ValidationError':
            return 'Error, por favor verifique los datos.'

        default:
            return e.message
    }
}

export const  capitalize = (string) => {
    return string.charAt(0).toUpperCase() + string.toLowerCase().slice(1)
}

export const randomChoice = (choices) => {
    return choices[Math.floor(Math.random() * choices.length)];
}