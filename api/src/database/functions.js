const { Users, DynamicDatas, Activities, ServerConfiguration, Roles, Subscriptions } = require('../models')
const { ActivitiesTypes, ActivitiesStatuses, ActivitiesModes } = require('../models/activities')
const { Countries, Genres } = require('../models/commons')
const { faker } = require('@faker-js/faker');
const { randomChoice } = require('../utils/functions')
const fs = require('fs')

const ACTIVITY_DEFAULT_IMAGES = process.env.ACTIVITY_DEFAULT_IMAGES.split(', ')

const createDb = async () => {
    //First
    await createServer()

    await createMinimunDataTypes()
    await createSubscriptionsData()

    //Late
    await createAdminUser()
    await createDummyEmployeesUsers(15)
    await createDummyUsers(15)
    await createDynamicData()
    await createDummyActivities(30)

    await createServerConfiguration()
}

const createDummyUsers = async (qty) => {
    let allGenres = (await Genres.find({})).map(genre => genre._id)
    let allCountries = (await Countries.find({})).map(country => country._id)
    let userRole = (await Roles.findOne({ type: 'user' }))._id


    let index = 0;
    while (index < qty) {
        let user = await Users.create({
            name: (await faker.name.findName()).split(' ')[0],
            lastName: await faker.name.lastName(),
            email: `user${index}@user.com`,
            dni: 11222333,
            password: 'asdasd',
            phone: 1122334455,
        })

        let address = await faker.address.streetName();
        let province = await faker.address.state();
        
        await user.setGenre(randomChoice(allGenres))
        await user.setCountry(randomChoice(allCountries));
        await user.setAddress(address);
        await user.setProvince(province);


        let title = await faker.name.jobTitle();
        let profession = await faker.name.jobTitle();
        let specialty = await faker.name.jobTitle();
        let jobAddress = await faker.address.streetName();
        await user.addJob(title, profession, specialty, jobAddress)

        await user.setRole(userRole)

        index++;
    }
}

const createDummyEmployeesUsers = async (qty) => {
    let employeeRole = (await Roles.findOne({ type: 'employee' }))._id

    let index = 0;
    while (index < qty) {
        let user = await Users.create({
            name: (await faker.name.findName()).split(' ')[0],
            email: `employee${index}@employee.com`,
            password: 'asdasd',
        })
        await user.setRole(employeeRole)
        index++;
    }
}

const createAdminUser = async () => {
    let adminRole = (await Roles.findOne({ type: 'admin' }))._id
    let user = await Users.create({
        name: 'Admin',
        email: 'admin@aaoc.com',
        password: 'asdasd'
    })

    await user.setRole(adminRole)
    return true
}

const createDummyHomeDynamicData = async () => {
    let data = {
        title1: 'PRECEPTORIA',
        title2: 'SISTEMA NERVIOSO',
        title3: 'CENTRAL',
        description: 'Lorem ipsum dolor sit amet consectetur adipisicing elit, quos culpa, atque incidunt, est adipisci iste alias accusamus molestiae vero numquam aspernatur, eligendi sunt blanditiis.',
        imgPath: `${process.env.PUBLIC_DATA_URL}/img/bannerHome.png`,
        url: 'https://www.google.com'
    }

    await DynamicDatas.create({
        name: 'newsData',
        jsonData: data
    })
}

const getNextActivities = async (user) => {
    let userActivities = user.activities
    let visibilityOfRole = user.role.visibilityActivityStatusPermission

    let nextActivities = await Activities.find({
        _id: { $nin: userActivities },
        status: { $in: visibilityOfRole }
    }).sort('startDate').limit(3)

    return nextActivities
}

const createMinCountries = async () => {
    let countries = ['Argentina', 'Bolivia', 'Brasil', 'Chile', 'Colombia', 'Costa Rica', 'Cuba', 'Ecuador', 'El Salvador', 'Guatemala', 'Honduras', 'México', 'Nicaragua', 'Panamá', 'Paraguay', 'Perú', 'Puerto Rico', 'República Dominicana', 'Uruguay', 'Venezuela']
   
    for(let country of countries){
        await Countries.create({
            name: country
        })
    }
    return true
}

const createDynamicData = async () => {
    await createDummyHomeDynamicData()
}



const createDummyActivities = async (qty) => {
    let index = 0;
    let numbers = [ '01', '02', '03', '04', '05', '06', '07', '08', '09', '10', '11', '12', '13', '14', '15', '16', '17', '18', '19', '20', '21', '22', '23', '24', '25', '26', '27', '28', '29', '30', '31', '32', '33', '34', '35', '36', '37', '38', '39', '40', '41', '42', '43', '44', '45', '46', '47', '48', '49', '50', '51', '52', '53', '54', '55', '56', '57', '58', '59' ]
    try{
        let activitiesModesIds = (await ActivitiesModes.find({})).map(mode => mode._id)
        let activitiesTypesIds = (await ActivitiesTypes.find({})).map(type => type._id)
        let activitiesStatusesIds = (await ActivitiesStatuses.find({ status: { $nin: [ 'cancelled', 'inactive', 'finished' ] } })).map(status => status._id)
        let rolesIds = (await Roles.find({ type: { $nin: [ 'admin', 'defaulter' ] } })).map(role => role._id)

        while (index < qty) {
    
            let initalDate = {
                year: 2022,
                month: randomChoice(numbers.slice(0,12)),
                day: randomChoice(numbers.slice(0,29))
            }
            let initialTime = {
                hour: randomChoice(numbers.slice(0,23)),
                minute: randomChoice(numbers),
            }
            let fulldate= `${initalDate.year}-${initalDate.month}-${initalDate.day}T${initialTime.hour}:${initialTime.minute}`
            let newActivity = await Activities.create({
                title: `Actividad N° ${index}`,
                subtitle: `Subtitle Act N° ${index}`,
                description: `Lorem ipsum dolor sit amet consectetur adipisicing elit, quos culpa, atque incidunt, est adipisci iste alias accusamus molestiae vero numquam aspernatur, eligendi sunt blanditiis.`,
                startDate: new Date(fulldate),
                finishDate: new Date(fulldate),
                address: `Address Act N° ${index}`,
                country: `Country Act N° ${index}`,
                province: `Province Act N° ${index}`,
                postalCode: `Postal Code Act N° ${index}`,
                doctors: [ 
                    `Doctor Act N° ${index}`,
                    `Doctor Act N° ${index+1}`,
                 ],
                type: randomChoice(activitiesTypesIds),
                mode: randomChoice(activitiesModesIds),
                visibility: [ randomChoice(rolesIds) ],
                status: randomChoice(activitiesStatusesIds),
                price: (index+1)*3,
                discount: randomChoice([ 10 , 20 ]),
                accessDiscount: [ randomChoice(rolesIds) ],
                quotas: randomChoice([100,200,300]),
                imgPath: randomChoice(ACTIVITY_DEFAULT_IMAGES),
            })

            newActivity.setAccess( randomChoice(rolesIds) )

            index++;
        }
    }
    catch(e){
        console.error(e)
        throw new Error("Error al crear actividades de prueba.")
    }
}

const createServer = async () => {
    await ServerConfiguration.create({
        name: 'aaoc',
        featuredActivity: null,
        activitiesCanCancel: [],
        activitiesStatusesCanNotSuscribe: [],
        actualSubscription: null
    })
}

const createServerConfiguration = async () => {
    let pendingStatus = (await ActivitiesStatuses.findOne({status:'pending'}))._id
    let processStatus = (await ActivitiesStatuses.findOne({status:'process'}))._id

    let featuredActivity = (await Activities.findOne({ status:pendingStatus }))._id
    let actualSubscription = (await Subscriptions.findOne({ name: 'subscription' }))._id

    let serverConfiguration = await ServerConfiguration.findOne({ name: 'aaoc' })

    //We set the featured activity on the server
    await serverConfiguration.setFeaturedActivity(featuredActivity)

    //We set the subscription of MercadoPago on the server
    await serverConfiguration.setActualSubscription(actualSubscription)

    //We set the status can suscribe
    await serverConfiguration.setStatusToSuscribe(pendingStatus)
    await serverConfiguration.setStatusToSuscribe(processStatus)

    await serverConfiguration.save()
    return true;
}

const getNewsData = async () => {
    const newsData = await DynamicDatas.findOne({ name: 'newsData' })
    return newsData.jsonData
}

const putBanner = async (title1, title2, title3, description, url, imgPath) => {
    let newsData = await DynamicDatas.findOne({ name: 'newsData' })
    let previousImage = newsData.jsonData.imgPath.split('/').pop()
    await deleteImage(previousImage)
    newsData.jsonData = {
        title1,
        title2,
        title3,
        description,
        url,
        imgPath: `${process.env.PUBLIC_DATA_URL}/img/${imgPath}`
    }
    await newsData.save()
    return true
}

const deleteImage = async (imgName) => {
    if(process.env.DEV_MODE === 'true'){
        switch (imgName) {
            case 'bannerHome.png':
                return true
        
            default:
                break;
        }
    }

    let path = __dirname.replace('src/database', 'public/img')
    fs.readdir(path, (err, files) => {
        if(err){
            console.error("Error al eliminar un archivo del almacenamiento")
        }
        if(files.includes(imgName)){
            fs.unlinkSync(`${path}/${imgName}`);
            return true
        }
        return false
    });
}

const createActivityProcess = async (title, subtitle, description, startDate, finishDate, address, country, province, postalCode, doctors, type, mode, access, visibility, status, price, discount, accessDiscount, quotas, imgPath) => {
    try{
        await Activities.create({title, subtitle, description, startDate, finishDate, address, country, province, postalCode, doctors, type, mode, access, visibility, status, price, discount, accessDiscount, quotas, imgPath})
        return true
    }
    catch(err){
        console.error(err)
        deleteImage(imgPath)
        throw new Error('Error al crear la actividad')
    }
}

const getActivities = async () => {
    return await Activities.find({}).sort('-startDate')
}

const setFeaturedActivityProcess = async (id) => {
    try{
        let serverConfiguration = await ServerConfiguration.findOne({ name: 'aaoc' })
        await Activities.validate(id)
        serverConfiguration.featuredActivity = activity._id
        await serverConfiguration.save()
        return true
    }
    catch(err){
        console.error(err)
        throw new Error('Error al establecer la actividad como destacada')
    }
}

const cancelActivityProcess = async (id) => {
    try{
        let statusCanCancel = (await ActivitiesConfiguration.findOne({})).statusCanCancel
        let activity = await Activities.findById(id)
        if(!activity){
            throw new Error('La actividad no existe')
        }
        if(statusCanCancel.includes(activity.status)){
            throw new Error('La actividad no puede ser cancelada')
        }

        activity.status = (await ActivitiesStatuses.findOne({ status: 'cancelled' }))._id
        await activity.save()
        return true
    }
    catch(err){
        console.error(err)
        throw new Error('Error al establecer la actividad como destacada')
    }
}

const createMinimunDataTypes = async () => {
    try {
        await createActivitiesDataTypes()
        await createRoles()
        await createMinCountries()
        await createMinGenres()
    }
    catch(e){
        console.error(e)
        throw new Error("Error al crear los tipos de datos")
    }
}

const createMinGenres = async () => {
    try{
        await Genres.create({ name: 'Femenino' })
        await Genres.create({ name: 'Masculino' })
        return true
    }
    catch(e){
        console.error(e)
        throw new Error("Error al crear los generos")
    }
}

const createRoles = async () => {
    try {
        //find status pending and process
        let visibilityAdmin = (await ActivitiesStatuses.find({})).map(status => status._id)
        let visibilityActivityStatusPermission = (await ActivitiesStatuses.find( { status: { $in: ['pending', 'process'] } } )).map(status => status._id)

        await Roles.create({
            type: 'admin',
            label: 'Administrador',
            visibilityActivityStatusPermission: visibilityAdmin
        });

        await Roles.create({
            type: 'employee',
            label: 'Empleado',
            visibilityActivityStatusPermission: visibilityAdmin
        });

        await Roles.create({
            type: 'defaulter',
            label: 'Deudor',
            visibilityActivityStatusPermission
        });

        let paidUserRole = await Roles.create({
            type: 'paidUser',
            label: 'Socio',
            visibilityActivityStatusPermission
        });

        await Roles.create(
            {
                type: 'user',
                label: 'Usuario',
                visibilityActivityStatusPermission,
                withPremissionOfActivities: [
                    paidUserRole._id,
                ]
            }
        );

        return true
    }
    catch(e){
        console.error(e)
        throw new Error("Error al crear los tipos de datos")
    }
}

const createActivitiesDataTypes = async () => {
    try {
        await createActivitiesTypes()
        await createActivitiesStatuses()
        await createActivitiesModes()
        return true
    }
    catch(e){
        console.error(e)
        throw new Error("Error al crear los tipos de datos")
    }
}

const createActivitiesTypes = async () => {
    try {
        await ActivitiesTypes.create({ type: 'free' })
        await ActivitiesTypes.create({ type: 'paid' })
        return true
    }
    catch(e){
        console.error(e)
        throw new Error("Error al crear los tipos de datos")
    }
}

const createActivitiesStatuses = async () => {
    try {
        await ActivitiesStatuses.create({ status: 'inactive' });
        await ActivitiesStatuses.create({ status: 'pending', style: 'info' });
        await ActivitiesStatuses.create({ status: 'process', style: 'warning' });
        await ActivitiesStatuses.create({ status: 'finished', style: 'success' });
        await ActivitiesStatuses.create({ status: 'cancelled', style: 'danger' });
        return true
    }
    catch(e){
        console.error(e)
        throw new Error("Error al crear los tipos de datos")
    }
}

const createActivitiesModes = async () => {
    try {
        await ActivitiesModes.create({ mode: 'online' })
        await ActivitiesModes.create({ mode: 'ftf' })
        return true
    }
    catch(e){
        console.error(e)
        throw new Error("Error al crear los tipos de datos")
    }
}

const getUserByKeyValue = async (key, value) => {
    let user = await Users.findOne({ [key]: value }).populate('paymentInfo')
    return user;
}

const createSubscriptionsData = async () => {
    try {
        let subscription = await Subscriptions.create({
            name: 'subscription',
            preferences:{
                payer_email: "",
                reason: "Subscripcion mensual AAOC",
                external_reference: "",
                back_url: "https://www.google.com/",
                auto_recurring: {
                    frequency: 1,
                    frequency_type: "months",
                    transaction_amount: 100,
                    currency_id: "ARS"
                }
            }
        })
        return true
    }
    catch(e){
        console.error(e)
        throw new Error("Error al crear los tipos de datos")
    }
}

module.exports = {
    createDb,
    getNewsData,
    putBanner,
    createActivityProcess,
    getActivities,
    setFeaturedActivityProcess,
    createServerConfiguration,
    cancelActivityProcess,
    getUserByKeyValue,
    getNextActivities,
}