const MercadoPago = require("mercadopago");
const ServerConfiguration = require('../models/ServerConfiguration');
const { authHeaderMp } = require('./AuthHeaderMp');
const axios = require('axios');

const getSubscriptionLink = async (clientEmail) => {
    MercadoPago.configure({
        client_id: process.env.MP_CLIENT_ID,
        client_secret: process.env.MP_CLIENT_SECRET
    });
    try {
        const { actualSubscription } = (await ServerConfiguration.findOne({name:'aaoc'}).populate('actualSubscription') );
        var preferences = actualSubscription.preferences;
        preferences.payer_email = clientEmail;
        const mp = await MercadoPago.preapproval.create(preferences);
        const { init_point, id, status }  = mp && mp.response;
        return {
            linkCheckout: init_point,
            id,
            status
        };
    } catch (err) {
        return mercadopagoErrorHandler(err);
    }
}

const checkSubscriptionStatus = async (subscriptionId) => {
    try {
        const response = await axios.get(`https://api.mercadopago.com/v1/payments/${subscriptionId}`, { headers: authHeaderMp() });
        let { status } = response.data;
        return status;
    }  
    catch(e){
        console.error(e)
        return mercadopagoErrorHandler(e);
    }
}

const mercadopagoErrorHandler = (err) => {
    switch (err.message) {
        case 'Cannot operate between different countries':
            throw new Error("El correo electronico pertenece a un pais diferente al pais de la cuenta de la AAOC en Mercadopago");
        default:
            return false;
    }
}

module.exports = {
    getSubscriptionLink,
    checkSubscriptionStatus
};