const { check, body } = require('express-validator')

module.exports = [

    //Personal Data
    body('name')
    .custom((value) => {
        let regexName = new RegExp(/^[a-zA-Z ]{2,}$/)
        return regexName.test(value)
    })
    .withMessage("Debe proveer un nombre valido"),

    body('lastName')
    .custom((value) => {
        let regexName = new RegExp(/^[a-zA-Z ]{2,}$/);
        return regexName.test(value)
    })
    .withMessage("Debe proveer un apellido valido"),
    
    body('email')
    .custom(value => {
        let regExEmail = new RegExp(/^(([^<>()\[\]\\.,:\s@"]+(\.[^<>()\[\]\\.,:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/);
        if (regExEmail.test(value)) {
            return true
        } else {
            return Promise.reject('Debe proveer un email valido')
        }
    }),
    
    body('dni')
    .notEmpty()
    .withMessage('Debe proveer un dni.')
    .custom(value => {
        return !isNaN(Number(value.replace(',', '.')))
    })
    .withMessage("Este campo solo admite números."),

    body('password')
    .custom((value) => {
        let regexPassword = new RegExp(/^(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{6,15}$/);
        return regexPassword.test(value)
    })
    .withMessage("Debe proveer una contraseña valida."),

    body('phone')
    .notEmpty()
    .withMessage('Debe proveer un numero de celular.')
    .custom(value => {
        return !isNaN(Number(value.replace(',', '.')))
    })
    .withMessage("Este campo solo admite números."),
    
    
    //Address

    body('address')
    .notEmpty()
    .withMessage("Debe proveer una dirección."),

    body('country')
    .notEmpty()
    .withMessage("Debe proveer un país"),

    body('province')
    .notEmpty()
    .withMessage("Debe proveer una provincia."),

    body('address')
    .notEmpty()
    .withMessage("Debe proveer un país."),

    //Job

    body('title')
    .notEmpty()
    .withMessage("Debe proveer un titulo."),

    body('specialty')
    .notEmpty()
    .withMessage("Debe proveer una especialidad."),

    body('jobAddress')
    .notEmpty()
    .withMessage("Debe proveer una dirección."),

]