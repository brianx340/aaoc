const { body } = require('express-validator')
const { ActivitiesModes, ActivitiesStatuses, ActivitiesTypes } = require('../../models/activities')
const { Roles } = require('../../models')

module.exports = [
    body('title')
    .notEmpty()
    .withMessage("Debe proveer un titulo."),

    body('subtitle')
    .notEmpty()
    .withMessage("Debe proveer un subtitulo."),
    
    body('description')
    .notEmpty()
    .withMessage("Debe proveer una descripción."),

    body('startDate')
    .notEmpty()
    .withMessage('Debe proveer una fecha de inicio.')
    .custom(value => {
        let date = new Date(value)
        if(date){
            return true
        }
        return false
    })
    .withMessage("Debe proveer una fecha valida."),

    body('finishDate')
    .notEmpty()
    .withMessage('Debe proveer una fecha de inicio.')
    .custom(value => {
        let date = new Date(value)
        if(date){
            return true
        }
        return false
    })
    .withMessage("Debe proveer una fecha valida."),

    body('address')
    .notEmpty()
    .withMessage("Debe proveer una direccion."),

    body('country')
    .notEmpty()
    .withMessage("Debe proveer un país."),

    body('province')
    .notEmpty()
    .withMessage("Debe proveer una provincia."),

    body('type')
    .notEmpty()
    .custom( async (value) => {
        const activityType = await ActivitiesTypes.findById(value)
        if(!activityType){
            return false
        }
        return true
    })
    .withMessage("Debe proveer un tipo valido."),

        
    body('mode')
    .notEmpty()
    .custom( async (value) => {
        const activityMode = await ActivitiesModes.findById(value)
        if(!activityMode){
            return false
        }
        return true
    })
    .withMessage("Debe proveer un modo valido."),

    body('access')
    .notEmpty()
    .withMessage("Debe proveer acceso al menos a un tipo de usuario.")
    .custom( async (value,{req}) => {
        var userRolesReceived = value.split(',')
        const userRolesInDb = await Roles.find({})

        for ( let role of userRolesReceived ) {
            let verified = false
            if(!verified){
                for ( let type of userRolesInDb ) {
                    if(type._id.toString() === role){
                        verified = true
                        break
                    }
                }
            }
            if(!verified){
                return false
            }
        }
        return true
    })
    .withMessage("Debe proveer tipos de usuarios validos."),

    

    body('visibility')
    .notEmpty()
    .withMessage("Debe proveer acceso al menos a un tipo de usuario.")
    .custom( async (value) => {
        var userRolesReceived = value.split(',')
        const userRolesInDb = await Roles.find({})

        for ( let role of userRolesReceived ) {
            let verified = false
            if(!verified){
                for ( let type of userRolesInDb ) {
                    if(type._id.toString() === role){
                        verified = true
                        break
                    }
                }
            }
            if(!verified){
                return false
            }
        }
        return true
    })
    .withMessage("Debe proveer visibilidad a tipos de usuarios validos."),


    


    body('status')
    .notEmpty()
    .withMessage('Debe proveer un estado inicial.')
    .custom( async (value) => {
        let statusInDb = await ActivitiesStatuses.findById(value)
        return statusInDb ? true : false
    })
    .withMessage("Debe proveer un estado inicial correcto."),


    body('price')
    .notEmpty()
    .withMessage('Debe proveer un precio.')
    .custom(value => {
        return !isNaN(Number(value.replace(',', '.')))
    })
    .withMessage("Este campo solo admite números."),

    body('accessDiscount')
    .notEmpty()
    .withMessage("Debe proveer el descuento al menos a un tipo de usuario.")
    .custom(value => {
        const userTypes = process.env.USER_TYPES.split(', ')
        var userTypesReceived = value.split(',')
        for ( let role of userTypesReceived ) {
            if(!userTypes.includes(role)){
                return false
            }
        }
        return true
    })
    .withMessage("Debe proveer el descuento al menos a un tipo de usuario."),
    
    body('quotas')
    .notEmpty()
    .withMessage('Debe proveer una cantidad.')
    .custom(value => {
        return !isNaN(Number(value.replace(',', '.')))
    })
    .withMessage("Este campo solo admite números."),

]