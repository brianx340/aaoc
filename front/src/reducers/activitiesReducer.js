const activitiesReducer = (state, action) => {
    switch (action.type) {
        case 'ACTIVITIES_FETCH_INIT':
            return {
                ...state,
                isLoading: true,
                isError: false,
            }

        case 'ACTIVITIES_FETCH_SUCCESS':
            return {
                ...state,
                isLoading: false,
                isError: false,
                data: action.payload,
            }
        
        case 'ACTIVITIES_FETCH_FAILURE':
            return {
                ...state,
                isLoading: false,
                isError: true,
            }

        default:
            return
    }
}

module.exports = {
    activitiesReducer
}