const registerReducer = (state, action) => {
    switch (action.type) {
        case 'REGISTER_FETCH_EMPTY':
            return {
                ...state,
                isLoading: false,
                isError: false,
                errorMessage: ''
            }

        case 'REGISTER_FETCH_INIT':
            return {
                ...state,
                isLoading: true,
                isError: false,
                errorMessage: ''
            }

        case 'REGISTER_FETCH_SUCCESS':
            return {
                ...state,
                isLoading: false,
                isError: false,
                errorMessage: '',
                data: action.payload,
            }
        
        case 'REGISTER_FETCH_FAILURE':
            return {
                ...state,
                isLoading: false,
                isError: true,
                message: action.payload,
            }

        case 'CLEAR_MESSAGE':
            return {
                ...state,
                isLoading: false,
                isError: false,
            }

        default:
            return
    }
}

module.exports = {
    registerReducer
}