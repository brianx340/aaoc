const userReducer = (state, action) => {
    switch (action.type) {
        case 'USER_FETCH_INIT':
            return {
                ...state,
                isLoading: true,
                isError: false,
            }

        case 'USER_FETCH_SUCCESS':
            return {
                ...state,
                isLoading: false,
                isError: false,
                data: action.payload,
            }
        
        case 'USER_FETCH_FAILURE':
            return {
                ...state,
                isLoading: false,
                isError: true,
            }

        default:
            return
    }
}

module.exports = {
    userReducer
}