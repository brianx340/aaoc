const axiosReducer = (state, action) => {
    switch (action.type) {
        case 'FETCH_EMPTY':
            return {
                ...state,
                isLoading: false,
                isError: false,
                isSuccess: false,
            }

        case 'FETCH_INIT':
            return {
                ...state,
                isLoading: true,
                isError: false,
                isSuccess: false,
            }

        case 'FETCH_SUCCESS':
            return {
                ...state,
                isLoading: false,
                isError: false,
                isSuccess: true,
                data: action.payload.data,
                message: action.payload.message,
            }
        
        case 'FETCH_FAILURE':
            return {
                ...state,
                isLoading: false,
                isError: true,
                isSuccess: false,
                message: action.payload.message,
            }

        default:
            return
    }
}

module.exports = {
    axiosReducer
}