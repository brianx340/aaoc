export const loginReducer = (state, action) => {
    switch (action.type) {
        case 'LOGIN_FETCH_EMPTY':
            return {
                ...state,
                isLoading: false,
                isError: false,
                errorMessage: ''
            }

        case 'LOGIN_FETCH_INIT':
            return {
                ...state,
                isLoading: true,
                isError: false,
                errorMessage: ''
            }

        case 'LOGIN_FETCH_SUCCESS':
            localStorage.setItem('token', action.payload.token)
            return {
                ...state,
                isLoading: false,
                isError: false,
                errorMessage: ''
            }

        case 'LOGIN_FETCH_FAILURE':
            return {
                ...state,
                isLoading: false,
                isError: true,
                message: action.payload,
            }

        case 'CLEAR_MESSAGE':
            return {
                ...state,
                isLoading: false,
                isError: false,
            }

        default:
            return
    }
}