export const putBannerReducer = (state, action) => {
    switch (action.type) {
        case 'PUT_BANNER_FETCH_EMPTY':
            return {
                ...state,
                isLoading: false,
                isError: false,
                errorMessage: ''
            }

        case 'PUT_BANNER_FETCH_INIT':
            return {
                ...state,
                isLoading: true,
                isError: false,
                errorMessage: ''
            }

        case 'PUT_BANNER_FETCH_SUCCESS':
            return {
                ...state,
                isLoading: false,
                isError: false,
                errorMessage: ''
            }

        case 'PUT_BANNER_FETCH_FAILURE':
            return {
                ...state,
                isLoading: false,
                isError: true,
                message: action.payload,
            }

        case 'CLEAR_MESSAGE':
            return {
                ...state,
                isLoading: false,
                isError: false,
            }

        default:
            return
    }
}