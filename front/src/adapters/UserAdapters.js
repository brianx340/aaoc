
const billingAndVouchersAdapter = (vouchers, billingData) => {
    let billing = billingAdapter( billingData );



    return { vouchers, billingData:billing };
}

const billingAdapter = (billingData) => {
    let { payMethod, membershipCost, debts, memberType } = billingData;
    let billing = {
        payMethod: payMethodAdapter(payMethod),
        membershipCost: moneyAdapter(membershipCost),
        debts: moneyAdapter(debts),
        memberType: memberTypeAdapter(memberType),
    }
    return billing;
}

const payMethodAdapter = (payMethod) => {
    //Methods
    // creditCard, bankTransfer, cash, mp
    switch (payMethod) {
        case 'creditCard':
            return 'Tarjeta de crédito';
            
        case 'bankTransfer':
            return 'Transferencia bancaria';
        
        case 'cash':
            return 'Efectivo';
        
        case 'mp':
            return 'MercadoPago';
        
        default:
            return 'No definido';
    }
}

const moneyAdapter = (amount) => {
    if(isNaN(amount)) {
        return 'No definido';
    }
    return `$${amount}`;
}

const memberTypeAdapter = (memberType) => {
    //Types
    // dev, admin, paidUser, user, defaulter
    if(!isNaN(memberType)){
        return 'No definido';
    }
    switch (memberType) {
        case 'dev':
            return 'Desarrollador';

        case 'admin':
            return 'Administrador';
        
        case 'paidUser':
            return 'Socio';
        
        case 'user':
            return 'Usuario';
        
        case 'defaulter':
            return 'Deudor';
        
        default:
            return 'No definido';
    }
}

module.exports = {
    billingAndVouchersAdapter
}