const activityTypesAdapter = (activityTypes) => {
    return activityTypes.map(activityType => {
        return {
            value: activityType._id,
            label: activityType.type
        }
    })
}

const activityModesAdapter = (activityModes) => {
    return activityModes.map(activityModes => {
        return {
            value: activityModes._id,
            label: activityModes.mode
        }
    })
}

const activityStatusesAdapter = (activityStatuses) => {
    return activityStatuses.map(activityStatuses => {
        return {
            value: activityStatuses._id,
            label: activityStatuses.status
        }
    })
}

const rolesAdapter = (roles) => {
    return roles.map(roles => {
        return {
            value: roles._id,
            label: roles.type
        }
    })
}

const countriesAdapter = (countries) => {
    return countries.map(country => {
        return {
            value: country._id,
            label: country.name
        }
    })
}

const genresAdapter = (genres) => {
    return genres.map(genre => {
        return {
            value: genre._id,
            label: genre.name
        }
    })
}

module.exports = {
    activityTypesAdapter,
    activityModesAdapter,
    activityStatusesAdapter,
    rolesAdapter,
    countriesAdapter,
    genresAdapter
}