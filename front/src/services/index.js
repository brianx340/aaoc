const { authHeader } = require('./AuthHeader');
const { loginService, registerService } = require('./AuthService');

module.exports = {
    authHeader,
    loginService,
    registerService
}