const axios = require('axios');
const { API_URL } = require('../config')
const {
    activityTypesAdapter,
    activityModesAdapter,
    activityStatusesAdapter,
    rolesAdapter,
    countriesAdapter,
    genresAdapter,
} = require('../adapters/ApiAdapters')


const getActivityTypes = async () => {
    try{
        const response = await axios.get(`${API_URL+'/api/getActivityTypes'}`)
        if(response.data.status !== 'success'){
            throw new Error(response.data.message)
        }
        let activityTypes = activityTypesAdapter(response.data.data.activityTypes)
        return activityTypes
    }catch(e){
        throw new Error(e.message)
    }
}

const getActivityModes = async () => {
    try{
        const response = await axios.get(`${API_URL+'/api/getActivityModes'}`)
        if(response.data.status !== 'success'){
            throw new Error(response.data.message)
        }
        let activityModes = activityModesAdapter(response.data.data.activityModes)
        return activityModes
    }catch(e){
        throw new Error(e.message)
    }
}

const getActivityStatuses = async () => {
    try{
        const response = await axios.get(`${API_URL+'/api/getActivityStatuses'}`)
        if(response.data.status !== 'success'){
            throw new Error(response.data.message)
        }
        let activityStatuses = activityStatusesAdapter(response.data.data.activityStatuses)
        return activityStatuses
    }catch(e){
        throw new Error(e.message)
    }
}

const getRoles = async () => {
    try{
        const response = await axios.get(`${API_URL+'/api/getRoles'}`)
        if(response.data.status !== 'success'){
            throw new Error(response.data.message)
        }
        let roles = rolesAdapter(response.data.data.roles)
        return roles
    }catch(e){
        throw new Error(e.message)
    }
}

const getCountries = async () => {
    try {
        let response = await axios.get(`${API_URL+'/api/getCountries'}`)
        let countries = countriesAdapter(response.data.data.countries)
        return countries
    }
    catch(e) {
        return { success: false, message: e.message }
    }
}

const getGenres = async () => {
    try {
        let response = await axios.get(`${API_URL+'/api/getGenres'}`)
        let genres = await genresAdapter(response.data.data.genres)
        return genres
    }
    catch(e) {
        return { success: false, message: e.message }
    }
}

module.exports = {
    getActivityTypes,
    getActivityModes,
    getActivityStatuses,
    getRoles,
    getCountries,
    getGenres,
}