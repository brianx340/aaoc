const axios = require('axios');
const { API_URL } = require('../config')
const { authHeader } = require('./AuthHeader');

const getHomeBannerData = async (data) => {
    try {
        let response = await axios.get(`${API_URL+'/admin/getBanner'}`, { headers: authHeader() })
        if(response.data.status !== 'success'){
            throw new Error("Error al obtener información.")
        }
        return response.data.newsData
    }
    catch(e) {
        return { success: false, message: e.message }
    }
}

const putBannerQuery = async (data) => {
    try {
        let response = await axios.put(`${API_URL+'/admin/modifyBanner'}`, data, {
            headers: {
                "content-type": "multipart/form-data",
                ...authHeader()
            }
        })
        if(response.data.status !== 'success'){
            throw new Error("Error al obtener información.")
        }
        return response.data
    }
    catch(e) {
        return { success: false, message: e.message }
    }
}

const createActivityQuery = async (formData) => {
    try {
        let response = await axios.post(`${API_URL+'/admin/createActivity'}`, formData, {
            headers: {
                "content-type": "multipart/form-data",
                ...authHeader()
            }
        })
        if(response.data.status !== 'success'){
            throw new Error(response.data.message)
        }
        return response.data
    }
    catch(e) {
        return { success: false, message: e.message }
    }
}

const getAllActivitiesQuery = async (data) => {
    try {
        let response = await axios.get(`${API_URL+'/admin/getAllActivities'}`, { headers: authHeader() })
        if(response.data.status !== 'success'){
            throw new Error("Error al obtener información.")
        }
        return response.data
    }
    catch(e) {
        return { success: false, message: e.message }
    }
}

const setFeaturedActivityQuery = async (activityId) => {
    try {
        let response = await axios.post(`${API_URL+'/admin/setFeaturedActivity'}`, {activityId}, { headers: authHeader() })
        if(response.data.status !== 'success'){
            throw new Error("Error al obtener información.")
        }
        return response.data
    }
    catch(e) {
        return { success: false, message: e.message }
    }
}

const cancelActivityQuery = async (activityId) => {
    try {
        let response = await axios.put(`${API_URL+'/admin/cancelActivity'}`, {activityId}, { headers: authHeader() })
        if(response.data.status !== 'success'){
            throw new Error("Error al obtener información.")
        }
        return response.data
    }
    catch(e) {
        return { success: false, message: e.message }
    }
}




const deleteCountryQuery = async (countryId) => {
    try {
        let response = await axios.delete(`${API_URL+'/api/deleteCountry'}`, { headers: authHeader(), data:{countryId} })
        if(response.data.status !== 'success'){
            throw new Error("Error al obtener información.")
        }
        return response.data
    }
    catch(e) {
        return { success: false, message: e.message }
    }
}

const getAllUsersQuery = async () => {
    try {
        let response = await axios.get(`${API_URL+'/admin/getAllUsers'}`, { headers: authHeader() })
        if(response.data.status !== 'success'){
            throw new Error("Error al obtener información.")
        }
        return response.data
    }
    catch(e) {
        return { success: false, message: e.message }
    }
}

module.exports = {
    getHomeBannerData,
    putBannerQuery,
    createActivityQuery,
    getAllActivitiesQuery,
    setFeaturedActivityQuery,
    cancelActivityQuery,
    deleteCountryQuery,
    getAllUsersQuery,
}