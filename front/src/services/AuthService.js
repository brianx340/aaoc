const axios = require('axios');

const loginService = async (data) => {  
    try{
        const response = await axios.post(`http://localhost:3001/login`, data)
        if(response.data.status !== 'success'){
            throw new Error(response.data.message)
        }
        return response.data
    }
    catch(e){
        throw new Error(e.message)
    }
}

const registerService = async (data) => {
    try{
        const response = await axios.post(`http://localhost:3001/register`, data)
        if(response.data.status !== 'success'){
            throw new Error(response.data.message)
        }
        return response.data
    }catch(e){
        throw new Error(e.message)
    }
}

module.exports = {
    loginService,
    registerService
}