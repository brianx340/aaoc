const axios = require('axios');
const { API_URL } = require('../config')
const { authHeader } = require('./AuthHeader');
const { billingAndVouchersAdapter } = require('../adapters/UserAdapters');

/*
const getData = async () => {
    axios()
    data = handlerResponse(response)
    return data;
}
*/

const getHomeData = async (data) => {
    try {
        let response = await axios.get(`${API_URL+'/user/getHomeData'}`, { headers: authHeader() })
        if(response.data.status !== 'success'){
            throw new Error("Error al obtener información.")
        }
        return response.data.data
    }
    catch(e) {
        return { success: false, message: e.message }
    }
}

const getCertificates = async () => {
    const allCertificates = [
        {
            name: 'Certificado 1',
            date: new Date(),
            imgPath: 'https://minimalmedics.com/wp-content/uploads/2020/05/minimal-medics-quienes-somos-1.png'
        },
        {
            name: 'Certificado 2',
            date: new Date(),
            imgPath: 'https://minimalmedics.com/wp-content/uploads/2020/05/minimal-medics-quienes-somos-1.png'
        }
    ]
    return allCertificates;
}

const getBillingAndVouchersData = async () => {
    let { billingData, vouchers } = await getDummyBillingData()
    let data = await billingAndVouchersAdapter(vouchers, billingData)
    return data;
}

const getDummyBillingData = async () => {
    const actualUser = {
        _id: 1,
        name: "Juan",
        lastName: "Perez",
        email: "asd@asd.com",
        dni: "12345678",
        password: "123456",
        genre: "Masculino",
        phone: "12345678",
        billingData: {
            payMethod: 'creditCard', // metodos... [ creditCard, bankTransfer, cash, mp ]
            membershipCost: 100,
            debts: 44,
            memberType: 'user', // dev, admin, paidUser, user, defaulter 
        },
        address: {
            _id: 1,
            address: "Calle falsa 1234",
            country: "Anon",
            province: "Springfield",
            postalCode: "12345"
        },
        job: {
            _id: 1,
            title: "Asd",
            profession: "Desarrollador",
            specialty: "Frontend",
            address: {
                _id: 2,
                address: "Av siempreviva 5678",
                country: "Anon",
                province: "Springfield",
                postalCode: "12345"
            }
        },
        role: {
            _id: 1,
            name: "user"
        },
        birthDate: "12/12/12"
    }
    var { billingData } = actualUser;
    const vouchers = [
        {
            _id:'525528',
            month:'Enero',
            status:'Pago',
            dueDate:'11-2-2021',
            dateOfIssue:'11-2-2021',
            urlPath:'www.asd.com/asd.jpg',
        },
        {
            _id:'525528',
            month:'Febrero',
            status:'Vencida',
            dueDate:'11-2-2021',
            dateOfIssue:'11-2-2021',
            urlPath:'www.asd.com/asd.jpg',
        },
        {
            _id:'525528',
            month:'Enero',
            status:'Pago',
            dueDate:'11-2-2021',
            dateOfIssue:'11-2-2021',
            urlPath:'www.asd.com/asd.jpg',
        },
        {
            _id:'525528',
            month:'Febrero',
            status:'Vencida',
            dueDate:'11-2-2021',
            dateOfIssue:'11-2-2021',
            urlPath:'www.asd.com/asd.jpg',
        }
    ]
    return { billingData, vouchers }
}

const suscribeMeQuery = async (data) => {
    try {
        let response = await axios.post(`${API_URL+'/user/suscribe'}`, data, { headers: authHeader() })
        return response.data
    }
    catch(e) {
        return { success: false, message: e.message }
    }
}

const getSubscriptionQuery = async () => {
    try {
        let response = await axios.get(`${API_URL+'/payment/getSubscription'}`, { headers: authHeader() })
        return response.data
    }
    catch(e) {
        return { success: false, message: e.message }
    }
}

const getNextActivities = async () => {
    try {
        let response = await axios.get(`${API_URL+'/user/getNextActivities'}`, { headers: authHeader() })
        if(response.data.status !== 'success'){
            throw new Error("Error al obtener información.")
        }
        return response.data
    }
    catch(e) {
        return { success: false, message: e.message }
    }
}

const getFeaturedActivity = async () => {
    try {
        let response = await axios.get(`${API_URL+'/user/getFeaturedActivity'}`, { headers: authHeader() })
        if(response.data.status !== 'success'){
            throw new Error("Error al obtener información.")
        }
        return response.data
    }
    catch(e) {
        return { success: false, message: e.message }
    }
}

const getNewsData = async () => {
    try {
        let response = await axios.get(`${API_URL+'/user/getNewsData'}`, { headers: authHeader() })
        if(response.data.status !== 'success'){
            throw new Error("Error al obtener información.")
        }
        return response.data
    }
    catch(e) {
        return { success: false, message: e.message }
    }
}

const getSubscriptionData = async () => {
    try {
        let response = await axios.get(`${API_URL+'/api/getSubscriptionData'}`, { headers: authHeader() })
        if(response.data.status !== 'success'){
            throw new Error("Error al obtener información.")
        }
        return response.data
    }
    catch(e) {
        return { success: false, message: e.message }
    }
}

const getUser = async () => {
    try {
        let response = await axios.get(`${API_URL+'/user/getUser'}`, { headers: authHeader() })
        if(response.data.status !== 'success'){
            throw new Error("Error al obtener información.")
        }
        return response.data
    }
    catch(e) {
        return { success: false, message: e.message }
    }
}

const getUserActivities = async () => {
    try {
        let response = await axios.get(`${API_URL+'/user/getUserActivities'}`, { headers: authHeader() })
        if(response.data.status !== 'success'){
            throw new Error("Error al obtener información.")
        }
        return response.data.userAactivities
    }
    catch(e) {
        return { success: false, message: e.message }
    }
}

const getUserActivitiesIsNot = async () => {
    try {
        let response = await axios.get(`${API_URL+'/user/getUserActivitiesIsNot'}`, { headers: authHeader() })
        if(response.data.status !== 'success'){
            throw new Error("Error al obtener información.")
        }
        return response.data.otherActivities
    }
    catch(e) {
        return { success: false, message: e.message }
    }
}

module.exports = {
    getBillingAndVouchersData,
    getUserActivitiesIsNot,
    getSubscriptionQuery,
    getFeaturedActivity,
    getSubscriptionData,
    getUserActivities,
    getNextActivities,
    getCertificates,
    suscribeMeQuery,
    getHomeData,
    getNewsData,
    getUser,
}