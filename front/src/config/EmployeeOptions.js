export const employeeOptions = [
    {
        label: 'Inicio',
        url: '/employee',
        icon: 'home',
        subOptions: false
    },
    {
        label: 'Clientes',
        url:'#',
        subOptions: [
            {
                label: 'Consultar',
                url: '/employee/checkQr',
            }
        ]
    },
]