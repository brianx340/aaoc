const { adminOptions } = require('../config/AdminOptions');
const { employeeOptions } = require('../config/EmployeeOptions');
const { testUsers } = require('../config/TestUsers');

module.exports = {
    API_URL: 'http://localhost:3001',
    panelAaocOptions: {
        admin: adminOptions,
        employee: employeeOptions,
    },
    testUsers,
}