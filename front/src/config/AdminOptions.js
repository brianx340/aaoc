export const adminOptions = [
    {
        label: 'Inicio',
        url: '/admin',
        icon: 'home',
        subOptions: false
    },
    {
        label: 'Clientes',
        url:'#',
        subOptions: [
            {
                label: 'Consultar',
                url: '/admin/clients/list',
            }
        ]
    },
    {
        label: 'Actividades',
        url:'#',
        subOptions: [
            {
                label: 'Crear',
                url: '/admin/activities/create',
            },
            {
                label: 'Consultar',
                url: '/admin/activities/list',
            }
        ]
    },
    {
        label: 'Configuraciones',
        url:'#',
        subOptions: [
            {
                label: 'Modificar banner',
                url: '/admin/config/modifyBanner',
            },
            {
                label: 'Países admitidos',
                url: '/admin/config/addAdmitCountries',
            }
        ]
    }
]