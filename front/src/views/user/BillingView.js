import React, { useState, useEffect, useContext } from 'react';
import { AppContext } from "../../context/AppContext";
import { useNavigate } from "react-router-dom";
import { getBillingAndVouchersData } from '../../services/UserService'
import "../../assets/css/billing.scss"
import arrowTitle from '../../assets/img/home/arrow_rigth.svg'
import BillingSection from '../../components/billing/BillingSection'
import BillingTable from '../../components/billing/BillingTable'
import FontAwesomeIconSolid from '../../components/custom/FontAwesomeIconSolid'

export default function BillingView() {
    const { app } = useContext(AppContext);
    const [ vouchers, setVouchers ] = useState(null)
    //const [ billing, setBilling ] = useState(null)
    const [ sessionReady, setSessionReady ] = useState(false)
    const navigate = useNavigate();

    const processSession = async () => {
        const { vouchers, billingData } = await getBillingAndVouchersData();
        if(billingData && vouchers) {
            //setBilling(billingData)
            setVouchers(vouchers)
            setSessionReady(true)
        }
    }

    useEffect(() => {
        if(!sessionReady) {
            processSession()
        }
    }, [sessionReady]);


    return (
        <>
        {
                !app?.isAuthenticated
                ?
                navigate('/login')
                :
                <>
                    {
                        !sessionReady
                        ?
                        ''
                        :
                        <main id='billing'>
                            
                            <section className='section sectionPrimary'>
                                <div className="columns is-gapless">
                                    <div className="column is-three-fifths">
                                        <h1 className="titleProp">
                                            <span className='title-bold-white text mb-5'> <span className='textLineBlue2'>FACTURACIÓN</span> </span> <br /> <img src={arrowTitle} alt="" width="80px" />
                                        </h1>
                                    </div>

                                </div>
                            </section>

                            <BillingSection user={app.user} />

                            <section className='section sectionTertiary'>
                                <div className="columns is-multiline">
                                    <div className="column is-12">
                                        <h1 className="titleProp">
                                            <div className='text'>
                                                <span className='title-border-blue'>COMPROBANTES </span>
                                                <br />
                                                <span className='title-bold-blue'>DE <span className='textLineYelow'>PAGO</span></span><br />
                                            </div>
                                        </h1>
                                    </div>
                                    <div className=" column is-12 table-container">

                                        <BillingTable vouchers={vouchers} />

                                        

                                    </div>
                                    <div className="field m-auto">
                                        <p className="control has-text-centered">
                                            <button className="button">
                                                Ver más
                                                <FontAwesomeIconSolid icon={'circle-plus ml-2'} />
                                            </button>
                                        </p>
                                    </div>

                                </div>
                            </section>


                        </main>
                    }
                </>
        }
        </>
    )
}