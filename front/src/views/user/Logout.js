import React, { useEffect, useContext } from 'react';
import { AppContext } from "../../context/AppContext";
import { useNavigate } from "react-router-dom";

export default function Logout() {
    //eslint-disable-next-line
    const { app, setApp } = useContext(AppContext);
    const navigate = useNavigate();

    const process = async () => {
        setApp({ isAuthenticated: false, user: null });
        localStorage.removeItem('token');
        navigate('/login')
    }

    useEffect( () => {
        process()
    });
    
	return (
        <>    
        </>
	);
}