import React, { useState, useContext, useEffect, useReducer } from 'react';
import { loginReducer } from '../../reducers/loginReducer';
import { AppContext } from "../../context/AppContext";
import { loginService } from '../../services/AuthService';
import { useNavigate, Link } from "react-router-dom";
import "../../assets/css/login.scss"
import arrowTitle from '../../assets/img/login/arrow_yelow_1.svg'
import Input from '../../components/inputs/Input'
import LoginRegisterNav from "../../components/navbar/LoginRegisterNav"
import { testUsers } from '../../config/index';


export default function LoginView(props) {
    const [login, dispatchLogin] = useReducer(loginReducer, { data: [], isLoading: true, isError: false });
    const navigate = useNavigate();

    //eslint-disable-next-line
    const { app, setApp } = useContext(AppContext);
    const [ email, setEmail ] = useState({ value: '', validationStatus: 'unset' });
    const [ password, setPass ] = useState({ value: '', validationStatus: 'unset' });

    const handleRedirectRole = (roleType) => {
        switch (roleType) {
            case 'admin':
                return navigate('/admin')

            case 'user':
                return navigate('/')

            case 'employee':
                return navigate('/employee')

            default:
                break;
        }
    }

    const loginProcess = async (e,devData) => {
        if(!e && props.devMode){
            let login = await loginService({ email: devData.email, password: devData.pass })
            setApp({ 
                isAuthenticated: true,
                user: login.user,
                viewsData: login.viewsData,
                role: login.role.type,
            })
            dispatchLogin({ type: 'LOGIN_FETCH_SUCCESS', payload: { token: login.token } })
            handleRedirectRole(login.role.type)
        }
        e?.preventDefault();
        dispatchLogin({ type: 'LOGIN_FETCH_INIT' })
        try {
            if (!email.validationStatus || password.value === '') {
                dispatchLogin({ type: 'LOGIN_FETCH_FAILURE', payload: 'Por favor completa los campos correctamente' })
                return
            }

            let login = await loginService({ email: email.value, password: password.value })
            if (!login) {
                dispatchLogin({ type: 'LOGIN_FETCH_FAILURE' })
                return
            }
            setApp({ 
                isAuthenticated: true,
                user: login.user,
                viewsData: login.viewsData,
                role: login.role.type,
            })
            dispatchLogin({ type: 'LOGIN_FETCH_SUCCESS', payload: { token: login.token } })
            handleRedirectRole(login.role.type)
        }
        catch (e) {
            let message = e.message
            if(message==='Network Error'){
                message = 'No se pudo realizar la conexión'
            }
            dispatchLogin({ type: 'LOGIN_FETCH_FAILURE', payload: message })
        }
    }

    useEffect(() => {
        dispatchLogin({ type: 'LOGIN_FETCH_EMPTY' })
    }, [])

    return (
        <>
            {
                login?.isLoading
                    ?
                    <div>Loading...</div>
                    :
                    <>
                        <LoginRegisterNav />
                        <main id='login'>
                            <section className='section mb-4'>

                                <div className="columns is-centered">
                                    <div className="column is-6">
                                        <h1 className="titleProp">
                                            <span className='title-border-white'>INICIAR</span> <br />
                                            <span className='title-bold-white'>SESIÓN <br /> <img src={arrowTitle} alt="" width="50px" /> </span>
                                        </h1>
                                        {
                                            !props.devMode
                                            ?
                                            null
                                            :
                                            <>
                                                <div style={{display:'flex',flexDirection:'column', width:'150px'}}>
                                                    {
                                                        testUsers.map(user=>{
                                                            return <button
                                                                    key={user.email}
                                                                    onClick={() => loginProcess(null, { email: user.email, pass: user.pass})}
                                                                    >Login with {user.email}</button>
                                                        })
                                                    }
                                                </div>
                                            </>
                                        }
                                    </div>
                                    <form className="column is-6">
                                        <div className="field">
                                            <p className="control">
                                                <Input
                                                    className={"input is-medium mb-5"}
                                                    type="text"
                                                    placeholder="Email"
                                                    setValue={setEmail}
                                                    value={email}
                                                    //eslint-disable-next-line
                                                    regexValidation={/^(([^<>()\[\]\\.,:\s@"]+(\.[^<>()\[\]\\.,:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/}
                                                />
                                            </p>
                                        </div>
                                        <div className="field">
                                            <p className="control has-icons-right">
                                                <Input
                                                    value={password}
                                                    setValue={setPass}
                                                    type={'password'}
                                                    placeholder={'Contraseña'}
                                                    //eslint-disable-next-line
                                                    regexValidation={/^(?=.*[A-Za-z])(?=.*\d)[A-Za-z\d]{6,}$/}
                                                />
                                                <span className="icon is-small is-right">
                                                    <i className="fas fa-eye"></i>
                                                </span>
                                            </p>
                                            <p className='control is-flex is-justify-content-flex-end'>
                                                <span className="help is-light">Olvidé mi contraseña</span>
                                            </p>

                                            {
                                                !login?.isError
                                                    ?
                                                    null
                                                    :
                                                    <span className="help is-danger mt-2" style={{ fontSize: '.8rem', whiteSpace: 'nowrap', backgroundColor: 'white' }} >{login.message}</span>
                                            }

                                        </div>
                                        <div className="field is-grouped is-grouped-centered">
                                            <p className="control">
                                                <button
                                                    onClick={(e) => loginProcess(e)}
                                                    className="button bg-white blue">
                                                    Iniciar sesión
                                                </button>
                                            </p>
                                        </div>

                                        <span className="mt-5 help is-light">
                                            ¿Primera vez aquí?
                                            <Link to="/register" className="yelow ml-2">
                                                Registrate ahora.
                                            </Link>
                                             
                                        </span>
                                    </form>

                                </div>


                            </section>
                        </main>
                    </>
            }
        </>
    )
}