import React, { useState, useEffect, useContext } from 'react';
import { AppContext } from "../../context/AppContext";
import arrowTitle from '../../assets/img/home/arrow_rigth.svg'
import arrowBtn from '../../assets/img/home/arrow_bottom.svg'
import lineBlue from '../../assets/img/home/bg_line-100.jpg'
import NextActivitiesSection from '../../components/home/NextActivitiesSection'
import FeaturedActivitySection from '../../components/home/FeaturedActivitySection'
import { getNewsData } from '../../services/UserService';

import "../../assets/css/home.scss"
import { useNavigate } from 'react-router-dom';

export default function HomeView(props) {
    const { app } = useContext(AppContext);
    const [sessionReady, setSessionReady] = useState(false)
    const [ newsData, setNewsData ] = useState({})
    const navigate = useNavigate();

    const processSession = async () => {
        let newsDataGet = await getNewsData()
        if(newsDataGet.status !== 'success'){
            return ''
        }
        setNewsData(newsDataGet.newsData)
        setSessionReady(true)
    }

    const checkLogin = () => {
        if(!props.test){
            if(!app?.isAuthenticated){
                return navigate('/login')
            }
        }
    }

    useEffect(() => {
        checkLogin()

        if(!sessionReady){
            processSession()
        }
    // eslint-disable-next-line
    }, [sessionReady]);

    return (
        <>
        {
            !sessionReady
            ?
            null
            :
            <main id='home'>
                <section
                    style={{
                        backgroundImage: `url(${newsData.imgPath})`
                    }}
                    className='section sectionPrimary'>

                    <div className="columns is-gapless mt-6">
                        <div className="column is-three-fifths">
                            <h1 className="titleProp">
                                <span className='title-border-white'>{newsData?.title1}</span> <br />
                                <span className='title-bold-white'>{newsData?.title2?.split(' ')[0]} <span className='textLineBlue'>{newsData?.title2?.split(' ')[1]}</span> <br /> {newsData?.title3} <img src={arrowTitle} alt="" width="80px" /> </span>
                            </h1>
                            <p className='white pt-4'>
                                {newsData?.description}
                            </p>
                            <div id='btn-more' className="field">
                                <a href={newsData?.url} className="control">
                                    <span className="button is-normal bg-white">
                                        <span className='blue'>Saber más</span>
                                    </span>
                                </a>
                            </div>
                        </div>

                    </div>

                    <div className="columns is-centered is-gaples py-0 my-0">
                        <div id='float' className="column is-8">
                            <h1 className="titleProp">
                                <div className='text'>
                                    <span className='title-border-blue'>CONOZCA </span> <img src={lineBlue} alt="" />
                                    <br />
                                    <span className='title-bold-blue'><span className='textLineYelow' >LAS</span> ACTIVIDADES </span>
                                </div>
                                <div className='btn-title'>
                                    <img src={arrowBtn} alt="" />
                                </div>
                            </h1>
                        </div>
                    </div>

                </section>

                <section className='section sectionSecondary'>

                    <div id='containerPosts' className="columns">

                        <FeaturedActivitySection />

                        <NextActivitiesSection />

                    </div>
                </section>


            </main>
        }
        </>
    )
}