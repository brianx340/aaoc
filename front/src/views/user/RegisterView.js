import React, { useState, useEffect, useReducer } from 'react';
import { registerReducer } from '../../reducers/registerReducer';
import { registerService } from '../../services'
import "../../assets/css/registerAndProfile.scss"
import LoginRegisterNav from "../../components/navbar/LoginRegisterNav"
import arrowTitle from '../../assets/img/registro/arrow_title.svg'
import Input from '../../components/inputs/Input'
import MessageModal from '../../components/custom/MessageModal'
import { useNavigate } from "react-router-dom";
import SelectGenres from '../../components/apiComponents/SelectGenres';
import SelectCountries from '../../components/apiComponents/SelectCountries';

export default function RegisterView() {
    const [ register, dispatchRegister ] = useReducer(registerReducer, { data: [], isLoading: true, isError: false });
    const navigate = useNavigate();

    //PersonalData
    const [ name, setName ] = useState({value:'', validationStatus: 'unset'});
    const [ lastName, setLastName ] = useState({value:'', validationStatus: 'unset'});
    const [ email, setEmail ] = useState({value:'', validationStatus: 'unset'});
    const [ dni, setDni ] = useState({value:'', validationStatus: 'unset'});
    const [ password, setPass ] = useState({value:'', validationStatus: 'unset'});
    const [ rePassword, setRePass ] = useState({value:'', validationStatus: 'unset'});
    const [ genre, setGenre ] = useState({value:'', validationStatus: 'unset'});
    const [ phone, setPhone ] = useState({value:'', validationStatus: 'unset'});
    //AddressData
    const [ address, setAddress ] = useState({value:'', validationStatus: 'unset'});
    const [ country, setCountry ] = useState({value:'', validationStatus: 'unset'});
    const [ province, setProvince ] = useState({value:'', validationStatus: 'unset'});
    //JobData
    const [ title, setTitle ] = useState({value:'', validationStatus: 'unset'});
    const [ profession, setProfession ] = useState({value:'', validationStatus: 'unset'});
    const [ specialty, setSpecialty ] = useState({value:'', validationStatus: 'unset'});
    const [ jobAddress, setJobAddress ] = useState({value:'', validationStatus: 'unset'});

    const registerProcess = async (e) => {
        e.preventDefault()

        dispatchRegister({ type: 'REGISTER_FETCH_INIT' })
        try{
            const registered = await registerService({
                name: name.value,
                lastName: lastName.value,
                email: email.value,
                dni: dni.value,
                password: password.value,
                genre: genre.value,
                phone: phone.value,
                address: address.value,
                country: country.value,
                province: province.value,
                title: title.value,
                profession: profession.value,
                specialty: specialty.value,
                jobAddress: jobAddress.value
            });
            if(!registered){
                dispatchRegister({ type: 'REGISTER_FETCH_FAILURE' })
                return
            }
            dispatchRegister({ type: 'REGISTER_FETCH_SUCCESS', payload: register })
            navigate('/login')
        }
        catch(e){
            console.error(e)
            dispatchRegister({ type: 'REGISTER_FETCH_FAILURE', payload: e.message })
        }
    }

    useEffect(() => {
        dispatchRegister({ type: 'REGISTER_FETCH_EMPTY' })
    }, []);

    return (
        <>
            {
                register?.isError
                ?
                <MessageModal showModal={register.isError} title={'Error!'} message={register.message} dispatch={dispatchRegister} />
                :
                ''
            }

            {
                register?.isLoading
                ?
                <div>Loading...</div>
                :
                <>
                    <LoginRegisterNav />
                    <main id='register'>
                        <section className='section'>

                            <div className="columns is-centered">
                                <div className="column is-4">
                                    <h1 className="titleProp">
                                        <span className='title-border-white'>REGISTRO</span> <br />
                                        <img src={arrowTitle} alt="" width="50px" />
                                    </h1>
                                </div>
                                <form className="column is-8">
                                    <div className="field column is-12 header-form">
                                        <div className="control">
                                            <label className="radio mb-3 has-text-white">
                                                <input type="radio" name="foobar" />
                                                Deseo hacerme socio (costo xxx por mes)
                                            </label>

                                        </div>
                                        <div className="control">
                                            <label className="radio has-text-white">
                                                <input type="radio" name="foobar" />
                                                No deseo hacerme socio
                                            </label>

                                        </div>
                                    </div>

                                    <div className="columns is-multiline is-centered">
                                        <div className="field column is-6">
                                            <p className="control">
                                                <Input
                                                    className={"input is-medium mb-5"}
                                                    type="text"
                                                    placeholder="Nombre"
                                                    setValue={setName}
                                                    value={name}
                                                    />
                                            </p>
                                        </div>
                                        <div className="field column is-6">
                                            <p className="control">
                                                <Input
                                                    className={"input is-medium mb-5"}
                                                    type="text"
                                                    placeholder="Apellido"
                                                    setValue={setLastName}
                                                    value={lastName}
                                                    />
                                            </p>
                                        </div>
                                        <div className="field column is-6">
                                            <p className="control">
                                                <Input
                                                    className={"input is-medium mb-5"}
                                                    type="text"
                                                    placeholder="Email"
                                                    setValue={setEmail}
                                                    value={email}
                                                    />
                                            </p>
                                        </div>
                                        <div className="field column is-6">
                                            <p className="control">
                                                <Input
                                                    className={"input is-medium mb-5"}
                                                    type="text"
                                                    placeholder="DNI"
                                                    setValue={setDni}
                                                    value={dni}
                                                    />
                                            </p>
                                        </div>
                                        <div className="field column is-6">
                                            <p className="control">
                                                <Input
                                                    className={"input is-medium mb-5"}
                                                    type="password"
                                                    placeholder="contraseña"
                                                    setValue={setPass}
                                                    value={password}
                                                    />
                                            </p>
                                        </div>
                                        <div className="field column is-6">
                                            <p className="control">
                                                <Input
                                                    className={"input is-medium mb-5"}
                                                    type="password"
                                                    placeholder="contraseña"
                                                    setValue={setRePass}
                                                    value={rePassword}
                                                    />
                                            </p>

                                        </div>
                                        <div className="field column is-6">
                                            <p className="control">
                                                <SelectGenres
                                                    setValue={setGenre}
                                                    value={genre}
                                                    />
                                            </p>
                                        </div>
                                        <div className="field column is-6">
                                            <p className="control">
                                                <Input
                                                    className={"input is-medium mb-5"}
                                                    type="text"
                                                    placeholder="Celular"
                                                    setValue={setPhone}
                                                    value={phone}
                                                    />
                                            </p>
                                        </div>
                                        <div className="field column is-12">
                                            <p className="control">
                                                <Input
                                                    className={"input is-medium mb-5"}
                                                    type="text"
                                                    placeholder="Dirección"
                                                    setValue={setAddress}
                                                    value={address}
                                                    />
                                            </p>
                                        </div>
                                        <div className="field column is-6">
                                            <p className="control">
                                                <SelectCountries
                                                    setValue={setCountry}
                                                    value={country}
                                                    />
                                            </p>
                                        </div>
                                        <div className="field column is-6">
                                            <p className="control">
                                                <Input
                                                    className={"input is-medium mb-5"}
                                                    type="text"
                                                    placeholder="Provincia"
                                                    setValue={setProvince}
                                                    value={province}
                                                    />
                                            </p>
                                        </div>
                                        <div className="field column is-6">
                                            <p className="control">
                                                <Input
                                                    className={"input is-medium mb-5"}
                                                    type="text"
                                                    placeholder="Titulo"
                                                    setValue={setTitle}
                                                    value={title}
                                                    />
                                            </p>
                                        </div>
                                        <div className="field column is-6">
                                            <p className="control">
                                                <Input
                                                    className={"input is-medium mb-5"}
                                                    type="text"
                                                    placeholder="Profesión"
                                                    setValue={setProfession}
                                                    value={profession}
                                                    />
                                            </p>
                                        </div>
                                        <div className="field column is-12">
                                            <p className="control">
                                                <Input
                                                    className={"input is-medium mb-5"}
                                                    type="text"
                                                    placeholder="Especialidad"
                                                    setValue={setSpecialty}
                                                    value={specialty}
                                                    />
                                            </p>
                                        </div>
                                        <div className="field column is-12 pb-5">
                                            <p className="control">
                                                <Input
                                                    className={"input is-medium mb-5"}
                                                    type="text"
                                                    placeholder="Lugar de trabajo"
                                                    setValue={setJobAddress}
                                                    value={jobAddress}
                                                    />
                                            </p>
                                        </div>

                                    </div>


                                    <div className="field column is-12 fot-register">
                                        <p className="control">
                                            <button
                                                onClick={(e)=>registerProcess(e)}
                                                className="button is-fullwidth">
                                                Registrarse
                                            </button>
                                        </p>
                                    </div>

                                </form>

                            </div>


                        </section>
                    </main>
                </>
            }

        </>
    )
}