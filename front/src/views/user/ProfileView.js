import React, { useState, useEffect, useContext } from 'react';
import { AppContext } from "../../context/AppContext";
import { useNavigate } from "react-router-dom";
import arrowTitle from '../../assets/img/registro/arrow_title.svg'
import "../../assets/css/registerAndProfile.scss"
import Input from '../../components/inputs/Input'
import ButtonPayCancelSubscription from '../../components/payments/ButtonPayCancelSubscription'
export default function ProfileView() {
    const [sessionReady, setSessionReady] = useState(false)
    const { app } = useContext(AppContext);
    const navigate = useNavigate();

    const fields = [
        {
            placeholder: 'Nombre',
            value: app?.user?.name,
            type: 'text',
            column: 'is-6'
        },
        {
            placeholder: 'Apellido',
            value: app?.user?.lastName,
            type: 'text',
            column: 'is-6'
        },
        {
            placeholder: 'Email',
            value: app?.user?.email,
            type: 'email',
            column: 'is-6'
        },
        {
            placeholder: 'DNI',
            value: app?.user?.dni,
            type: 'number',
            column: 'is-6'
        },
        {
            placeholder: 'Sexo',
            value: app?.user?.genre.name,
            type: 'text',
            column: 'is-6'
        },
        {
            placeholder: 'Celular',
            value: app?.user?.phone,
            type: 'number',
            column: 'is-6'
        },
        {
            placeholder: 'Direccion',
            value: app?.user?.address,
            type: 'text',
            column: 'is-12'
        },
        {
            placeholder: 'País',
            value: app?.user?.country.name,
            type: 'text',
            column: 'is-6'
        },
        {
            placeholder: 'Provincia',
            value: app?.user?.province,
            type: 'text',
            column: 'is-6'
        },
        {
            placeholder: 'Titulo',
            value: app?.user?.title,
            type: 'text',
            column: 'is-6'
        },
        {
            placeholder: 'Profesion',
            value: app?.user?.profession,
            type: 'text',
            column: 'is-6'
        },
        {
            placeholder: 'Especialidad',
            value: app?.user?.specialty,
            type: 'text',
            column: 'is-12'
        },
        {
            placeholder: 'Direccion Laboral',
            value: app?.user?.jobAddress,
            type: 'text',
            column: 'is-12'
        }
    ]

    useEffect(() => {
        const processSession = async () => {
            setSessionReady(true)
        }

        processSession()
    }, [sessionReady]);

    return (
        <>
         {
                !app?.isAuthenticated
                ?
                navigate('/login')
                :
                <>
                    {
                    !sessionReady
                        ?
                        'loading'
                        :
                        <>

                            <main id='profile' style={{paddingTop:'5rem'}}>
                                <section className='section'>

                                    <div className="columns is-centered">
                                        <div className="column is-4">
                                            <h1 className="titleProp">
                                                <span className='title-border-white'>MI</span> <br /> <span className='title-border-white'>PERFIL</span> <br />
                                                <img src={arrowTitle} alt="" width="50px" />
                                            </h1>
                                        </div>
                                        <form className="column is-8 my-5">

                                            <div className="columns is-multiline is-centered">
                                                
                                                <ButtonPayCancelSubscription />

                                                {
                                                    !fields
                                                    ?
                                                    ''
                                                    :
                                                    fields.map((field, index) => {
                                                        return  <div key={index} className={ `field column ${field.column}` }>
                                                                    <p className="control">
                                                                        <Input
                                                                            type={field.type}
                                                                            placeholder={field.placeholder}
                                                                            value={{
                                                                                value: field.value,
                                                                                validationStatus: 'unset'
                                                                            }}
                                                                            styles={{ height: '40px', minHeight:'unset', cursor:'default' }}
                                                                            disabled={true}/>
                                                                    </p>
                                                                </div>
                                                    })
                                                }


                                            </div>


                                            <div className="field column is-12 fot-register">
                                                <p className="control">
                                                    <button className="button is-fullwidth">
                                                        Aceptar
                                                    </button>
                                                </p>
                                            </div>

                                        </form>

                                    </div>


                                </section>
                            </main>
                        </>
                    }
                </>
        }
        </>
    )
}