import React, { useState, useEffect, useContext } from 'react';
import { AppContext } from "../../context/AppContext";
import { getCertificates } from '../../services/UserService'
import { useNavigate } from "react-router-dom";
import "../../assets/css/certificatesAndActivities.scss"
import lineBlue from '../../assets/img/home/bg_line-100.jpg'
import Certificate from '../../components/certificates/Certificate'
import BigTopSection from '../../components/sections/BigTopSection';

export default function HomeView() {
    const { app } = useContext(AppContext);
    const [ certificates, setCertificates ] = useState(null)
    const [ sessionReady, setSessionReady ] = useState(false)
    const navigate = useNavigate();

    const processSession = async () => {
        const allCertificates = await getCertificates();

        setCertificates(allCertificates)
        setSessionReady(true)
    }

    useEffect(() => {
        if(!sessionReady) {
            processSession()
        }
    }, [sessionReady]);

    return (
        <>
        {
                !app?.isAuthenticated
                ?
                navigate('/login')
                :
                <>
                    <main id='certificates'>

                        <BigTopSection type={'certificate'} />

                        <section id='sectionSecondary' className='section'>
                            <h1 className="titleProp">
                                <div className='text'>
                                    <span className='title-border-blue'>TODOS</span> <img src={lineBlue} alt="" />
                                    <br />
                                    <span className='title-bold-blue'>MIS <span className='textLineBlue' >CERTIFICADOS</span> </span>
                                </div>
                            </h1>
                            <div className="columns is-multiline is-centered">

                                {   
                                    !certificates
                                    ?
                                    ''
                                    :
                                    certificates.map((certificate, index) => {
                                        return <Certificate key={index} certificate={certificate} />
                                    })
                                }

                            </div>
                        </section>

                    </main>
                </>
        }
        </>
    )
}