import React, { useContext, useEffect, useState } from 'react';
import { AppContext } from "../../context/AppContext";
import "../../assets/css/certificatesAndActivities.scss"
import UserAndOtherActivities from '../../components/myActivities/UserAndOtherActivities'
import BigTopSection from '../../components/sections/BigTopSection';
import { useNavigate } from "react-router-dom";

export default function ActivitiesView() {
    const { app } = useContext(AppContext);
    const navigate = useNavigate();
    const [ sessionReady, setSessionReady ] = useState(false);

    const processSession = async () => {
        setSessionReady(true)
    }

    useEffect(() => {
        if(!sessionReady){
            processSession()
        }
    }, [sessionReady]);

    return (
        <>
        {
                !app?.isAuthenticated
                ?
                navigate('/login')
                :
                <>
                    <main id='activities'>

                        <BigTopSection type={'activity'} />

                        <UserAndOtherActivities />

                    </main>
                </>
        }
        </>
    )
}