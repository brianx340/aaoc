import SidebarSection from "../../components/aaocPanel/SidebarSection";
import QrChecker from '../../components/employee/QrChecker';

export default function EmployeeHome() {

    return (
        <>
            <div style={{ height: '100vh', }}>

                <SidebarSection
                    title='Inicio'
                    children={<QrChecker/>}
                />

            </div>
        </>
    )

}