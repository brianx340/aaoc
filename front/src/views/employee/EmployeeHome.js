import SidebarSection from "../../components/aaocPanel/SidebarSection";
import Home from '../../components/employee/Home';

export default function EmployeeHome() {

    return (
        <>
            <div style={{ height: '100vh', }}>

                <SidebarSection
                    title='Inicio'
                    children={<Home/>}
                />

            </div>
        </>
    )

}