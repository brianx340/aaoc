import SidebarSection from "../../components/aaocPanel/SidebarSection";
import ListClients from '../../components/admin/ListClients';

export default function AdminListClients() {

    return (
        <>
            <div style={{ height: '100vh', }}>

                <SidebarSection
                    title='Inicio'
                    children={<ListClients/>}
                />

            </div>
        </>
    )

}