import SidebarSection from "../../components/aaocPanel/SidebarSection";
import Home from '../../components/admin/Home';

export default function AdminHome() {

    return (
        <>
            <div style={{ height: '100vh', }}>

                <SidebarSection
                    title='Inicio'
                    children={<Home/>}
                />

            </div>
        </>
    )

}