import SidebarSection from "../../components/aaocPanel/SidebarSection";
import ListActivity from '../../components/admin/ListActivity';

export default function AdminListActivities() {

    return (
        <>
            <div style={{ height: '100vh', }}>

                <SidebarSection
                    title='Inicio'
                    children={<ListActivity/>}
                />

            </div>
        </>
    )

}