import SidebarSection from "../../components/aaocPanel/SidebarSection";
import AdmitedCountries from '../../components/admin/AdmitedCountries';

export default function AdminAddAdmitCountries() {

    return (
        <>
            <div style={{ height: '100vh', }}>

                <SidebarSection
                    title='Inicio'
                    children={<AdmitedCountries/>}
                />

            </div>
        </>
    )

}