import SidebarSection from "../../components/aaocPanel/SidebarSection";
import CreateActivity from '../../components/admin/CreateActivity';

export default function AdminModifyBannerView() {

    return (
        <>
            <div style={{ height: '100vh', }}>

                <SidebarSection
                    title='Inicio'
                    children={<CreateActivity/>}
                />

            </div>
        </>
    )

}