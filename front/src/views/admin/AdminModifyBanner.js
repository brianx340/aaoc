import SidebarSection from "../../components/aaocPanel/SidebarSection";
import HomeBannerModifier from '../../components/admin/HomeBannerModifier';

export default function AdminModifyBanner() {

    return (
        <>
            <div style={{ height: '100vh', }}>

                <SidebarSection
                    title='Inicio'
                    children={<HomeBannerModifier/>}
                />

            </div>
        </>
    )

}