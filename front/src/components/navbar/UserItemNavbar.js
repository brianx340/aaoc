import { Link } from 'react-router-dom';

import FontAwesomeIconRegular from '../custom/FontAwesomeIconRegular';
export default function UserItemNavbar(props) {
    return (
        <>
            <Link to="/" id='userItem' className="navbar-item bg-blue">
                <span className="icon-text">

                    <span className="icon mr-1">
                        <FontAwesomeIconRegular icon='circle-user white' />
                    </span>
                    <span className='white mb-1'>{ props.user && props.user.name ? props.user.name : 'Usuario' }</span>
                </span>
            </Link>
        </>
    )
}