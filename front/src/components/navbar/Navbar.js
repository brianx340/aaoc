import React, { useState, useEffect, useContext } from 'react';
import { AppContext } from "../../context/AppContext";

import "../../assets/css/navbar.scss"
import NavbarOption from './NavbarOption'
import BurgerMenu from './BurgerMenu'
import LogoNavbar from './LogoNavbar'
import UserItemNavbar from './UserItemNavbar'
import AaocTvItem from './AaocTvItem'

export default function Navbar() {
    const [ sessionReady, setSessionReady ] = useState(false)
    const { app } = useContext(AppContext);

    const processSession = async () => {
        setSessionReady(true)
    }

    useEffect(() => {
        if(!sessionReady) {
            processSession()
        }
    });

    const navbarOptions = [
        {
            name: 'Inicio',
            link: '/'
        },
        {
            name: 'Mi Perfil',
            link: '/profile'
        },
        {
            name: 'Mis Actividades',
            link: '/activities'
        },
        {
            name: 'Certificados',
            link: '/certificates'
        },
        {
            name: 'Facturación',
            link: '/billing'
        },
        {
            name: 'Cerrar Sesión',
            link: '/logout'
        }
    ]

    return (
        <>
        {
                !app?.isAuthenticated
                ?
                null
                :
                <>
                <header id="menuNav" style={{width:'100%'}} className="">
                    <nav className="navbar py_5" role="navigation" aria-label="main navigation">
                        <div className="navbar-brand ml-2">
                            <LogoNavbar />
                            <BurgerMenu />
                        </div>
                        <div className="navbar-menu mr-2" id="navMenu">

                            <div className="navbar-end py-5">

                                {
                                    navbarOptions.map((option, index) => {
                                        return <NavbarOption key={index} link={option.link} name={option.name} />
                                    })
                                }

                                <AaocTvItem />

                                <UserItemNavbar user={ app.user ? app.user : '' } />

                            </div>
                        </div>
                    </nav>
                </header>
                </>
        }   
        </>
    )

}