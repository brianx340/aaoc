import { Link } from 'react-router-dom';
export default function NavbarOption(props) {
    return (
        <>
            <Link to={props.link} className="navbar-item">
                <span>{props.name}</span>
            </Link>
        </>
    )
}