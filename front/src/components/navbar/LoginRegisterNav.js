import Logo from '../../assets/img/navbar/logo.svg'
import { Link } from 'react-router-dom'

export default function LoginRegisterNav(props) {
    return (
        <>
            <header id="menuNav" className="container is-fluid">
                <nav className="navbar py_5" role="navigation" aria-label="main navigation">
                    <div className="navbar-brand ml-2">

                        <Link to="/" className="navbar-item">
                            <img src={Logo} alt='logo' width="150" height="150" />
                        </Link>

                        <p role="button" className="navbar-burger" data-target="navMenu" aria-label="menu" aria-expanded="false">
                            <span aria-hidden="true"></span>
                            <span aria-hidden="true"></span>
                            <span aria-hidden="true"></span>
                        </p>
                        
                    </div>
                    <div className="navbar-menu mr-2" id="navMenu">

                    </div>
                </nav>
            </header>
        </>
    )
}