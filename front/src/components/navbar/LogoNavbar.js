import { Link } from 'react-router-dom';
import Logo from '../../assets/img/navbar/logo.svg'

export default function LogoNavbar(props) {
    return (
        <>
            <Link to="/" className="navbar-item">
                <img src={Logo} alt='logo' />
            </Link>
        </>
    )
}