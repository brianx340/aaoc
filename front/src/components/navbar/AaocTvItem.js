//import { Link } from 'react-router-dom';
import FontAwesomeIconSolid from '../custom/FontAwesomeIconSolid'

export default function AaocTvItem(props) {
    return (
        <>
            <span id='tvItem' className="navbar-item">
                <span className="icon-text">
                    <span className="icon mr-2">
                        <FontAwesomeIconSolid icon={'tv'} />
                    </span>
                    <span className='mb-1'>AAOC Tv</span>
                </span>
            </span>
        </>
    )
}