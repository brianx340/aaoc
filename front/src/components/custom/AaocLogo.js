export default function AaocLogo(props) {

    return (
        <>
            <span
                style={{...props.styles}}
                className={`mm svg-logo ${props.className}`}
                ></span>
        </>
    )
}