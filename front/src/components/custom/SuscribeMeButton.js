import React, { useReducer } from 'react';
import { axiosReducer } from '../../reducers/axiosReducer';
import { suscribeMeQuery } from '../../services/UserService';
import FontAwesomeIconRegular from '../custom/FontAwesomeIconRegular'
import PopUpMessage from '../custom/PopUpMessage';

export default function SuscribeMeButton(props) {
    const [ axiosDp, dispatchAxios ] = useReducer(axiosReducer, { data: [], isLoading: true, isError: false });


    const suscribeMe = async (e) => {
        e.preventDefault()
        const activityId = props.activityId
    
        try{
            let data = await suscribeMeQuery({activityId})
            if(data.status !== 'success'){
                return dispatchAxios({ type: 'FETCH_FAILURE', payload: { message: data.message} })
            }
            return dispatchAxios({ type: 'FETCH_SUCCESS', payload: { message: 'Suscrito con éxito!' } })
        }
        catch(e){
            console.error(e)
            return dispatchAxios({ type: 'FETCH_FAILURE', payload: e.message})
        }
    }

    return (
        <>               
            <>
                <button
                    onClick={(e)=> suscribeMe(e)}
                    className="button mr-5 blue">
                    <FontAwesomeIconRegular icon={'calendar mr-1'} />
                    Agendar evento
                </button>
            </>

            {
                axiosDp.isError
                ?
                <PopUpMessage title={'Error'} type={'danger'} message={axiosDp.message} refresh={true} refreshSession={props.refreshSession}/>
                :
                null
            }

            {
                axiosDp.isSuccess
                ?
                <PopUpMessage title={':D'} type={'success'} message={axiosDp.message} refresh={true} refreshSession={props.refreshSession}/>
                :
                null
            }
        </>
    )
}