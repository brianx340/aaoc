import React, { useEffect, useReducer } from 'react';
import { cancelActivityQuery } from '../../services/AdminService';
import { axiosReducer } from '../../reducers/axiosReducer';
import LoadingGif from './LoadingGif';
import PopUpMessage from './PopUpMessage';

export default function ConfirmCancelActivityModal(props) {
    const [ axiosDp, dispatchAxios ] = useReducer(axiosReducer, { data: [], isLoading: true, isError: false });
    
    const cancelActivity = async (activityId) => {
        dispatchAxios({ type: 'FETCH_INIT' })
        try{
            let response = await cancelActivityQuery(activityId);
            props.setShow(false)
            if(response.status !== 'success'){
                return dispatchAxios({ type: 'FETCH_FAILURE', payload: { message: 'Ups! no se han podido cancelar la actividad.'} })
            }
            return dispatchAxios({ type: 'FETCH_SUCCESS', payload: { message: 'Se ha cancelado correctamente' } })
        }
        catch(e){
            console.error(e);
            props.setShow(false)
            return dispatchAxios({ type: 'FETCH_FAILURE', payload: { message: e.message}})
        }
    }

    useEffect(() => {
        dispatchAxios({ type: 'FETCH_EMPTY' })
    }, [])

    return (
        <>
            {
                axiosDp.isError
                ?
                <PopUpMessage title={'Error'} type={'danger'} message={axiosDp.message} />
                :
                null
            }

            {
                axiosDp.isSuccess
                ?
                <PopUpMessage title={':D'} type={'success'} message={axiosDp.message}/>
                :
                null
            }

            <div className={!props.show ? "modal" : "modal is-active"}>
                <div className="modal-background"></div>
                <div className="modal-card">
                {
                    axiosDp.isLoading
                    ?
                    <div style={{margin:'auto'}}>
                        <LoadingGif />
                    </div>
                    :
                    <>
                        <header className="modal-card-head">
                            <p className="modal-card-title">Cancelar la actividad?</p>
                            <button
                                onClick={() => props.setShow(false)}
                                className="delete"
                                aria-label="close"></button>
                        </header>
                        <footer className="modal-card-foot is-justify-content-space-evenly">
                            <button
                                onClick={() => cancelActivity(props.activityId)}
                                className="button is-danger">
                                Cancelar
                            </button>
                            <button
                                onClick={() => props.setShow(false)}
                                className="button">
                                Volver
                            </button>
                        </footer>
                    </>
                }    
                </div>
            </div>
        </>
    )
}