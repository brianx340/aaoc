import React, { useState, useEffect } from 'react'
import Button from '../inputs/Button'

export default function DownloadCertificateButton(props) {
    const [sessionReady, setSessionReady] = useState(false)

    const processSession = async () => {
        setSessionReady(true)
    }

    useEffect(() => {
        processSession()
    }, [sessionReady]);

    return (
        <>
            {
                !sessionReady
                    ?
                    ''
                    :
                    <Button
                        styles={{
                            ...props.styles
                        }}
                        onClick={props.onClick ? props.onClick : ''}
                        name={'Descargar certificado'}
                        className={'bg-skyBlue white'} />
            }
        </>
    )

}