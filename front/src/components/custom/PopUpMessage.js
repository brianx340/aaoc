import React, { useState } from 'react';

export default function PopUpMessage(props) {
    const [ show, setShow ] = useState(1);

    const redir = async () => {
        if(props.refresh){
            props.refreshSession()
        }
    }

    return (
        <>
            <div className={ show ? "modal is-active" : "modal" }>
                <div className="modal-background"></div>
                <div className="modal-content">

                    <article className={`message is-${props.type}`}>
                        <div className="message-header">

                            <p>{props.title}</p>

                            <button
                                onClick={() => {
                                    setShow(0)
                                    redir()
                                }}
                                className="delete" aria-label="delete"></button>

                        </div>
                        <div className="message-body">
                            {props.message}
                        </div>
                    </article>
                </div>
            </div>
        </>
    )

}