import React, { useState, useEffect } from 'react'
import FontAwesomeIconRegular from '../custom/FontAwesomeIconRegular'

export default function HourHHMM(props) {
    const [date, setDate] = useState('')
    const [sessionReady, setSessionReady] = useState(false)

    const processSession = async () => {
        if (!props.date) {
            return
        }

        let sDate = JSON.stringify(props.date)
        sDate = sDate.substring(12, 14) + ':' + sDate.substring(15, 17) + 'hs'
        setDate(sDate)
        setSessionReady(true)
    }

    useEffect(() => {
        processSession()
    });

    return (
        <>
            {
                !sessionReady || date === ''
                    ?
                    ''
                    :
                    <span className={`buttonFeatured ${props.colorDates}`}>
                        <FontAwesomeIconRegular icon='clock' />
                        {date}
                    </span>
            }
        </>
    )

}