import React, { useEffect, useState } from 'react';

export default function LoadingText(props) {
    const [ text, setText ] = useState('');

    const writing = (str) => {
        let arrFromStr = str.split('');
        let i = 0;
        setInterval(() => {
            if (i < arrFromStr.length) {
                setText(str.slice(0,i) + arrFromStr[i]);
                i++;
            } else {
                setText('');
                i = 0;
            }
        }, 100);

    }

    useEffect(() => {
        writing('Cargando...')
    },[])

    return (
        <>
           <span>{text}<br></br></span>
        </>
    )
}
