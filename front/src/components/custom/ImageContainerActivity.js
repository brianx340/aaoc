import { API_URL } from '../../config/index';
export default function ImageContainerActivity(props) {
    return (
        <>
            <figure className='image' style={{ ...props.styles }}>
                <img src={`${API_URL}/public/img/${props.src}`} alt="" />
            </figure>
        </>
    )
}