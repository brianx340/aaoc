import Loading from '../../assets/loading.gif'
export default function LoadingGif(props) {
    return (
        <>
           <figure className='image'>
                <img src={Loading} alt='Loading...' />
           </figure>
        </>
    )
}
