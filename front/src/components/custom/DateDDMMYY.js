import React, { useState, useEffect } from 'react'
import FontAwesomeIconRegular from './FontAwesomeIconRegular'
export default function DateDDMMYY(props) {
    const [date, setDate] = useState('')
    const [sessionReady, setSessionReady] = useState(false)

    const processSession = async () => {
        if (!props.date) {
            return
        }

        let sDate = JSON.stringify(props.date)
        sDate = sDate.substring(9, 11) + '/' + sDate.substring(6, 8) + '/' + sDate.substring(3, 5)

        setDate(sDate)
        setSessionReady(true)
    }

    useEffect(() => {
        processSession()
    });

    return (
        <>
            {
                !sessionReady || date === ''
                    ?
                    ''
                    :
                    <span className={`buttonFeatured ${props.colorDates}`}>
                        <FontAwesomeIconRegular icon={'calendar'} />
                        {date}
                    </span>
            }
        </>
    )

}