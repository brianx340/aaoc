import React, { useState, useEffect } from 'react'
export default function FullDate(props) {
    const [date, setDate] = useState('')
    const [hour, setHour] = useState('')
    const [sessionReady, setSessionReady] = useState(false)

    const processSession = async () => {
        if (!props.date) {
            return
        }
        let sDate = JSON.stringify(props.date)
        let hhmm = sDate.substring(12, 14) + ':' + sDate.substring(15, 17) + 'hs'
        let ddmmaa = sDate.substring(9, 11) + '/' + sDate.substring(6, 8) + '/' + sDate.substring(3, 5)
        setHour(hhmm)
        setDate(ddmmaa)
        setSessionReady(true)
    }

    useEffect(() => {
        processSession()
    });

    return (
        <>
            {
                !sessionReady || date === ''
                    ?
                    ''
                    :
                    <span style={{cursor:'default', fontSize:'1rem'}} className={`${props.colorDates}`}>
                        {
                            `${date} - ${hour}`
                        }
                    </span>
            }
        </>
    )

}