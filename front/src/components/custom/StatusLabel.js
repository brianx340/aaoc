export default function StatusLabel(props) {
    var { status, style } = props.status;
    return (
        <>
            <button
                style={{
                    width: '100%',
                }}
                className={`button is-${style}`}>
                    { status ? status : 'N/A' }
            </button>
        </>
    )
}