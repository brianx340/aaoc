import DateDDMMYY from "./DateDDMMYY"
import HourHHMM from "./HourHHMM"

export default function DuoDate(props) {
    return (
        <>
            {
                !props.date
                    ?
                    null
                    :
                    <div
                        style={{
                            ...props.style
                        }}
                        className="control date mt-2">
                        <DateDDMMYY date={props.date} colorDates={props.colorDates} />
                        <HourHHMM date={props.date} colorDates={props.colorDates} />
                    </div>
            }
        </>
    )

}