export default function FontAwesomeIconRegular(props) {
    return (
        <>
            <i className={`fa-regular fa-${props.icon}`}></i>
        </>
    )
}