export default function FontAwesomeIconSolid(props) {
    return (
        <>
            <i
                onClick={props.onClick}
                style={{
                    ...props.styles
                }}
                className={`fa-solid fa-${props.icon} ${props.className ? props.className : ''}`}></i>
        </>
    )
}