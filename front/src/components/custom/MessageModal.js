export default function MessageModal(props) {

    return (
        <>
            <div className={props.showModal ? "modal is-active" : "modal"}>
                <div className="modal-background"></div>
                <div className="modal-card" >
                    <section
                        style={{ backgroundColor: '#0258a0' }}
                        className="modal-card-body">
                        <div className="columns is-mobile">
                            <div className="column is-12"> <h2 className='white pb-4'>{props.title}</h2>
                                <div className="field is-grouped mt-2 white is-flex is-justify-content-center">
                                    <p>{props.message}</p>
                                </div>
                            </div>

                        <button onClick={() => props.dispatch({type:'CLEAR_MESSAGE'})} className="modal-close is-large" aria-label="close"></button>
                        </div>
                    </section>
                </div>
            </div>
        </>
    )

}