import React, { useState } from 'react';
import FontAwesomeIconSolid from '../../custom/FontAwesomeIconSolid'
import Input from '../../inputs/Input'

export default function Search(props) {
    const [search, setSearch ] = useState({ value: '', validationStatus: 'unset' });


    return (
        <>     
            <div
                style={{
                    height: 'fit-content',
                    display: 'flex',
                    justifyContent: 'center',
                    alignItems: 'center',
                }}
                >
                <Input
                    styles={{
                        minHeight: 'unset',
                        height: '30px ',
                        backgroundColor: 'transparent',
                        color:'white',
                        borderBottom: '1px solid white',
                    }}
                    type="text"
                    placeholder="Buscar..."
                    setValue={setSearch}
                    value={search}
                />
                <FontAwesomeIconSolid styles={{color:'white', marginLeft:'5px'}} icon={'search'} />
            </div>
        </>
    )

}