import { Link } from "react-router-dom";
import Search from './Search'
import AaocLogo from '../../custom/AaocLogo';

export default function AdminNavbar(props) {
    return (
        <>
            <div
                style={{backgroundColor:'#30c5f3', height: '70px'}}
                className='columns p-0 m-0'
                >

                <div className='column is-2'>
                    <AaocLogo
                        styles={{
                            width: '100px',
                            height: '100%',
                        }}
                        className={'m-auto is-block'}
                        />
                </div>

                <div className='column is-8 p-0'>

                    <div style={{height: '100%'}} className='columns p-0 m-0 is-flex is-justify-content-space-between'>
                        <div
                            style={{height: '100%'}}
                            className='column is-3 p-0 is-align-items-center is-flex'>
                            {/* search section */}

                            <Search />

                            
                        </div>

                        <div
                            style={{
                                display: 'flex',
                                justifyContent: 'center',
                                alignItems: 'center',
                            }}
                            className='column is-3 p-0'>
                            <Link
                                style={{
                                    color: 'white',
                                    display: 'flex',
                                }}
                                to='/logout'>
                                Cerrar Sesión
                            </Link>
                        </div>
                    </div>
                    
                </div>

                <div className='column is-2'>
                    <AaocLogo
                        styles={{
                            width: '100px',
                            height: '100%',
                        }}
                        className={'m-auto is-block'}
                        />
                </div>
                
            </div>
        </>
    )

}