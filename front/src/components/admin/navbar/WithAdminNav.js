import React from 'react';
import NavBar from './adminNavbar';
import { Outlet } from 'react-router';

export default function WithAdminNav() {
	//verificar rol
	return (
		<>
			<NavBar />
			<Outlet />
		</>
	)
}