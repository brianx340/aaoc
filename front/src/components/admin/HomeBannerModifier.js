import React, { useEffect, useState, useReducer } from 'react';
import { putBannerReducer } from '../../reducers/putBannerReducer';
import ImageUploader from './ImageUploader';
import Input from '../inputs/Input';
import Textarea from '../inputs/Textarea';
import { getHomeBannerData, putBannerQuery } from '../../services/AdminService';
import LoadingGif from '../custom/LoadingGif';
//import HomeView from '../../views/user/HomeView';

export default function HomeBannerModifier(props) {
    const [ putBanner, dispatchPutBanner ] = useReducer(putBannerReducer, { data: [], isLoading: true, isError: false });
    const [ sessionReady, setSessionReady ] = useState(false)
    const [ titulo1, setTitulo1 ] = useState({ value: 'PRECEPTORIA', validationStatus: 'unset' });
    const [ titulo2, setTitulo2 ] = useState({ value: 'SISTEMA NERVIOSO', validationStatus: 'unset' });
    const [ titulo3, setTitulo3 ] = useState({ value: 'CENTRAL', validationStatus: 'unset' });
    const [ description, setDescription ] = useState({ value: '', validationStatus: 'unset' });
    const [ image , setImage ] = useState(null);
    const [ imageToSend , setImageToSend ] = useState(null);
    const [ imageUrl , setImageUrl ] = useState(null);
    const [ url, setUrl ] = useState('');

    const delay = ms => new Promise(res => setTimeout(res, ms));

    const processBanner = async () => {
        let { title1, title2, title3, description, imgPath, url } = await getHomeBannerData();
        setTitulo1({ value: title1, validationStatus: 'unset' });
        setTitulo2({ value: title2, validationStatus: 'unset' });
        setTitulo3({ value: title3, validationStatus: 'unset' });
        setDescription({ value: description, validationStatus: 'unset' });
        setImageUrl(imgPath);
        setUrl(url);
        setSessionReady(true);
    }

    const changeBanner = async (e) => {
        e.preventDefault();
        const formData = new FormData();
        formData.append('title1', titulo1.value);
        formData.append('title2', titulo2.value);
        formData.append('title3', titulo3.value);
        formData.append('description', description.value);
        formData.append('image', imageToSend);
        formData.append('url', url);
        dispatchPutBanner({ type: 'PUT_BANNER_FETCH_INIT' })
        try{
            await putBannerQuery(formData);
            await delay(1000)
            dispatchPutBanner({ type: 'PUT_BANNER_FETCH_SUCCESS' })
        }
        catch(e){
            dispatchPutBanner({ type: 'PUT_BANNER_FETCH_FAILURE', payload: e.message })
        }
    }

    useEffect(() => {
        dispatchPutBanner({ type: 'PUT_BANNER_FETCH_EMPTY' })
        if(!sessionReady){
            processBanner();
        }
    } , [sessionReady]);

    return (
        <>
            {
                putBanner.isLoading
                ?
                <div
                    className='columns'
                    style={{
                        maxHeight:'400px',
                        padding:'1rem',
                        display:'flex',
                        justifyContent:'center',
                        alignItems:'center',
                        }}>
                    <LoadingGif />
                </div>
                :
                <>
                    <div className='columns' style={{maxHeight:'400px', padding:'1rem'}}>

                    
                        <div className='column is-6' style= {{width:'50%'}}>
                            <div style={{height:'30%'}}>

                                <div style={{width:'100%', justifyContent:'center', fontSize: '1.4rem' }} className='is-flex'>
                                    <span className='mr-3'>
                                        Titulo 1:
                                    </span>
                                    <Input
                                        type="text"
                                        placeholder="Titulo 1:"
                                        setValue={setTitulo1}
                                        value={titulo1}
                                        stylesUnset={true}
                                    />
                                </div>

                                <div style={{width:'100%', justifyContent:'center', fontSize: '1.4rem' }} className='is-flex'>
                                    <span className='mr-3'>
                                        Titulo 2:
                                    </span>
                                    <Input
                                        type="text"
                                        placeholder="Titulo 2:"
                                        setValue={setTitulo2}
                                        value={titulo2}
                                        stylesUnset={true}
                                    />
                                </div>

                                <div style={{width:'100%', justifyContent:'center', fontSize: '1.4rem' }} className='is-flex'>
                                    <span className='mr-3'>
                                        Titulo 3:
                                    </span>
                                    <Input
                                        type="text"
                                        placeholder="Titulo 3:"
                                        setValue={setTitulo3}
                                        value={titulo3}
                                        stylesUnset={true}
                                    />
                                </div>

                            </div>

                            <Textarea
                                divStyles={{width:'100%', height:'70%', padding:'5px'}}
                                styles={{width:'100%', resize:'none', padding:'.8rem', fontSize: '1.3rem'}}
                                placeholder="Descripción"
                                setValue={setDescription}
                                value={description}
                                maxLength={200}
                            />

                        </div>
                        
                        <ImageUploader imageUrl={imageUrl} image={image} setImage={setImage} setImageToSend={setImageToSend} />
                    </div>

                    

                    <button
                        className='m-auto is-block'
                        onClick={changeBanner}
                        >
                        Actualizar
                    </button>

                   

                </>
            }
        </>
    )
}

/*
 <div>
    <h2>Preview</h2>
    <div
        style={{transform: 'scale(.9)'}}
        >
        <HomeView test={true} />
    </div>
</div>
*/