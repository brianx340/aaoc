import React, { useEffect, useReducer } from 'react';
import { setFeaturedActivityQuery } from '../../../services/AdminService';
import FeaturedActivitySection from '../../home/FeaturedActivitySection';

import { axiosReducer } from '../../../reducers/axiosReducer';
import LoadingGif from '../../custom/LoadingGif';
import PopUpMessage from '../../custom/PopUpMessage';

export default function FeaturedActivityConfirmModal(props) {
    const [ axiosDp, dispatchAxios ] = useReducer(axiosReducer, { data: [], isLoading: true, isError: false });

    const setFeaturedActivity = async (activityId) => {
        dispatchAxios({ type: 'FETCH_INIT' })
        try{
            let response = await setFeaturedActivityQuery(activityId);
            props.setShow(false)
            if(response.status !== 'success'){
                return dispatchAxios({ type: 'FETCH_FAILURE', payload: { message: 'Ups! no se han podido establecer la actividad.'} })
            }
            return dispatchAxios({ type: 'FETCH_SUCCESS', payload: { message: 'Se ha establecido correctamente' } })
        }
        catch(e){
            console.error(e);
            props.setShow(false)
            return dispatchAxios({ type: 'FETCH_FAILURE', payload: { message: e.message}})
        }
    }

    useEffect(() => {
        dispatchAxios({ type: 'FETCH_EMPTY' })
    }, [])

    return (
        <>
            {
                axiosDp.isError
                ?
                <PopUpMessage title={'Error'} type={'danger'} message={axiosDp.message} />
                :
                null
            }

            {
                axiosDp.isSuccess
                ?
                <PopUpMessage title={':D'} type={'success'} message={axiosDp.message} />
                :
                null
            }

            <div className={!props.show ? "modal" : "modal is-active"}>
                <div className="modal-background"></div>
                <div className="modal-card">
                {
                    axiosDp.isLoading
                    ?
                    <div style={{margin:'auto'}}>
                        <LoadingGif />
                    </div>
                    :
                    <>
                        <header className="modal-card-head">
                            <p className="modal-card-title">Confirmar cambios</p>
                            <button
                                onClick={() => props.setShow(false)}
                                className="delete"
                                aria-label="close"></button>
                        </header>
                        <section className="modal-card-body">
                            <FeaturedActivitySection adminPreview={true} activity={props.activity} />
                        </section>
                        <footer className="modal-card-foot">
                            <button
                                onClick={() => setFeaturedActivity(props.activity._id)}
                                className="button is-success">
                                Establecer
                            </button>
                            <button
                                onClick={() => props.setShow(false)}
                                className="button">
                                Cancelar
                            </button>
                        </footer>
                    </>
                }    
                </div>
            </div>
        </>
    )
}