import React, { useEffect } from 'react';

import FullDate from "../../custom/FullDate"
import StatusLabel from "../../custom/StatusLabel"
import OptionsActivity from "./OptionsActivity"
export default function TrTable(props) {

    useEffect(() => {
        if(props.isSearch){
            props.setPage(props.activities.length);
        }
    })

    return (
        <>
           {
                props.activities?.slice(props.page*props.rowPerPage, (props.page+1)*props.rowPerPage).map((activity, index) => {
                    return  <tr key={index} style={{fontSize:'1rem', height:'50px', whiteSpace: 'nowrap'}}>
    
                                <td>
                                    <div style={props.styles.thDiv}>
                                        {index + 1 + (props.page*props.rowPerPage)}
                                    </div>
                                </td>
    
                                <td>
                                    <div style={props.styles.thDiv}>
                                        {activity.title}
                                    </div>
                                </td>
    
                                <td>
                                    <div style={props.styles.thDiv}>
                                        <FullDate date={activity.startDate} />
                                    </div>
                                </td>
    
                                <td>
                                    <div style={props.styles.thDiv}>
                                        <StatusLabel status={activity.status} />
                                    </div>
                                </td>
    
                                <td>
                                    <div style={{...props.styles.thDiv, justifyContent:'flex-end', paddingRight:'0'}}>
                                        <OptionsActivity activity={activity} />
                                    </div>
                                </td>
                            </tr>
                })    
           }
        </>
    )
}