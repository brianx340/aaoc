import React, { useState } from 'react';

import ActivityOption from './ActivityOption';
import FeaturedActivityConfirmModal from './FeaturedActivityConfirmModal';
import ConfirmCancelActivityModal from '../../custom/ConfirmCancelActivityModal';
import ArticleModal from '../../../components/myActivities/ArticleModal'


export default function OptionsActivity(props) {
    const [showDropdown, setShowDropdown] = useState(false);
    const [showConfirmSetFeaturedModal, setShowConfirmSetFeaturedModal] = useState(false);
    const [showConfirmDelete, setShowConfirmDeleteModal] = useState(false);
    const [showViewDetail, setShowViewDetail] = useState(false);

    return (
        <>
            <div className={!showDropdown ? "dropdown" : "dropdown is-active"}>
                <div className="dropdown-trigger">
                    <button
                        onClick={() => setShowDropdown(!showDropdown)}
                        className="button"
                        aria-haspopup="true"
                        aria-controls="dropdown-menu">

                        <span>Opciones</span>
                        <span className="icon is-small">
                            <i className="fas fa-angle-down" aria-hidden="true"></i>
                        </span>

                    </button>
                </div>
                <div className="dropdown-menu" id="dropdown-menu" role="menu">
                    <div className="dropdown-content">

                        <ActivityOption
                            onClickAction={() => setShowViewDetail(true)}
                            title={'Ver detalles'} />

                        {
                            props.activity.status !== 'inactive'
                                && props.activity.status !== 'cancelled'
                                    && props.activity.status !== 'finished'
                            ?
                            <ActivityOption
                                    title={'Marcar como Destacada'}
                                    onClickAction={() => {
                                        setShowConfirmSetFeaturedModal(true)
                                        setShowDropdown(false)
                                    }} />
                            :
                            null
                        }
                        

                        {
                            props.activity.status !== 'cancelled'
                                && props.activity.status !== 'finished'
                            ?
                            <>
                                <ActivityOption
                                    title={'Cancelar'}
                                    onClickAction={() => {
                                        setShowConfirmDeleteModal(true)
                                        setShowDropdown(false)
                                    }} />
                            </>
                            :
                            null
                        }
                        
                    </div>
                </div>
            </div>

            <FeaturedActivityConfirmModal show={showConfirmSetFeaturedModal} setShow={setShowConfirmSetFeaturedModal} activity={props.activity}/>

            <ConfirmCancelActivityModal show={showConfirmDelete} setShow={setShowConfirmDeleteModal} activityId={props.activity._id} />

            <ArticleModal isSubscribed={1} activity={props.activity} showModal={showViewDetail}  setShowModal={setShowViewDetail} />

        </>
    )

}