import FontAwesomeIconSolid from '../../custom/FontAwesomeIconSolid';

export default function TableSearchBar(props) {
    return (
        <>
            <div
                style={{
                    width: '100%',
                    height: '60px',
                    backgroundColor: '#fafafa',
                    borderRadius: '5px',
                }}
                className='mb-5 p-3 is-flex is-align-items-center'>

                <FontAwesomeIconSolid
                    styles={{color:'#4a4a4a', marginLeft:'5px'}}
                    icon={'search'}
                    className={'mr-3'}/>

                <input
                    onChange={(e) => {
                        props.setSearch(e.target.value);
                    }}
                    style={{
                        width: '100%',
                        height: '100%',
                        outline: 'none',
                        border: 'none',
                        backgroundColor: '#fafafa',
                        fontSize: '1.2rem',
                        color: '#4a4a4a',
                    }}
                    placeholder='Buscar...'
                    type='text'
                    value={props.search}
                    />

                <div
                    style={{
                        width:'20%',
                        height:'100%',
                        display:'flex',
                        justifyContent:'space-between',
                        alignItems:'center',
                        borderLeft:'1px solid #959595',
                        paddingLeft:'5px',
                    }}
                    >

                    <span className='mr-2'>Filas</span>

                    <FontAwesomeIconSolid
                        onClick={() =>  props.rowPerPage > 2 ? props.setRowPerPage(props.rowPerPage - 1) : null }
                        styles={{color:'#4a4a4a'}}
                        icon={'angle-left'}/>
                    
                    <span
                        className='mx-2'
                        style={{
                            width: '30px',
                            fontSize: '1.1rem',
                            display: 'flex',
                            justifyContent: 'center',
                            alignItems: 'center',
                        }}>
                        {props.rowPerPage}
                    </span>

                    <FontAwesomeIconSolid
                        onClick={() => props.setRowPerPage(props.rowPerPage + 1)}
                        styles={{color:'#4a4a4a'}}
                        icon={'angle-right'}/>
                </div>

            </div>
        </>
    )
}