export default function ActivityOption(props) {
    return (
        <>
            <p 
                onClick={() => props.onClickAction()}
                className="dropdown-item"
                style={{ cursor:'pointer' }}>
                {props.title}
            </p>
        </>
    )
}