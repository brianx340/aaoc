import React, { useState } from 'react';
import TrTable from './TrTable'
import TableSearchBar from './TableSearchBar'

export default function TableAdminListActivities(props) {
    const [ page, setPage ] = useState(0);
    const [ rowPerPage, setRowPerPage ] = useState(10);
    const [ reverseSort, setReverseSort ] = useState(false);
    const [ search, setSearch ] = useState('');
    const [ pages, setPages ] = useState(null);
    
    const styles = {
        thDiv : {width:'100%', height:'100%', display:'flex', justifyContent:'flex-start', alignItems:'center', padding:'0.5rem'},
    }

    const sortObjectByKey = (key, arr) => {
        if(key === 'status' || key === 'type' || key === 'mode'){
            return arr.sort(function (a, b) {
                if (a[key][key] > b[key][key]) {
                    return 1;
                }
                if (a[key][key] < b[key][key]) {
                    return -1;
                }
                return 0;
            });
        }
        return arr.sort(function (a, b) {
            if (a[key] > b[key]) {
                return 1;
            }
            if (a[key] < b[key]) {
                return -1;
            }
            return 0;
        });
    }

    const sortByKey = (key) => {
        let sortedActivities = sortObjectByKey(key, props.activities);
        if(reverseSort){
            props.setActivities([...sortedActivities]);
        } else {
            props.setActivities([...sortedActivities.reverse()]);
        }
        setReverseSort(!reverseSort);
    }

    const tHead = [
        {
            name: 'title',
            label: 'Título',
        },
        {
            name: 'startDate',
            label: 'Fecha y hora',
        },
        {
            name: 'status',
            label: 'Estado',
        },
    ]

    const setNthPages = (length) => {
        setPages(Math.ceil(length / rowPerPage));
    }


    return (
        <>
        <div className='column is-11 mx-auto'>

            <TableSearchBar search={search} setSearch={setSearch} setRowPerPage={setRowPerPage} rowPerPage={rowPerPage} />

            <table style={{width:'100%'}}>
                <thead>
                    <tr style={{fontSize:'1.4rem', whiteSpace: 'nowrap'}}>
                        <th style={{minWidth:'40px'}}>
                            <div style={styles.thDiv}>
                                #
                            </div>
                        </th>

                        {
                            tHead.map((option, index) => {
                                return  <th
                                            key={index}
                                            >
                                            <div
                                                onClick={() => sortByKey(option.name)}
                                                style={styles.thDiv}>
                                                {option.label}
                                            </div>
                                        </th>
                            })
                        }

                        <th>
                            <div style={styles.thDiv}></div>
                        </th>
                    </tr>
                </thead>
                <tbody>
                    {
                        props.activities?.length < 1
                        ?
                        <>
                            <tr>
                                <td colSpan='5'>No hay actividades registradas</td>
                            </tr>
                        </>
                        :
                        <>
                            {
                                search === ''
                                ?
                                <TrTable
                                    activities={props.activities}
                                    rowPerPage={rowPerPage}
                                    styles={styles}
                                    page={page}
                                    isSearch={false}
                                    setPage={setNthPages}/>
                                :
                                <TrTable
                                    activities={
                                        props.activities.filter(activity => {
                                            return activity.title.toLowerCase().includes(search.toLowerCase());
                                        })
                                    }
                                    rowPerPage={rowPerPage}
                                    styles={styles}
                                    page={page}
                                    isSearch={true}
                                    setPage={setNthPages}/>
                            }
                        </>
                    }
                </tbody>
            </table>

            <ul className="pagination-list is-justify-content-center mt-4">
                {
                    pages === 0
                    ?
                    <>
                        <span>No se han encontrado resultados</span>
                    </>
                    :
                    [...Array( 
                        pages && search !== ''
                        ?
                        pages
                        :
                        Math.ceil(props.activities.length / rowPerPage)
                     )].map((_, i) =>{
                        return  <li
                                    key={i}
                                    onClick={()=>setPage(i)}>
                                    <p
                                        className={ i === page ? 'pagination-link is-current' : 'pagination-link' }
                                        aria-label="Goto page 1">
                                            {i+1}
                                    </p>
                                </li>
                    })
                }
            </ul>

        </div>
        </>
    )
}