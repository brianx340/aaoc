import React, { useState, useEffect } from 'react';
import { getActivityTypes, getActivityModes, getActivityStatuses, getRoles } from '../../../services/ApiService';
import { getCountries } from '../../../services/ApiService';

import InputFormSwitcher from '../../../components/admin/forms/InputFormSwitcher';

export default function ActivityCreationForm(props) {
    const [ formPage, setFormPage ] = useState(1);
    const [ formPages, setFormPages ] = useState([]);
    const [ sessionReady, setSessionReady ] = useState(false)
    
    const getActivityFormData = async () => {
        let activityTypes = await getActivityTypes();
        let activityModes = await getActivityModes();
        let activityStatus = await getActivityStatuses();
        let countries = await getCountries()
        let roles = await getRoles();

        setFormPages([
            {
                number: 1,
                inputs: [
                    {
                        name: 'title',
                        label: 'Titulo:',
                        type: 'text'
                    },
                    {
                        name: 'subtitle',
                        label: 'Subtitulo:',
                        type: 'text'
                    },
                    {
                        name: 'description',
                        label: 'Descripción:',
                        type: 'text'
                    },
                    {
                        name: 'startDate',
                        label: 'Fecha y Hora de inicio:',
                        type: 'datetime-local'
                    },
                    {
                        name: 'finishDate',
                        label: 'Fecha y Hora de finalización:',
                        type: 'datetime-local'
                    }
                ]
            },
            {
                number: 2,
                inputs: [
                    {
                        name: 'address',
                        label: 'Dirección:',
                        type: 'text'
                    },
                    {
                        name: 'country',
                        label: 'País:',
                        type: 'select',
                        options: countries
                    },
                    {
                        name: 'province',
                        label: 'Provincia:',
                        type: 'text'
                    },
                    {
                        name: 'postalCode',
                        label: 'Código postal:',
                        type: 'text'
                    },
                    {
                        name: 'doctors',
                        label: 'Médicos: (separar por comas)',
                        type: 'text'
                    },
                    {
                        name: 'type',
                        label: 'Tipo de actividad:',
                        type: 'select',
                        options: activityTypes,
                    }
                ]
            },
            {
                number: 3,
                inputs: [
                    {
                        name: 'image',
                        label: 'Imagen:',
                        type: 'file',
                        imageToView: '',
                        imageToSend: ''
                    },
                    {
                        name: 'mode',
                        label: 'Modo:',
                        type: 'select',
                        options: activityModes
                    },
                    {
                        name: 'access',
                        label: 'Acceso:',
                        type: 'multiSelect',
                        options: roles
                    },
                    {
                        name: 'visibility',
                        label: 'Visibilidad:',
                        type: 'multiSelect',
                        options: roles
                    },
                    {
                        name:'status',
                        label: 'Estado Inicial:',
                        type: 'select',
                        options: activityStatus
                    }
                ]
            },
            {
                number: 4,
                inputs: [
                    {
                        name: 'quotas',
                        label: 'Máximo de invitados: (0 = sin limite)',
                        type: 'number'
                    },
                    {
                        name: 'price',
                        label: 'Precio:',
                        type: 'number'
                    },
                    {
                        name: 'discount',
                        label: 'Descuento (%):',
                        type: 'number'
                    },
                    {
                        name: 'accessDiscount',
                        label: 'Descuento solo para:',
                        type: 'multiSelect',
                        options: roles
                    }
                ]
            }
        ])
        setSessionReady(true)
    }

    useEffect(() => {
        if(!sessionReady){
            getActivityFormData()
        }
    },[sessionReady])

    return (
        <>
            <div className='column is-4'>
                    <form style={{height:'90%'}}>

                        {
                            formPages.map((page, indexPage) => {
                                return <div
                                            key={indexPage}
                                            style={{
                                                display: formPage === page.number ? 'block' : 'none',
                                            }}>

                                                {
                                                    page.inputs.map((input, indexInput) => {
                                                        return  <div key={indexInput} className="field">
                                                                    <label className='label'>{input.label}</label>
                                                                    <div className="control">

                                                                        <InputFormSwitcher
                                                                            setFormData={props.setFormData}
                                                                            formData={props.formData}
                                                                            input={input}
                                                                            />

                                                                    </div>
                                                                </div>
                                                    })
                                                }
                                            
                                                {
                                                    page.number === formPages.length
                                                    ?
                                                    <>
                                                        <div className="field">
                                                            <button
                                                                onClick={(e) => { props.createActivity(e) }}
                                                                className='button m-auto is-block'
                                                                >
                                                                Crear actividad
                                                            </button>
                                                        </div>
                                                    </>
                                                    :
                                                    null
                                                }

                                        </div>
                            })
                        }

                    </form>

                    <ul
                         style={{height:'10%'}}
                        className="pagination-list is-flex is-justify-content-center mt-2">

                        {
                            formPages.map((page, indexPagination) => {
                                return  <li key={indexPagination}>
                                            <span
                                                onClick={() => setFormPage(page.number)}
                                                className={`pagination-link ${formPage === page.number ? 'is-current' : null}`}
                                                aria-label={`Pagina ${page.number}`}>
                                                    {page.number}
                                            </span>
                                        </li>
                            })
                        }

                    </ul>
                </div>
        </>
    )
}