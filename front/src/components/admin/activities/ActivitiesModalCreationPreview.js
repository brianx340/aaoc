import "../../../assets/css/modalActivities.scss"
import FontAwesomeIconSolid from '../../custom/FontAwesomeIconSolid'
import FontAwesomeIconRegular from '../../custom/FontAwesomeIconRegular'
import DateTitleActivityModal from '../../myActivities/DateTitleActivityModal'

export default function ActivitiesModalCreationPreview(props) {

    return (
        <>
            <div className="modal-card">
                <header
                    style={{
                        backgroundImage: `url(${props.formData.image.imageToView})`
                    }}
                    className="modal-card-head activity"
                    >
                    <DateTitleActivityModal startDate={props.formData.startDate} finishDate={props.formData.finishDate}  />
                    <h1 className='title-bold-white is-centered'> {props.formData.title} </h1>

                </header>
                <section className="modal-card-body">
                    <div className="columns is-mobile">
                        <div className="column is-7">
                            
                            <h2 className='blue pb-4'> {props.formData.subtitle}</h2>

                            <div className="field is-grouped mt-2">
                                <span className="control">
                                    <button className="button blue is-small">
                                        {
                                            props.formData.type === 'free'
                                                ?
                                                'EVENTO GRATIS'
                                                :
                                                'EVENTO PAGO'
                                        }
                                    </button>
                                </span>

                                <span className="control">
                                    <button className="button bg-skyBlue white is-small">
                                        ${props.formData.price}
                                    </button>
                                </span>
                            </div>

                            <span className="is-2 skyBlue py-4">
                                <FontAwesomeIconSolid icon={'location-dot mr-1'} />
                                <span>
                                    {
                                        props.formData.mode === 'online'
                                            ?
                                            'Evento Online'
                                            :
                                            'Evento presencial'
                                    }
                                    <br />
                                    {
                                        ` ${ props.formData.province ? `${props.formData.province}, ` : '' }${ props.formData.address ? props.formData.address : '' } `
                                    }
                                </span>
                                <div className="control my-2">
                                    <button className="btn-y yelow is-small">
                                        {
                                            props.formData.quotas === 0
                                                ?
                                                'CUPOS ILIMITADOS'
                                                :
                                                'CUPO LIMITADO'
                                        }
                                    </button>
                                </div>

                            </span>

                            <span className="is-2 blue">
                                <span>
                                    {
                                        !props.formData.doctors
                                        ?
                                        null
                                        :
                                        props.formData.doctors.split(',').map( (doctor, index) => {
                                            return  <span key={index} >
                                                        <FontAwesomeIconRegular icon={'circle-user mr-1'} /> {doctor}
                                                        <br />
                                                    </span>
                                        } )
                                    }
                                </span>
                            </span>

                        </div>


                        <div className="column is-5">
                            <div className="field">
                                <span className="control is-flex is-justify-content-flex-end">
                                    <span className="button is-normal is-info is-outlined bg-white">
                                        <span className='blue'>
                                            <FontAwesomeIconSolid icon={'share-nodes mr-2'} />
                                            Compartir evento
                                        </span>
                                    </span>
                                </span>
                            </div>
                        </div>
                    </div>
                </section>
                <footer className="modal-card-foot">
                    {
                        props.isSubscribed
                            ?
                            <button className="button ml-5 bg-gray white">Ya estoy inscrito</button>
                            :
                            <button className="button mr-5 blue">
                                <FontAwesomeIconRegular icon={'calendar mr-1'} />
                                Agendar evento
                            </button>
                    }
                </footer>
            </div>
        </>
    )

}




