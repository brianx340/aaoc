import React, { useState } from 'react';
import ModalViewEditClient from './ModalViewEditClient';

export default function ButtonClientDetail(props) {
    const [showDetail, setShowDetail] = useState(false);

    return (
        <>
            <button
                className="button"
                onClick={() => setShowDetail(true)}
            >
                Ver detalles
            </button>
            {
                !showDetail
                    ?
                    null
                    :
                    <>
                        <ModalViewEditClient user={props.user} setShow={setShowDetail} />
                    </>
            }
        </>
    )
}