export default function InputField(props) {
    return (
        <>
            <input
                style={{
                    ...props.styles,
                }}
                className={`input ${props.className}`}
                type={props.type}
                placeholder={props.placeholder}
                value={props.value}
                onChange={(e) => {
                    props.setFormData({
                        ...props.formData,
                        [props.keyName]: {
                            ...props.formData[props.keyName],
                            value: e.target.value,
                        }
                    });
                }}
                disabled={props.disabled}
            />
        </>
    )
}