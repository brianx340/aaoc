export default function InputSelect(props) {
    return (
        <>
            <select
                onChange={(e) => {
                    e.preventDefault()
                    console.log(e.target.value)
                    props.setFormData({
                        ...props.formData,
                        [props.keyName]: {
                            ...props.formData[props.keyName],
                            value: e.target.value,
                        }
                    });
                }}
                disabled={props.disabled}
                className="input">

                <option hidden>{props.text}</option>

                {
                    props.options?.map((option, index) => {
                        return  <option
                                    selected={
                                        props.value === option.value
                                    }
                                    value={option.value}
                                    key={index}>
                                    {option.label}
                                </option>
                    })
                }
            </select>
        </>
    )
}