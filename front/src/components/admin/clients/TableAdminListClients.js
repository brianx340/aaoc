
import React, { useState } from 'react';
import TrTable from './TrTable'
import TableSearchBar from './TableSearchBar'

export default function TableAdminListClients(props) {
    const [ page, setPage ] = useState(0);
    const [ rowPerPage, setRowPerPage ] = useState(10);
    const [ reverseSort, setReverseSort ] = useState(false);
    const [ search, setSearch ] = useState('');
    const [ pages, setPages ] = useState(null);
    
    const styles = {
        thDiv : {width:'100%', height:'100%', display:'flex', justifyContent:'flex-start', alignItems:'center', padding:'0.5rem'},
    }

    const sortObjectByKey = (key, arr) => {
        if(key === 'status' || key === 'type' || key === 'mode'){
            return arr.sort(function (a, b) {
                if (a[key][key] > b[key][key]) {
                    return 1;
                }
                if (a[key][key] < b[key][key]) {
                    return -1;
                }
                return 0;
            });
        }
        return arr.sort(function (a, b) {
            if (a[key] > b[key]) {
                return 1;
            }
            if (a[key] < b[key]) {
                return -1;
            }
            return 0;
        });
    }

    const sortByKey = (key) => {
        let sortedUsers = sortObjectByKey(key, props.users);
        if(reverseSort){
            props.setUsers([...sortedUsers]);
        } else {
            props.setUsers([...sortedUsers.reverse()]);
        }
        setReverseSort(!reverseSort);
    }

    const tHead = [
        {
            name: 'name',
            label: 'Nombre',
        },
        {
            name: 'email',
            label: 'Email',
        },
        {
            name: 'dni',
            label: 'DNI',
        },
        {
            name: 'role',
            label: 'Rol',
        },
    ]

    const setNthPages = (length) => {
        setPages(Math.ceil(length / rowPerPage));
    }


    return (
        <>
        <div className='column is-11 mx-auto'>

            <TableSearchBar search={search} setSearch={setSearch} setRowPerPage={setRowPerPage} rowPerPage={rowPerPage} />

            <table style={{width:'100%'}}>
                <thead>
                    <tr style={{fontSize:'1.4rem', whiteSpace: 'nowrap'}}>
                        <th style={{minWidth:'40px'}}>
                            <div style={styles.thDiv}>
                                #
                            </div>
                        </th>

                        {
                            tHead.map((option, index) => {
                                return  <th
                                            key={index}
                                            >
                                            <div
                                                onClick={() => sortByKey(option.name)}
                                                style={styles.thDiv}>
                                                {option.label}
                                            </div>
                                        </th>
                            })
                        }

                        <th>
                            <div style={styles.thDiv}></div>
                        </th>
                    </tr>
                </thead>
                <tbody>
                    {
                        props.users?.length < 1
                        ?
                        <>
                            <tr>
                                <td colSpan='5'>No se han encontrado usuarios.</td>
                            </tr>
                        </>
                        :
                        <>
                            {
                                search === ''
                                ?
                                <TrTable
                                    users={props.users}
                                    rowPerPage={rowPerPage}
                                    styles={styles}
                                    page={page}
                                    isSearch={false}
                                    setPage={setNthPages}/>
                                :
                                <TrTable
                                    users={
                                        props.users.filter(user => {
                                            return (
                                                user.name?.toLowerCase().includes(search.toLowerCase())
                                                ||
                                                user.dni?.toString().toLowerCase().includes(search.toLowerCase())
                                                ||
                                                user.email?.toLowerCase().includes(search.toLowerCase())
                                                );
                                        })
                                    }
                                    rowPerPage={rowPerPage}
                                    styles={styles}
                                    page={page}
                                    isSearch={true}
                                    setPage={setNthPages}/>
                            }
                        </>
                    }
                </tbody>
            </table>

            <ul className="pagination-list is-justify-content-center mt-4">
                {
                    pages === 0
                    ?
                    <>
                        <span>No se han encontrado resultados</span>
                    </>
                    :
                    [...Array( 
                        pages && search !== ''
                        ?
                        pages
                        :
                        Math.ceil(props.users.length / rowPerPage)
                     )].map((_, i) =>{
                        return  <li
                                    key={i}
                                    onClick={()=>setPage(i)}>
                                    <p
                                        className={ i === page ? 'pagination-link is-current' : 'pagination-link' }
                                        aria-label="Goto page 1">
                                            {i+1}
                                    </p>
                                </li>
                    })
                }
            </ul>

        </div>
        </>
    )
}