import React, { useState, useEffect } from 'react';
import InputField from './fields/InputField';
import InputSelect from './fields/InputSelect'
import { getCountries, getGenres } from '../../../services/ApiService';

export default function ModalViewEditClient(props) {
    //const [ user, setUser ] = useState(props.user);
    const [ editOn, setEditOn ] = useState(false);
    const [ sessionReady, setSessionReady ] = useState(false);
    const [ formData, setFormGenerator ] = useState({});
    const [ initData, setInitData ] = useState({});

    const processApiData = async () => {
        let countries = await getCountries();
        let genres = await getGenres();

        let data = {
            'name': {
                value: props.user.name,
                editable: true,
                input: 'input',
                type: 'text',
                label:'Nombre'
            },
            'lastName': {
                value: props.user.lastName,
                editable: true,
                input: 'input',
                type: 'text',
                label: 'Apelido',
            },
            'email': {
                value: props.user.email,
                editable: false,
                input: 'input',
                type: 'text',
                label:'Email'
            },
            'dni': {
                value: props.user.dni,
                editable: true,
                input: 'input',
                type: 'number',
                label:'DNI'
            },
            'genre': {
                value: props.user.genre._id,
                editable: true,
                input: 'select',
                options: genres,
                label:'Género'
            },
            'phone': {
                value: props.user.phone,
                editable: true,
                input: 'input',
                type: 'number',
                label:'Teléfono'
            },
            'address': {
                value: props.user.address,
                editable: true,
                input: 'input',
                type: 'text',
                label:'Dirección'
            },
            'country': {
                value: props.user.country._id,
                editable: true,
                input: 'select',
                options: countries,
                label:'País'
            },
            'province': {
                value: props.user.province,
                editable: true,
                input: 'input',
                type: 'text',
                label:'Provincia'
            },
            'title': {
                value: props.user.title,
                editable: true,
                input: 'input',
                type: 'text',
                label:'Título'
            },
            'profession': {
                value: props.user.profession,
                editable: true,
                input: 'input',
                type: 'text',
                label:'Profesión'
            },
            'specialty': {
                value: props.user.specialty,
                editable: true,
                input: 'input',
                type: 'text',
                label:'Especialidad'
            },
            'jobAddress': {
                value: props.user.jobAddress,
                editable: true,
                input: 'input',
                type: 'text',
                label:'Dirección de trabajo'
            },
        }

        setFormGenerator(data);
        setInitData(data);
    }
    
    const getChanges = () => {
        let changes = {};

        for (let key in formData) {
            if (formData[key].value !== initData[key].value) {
                changes[key] = formData[key].value;
            }
        }
        return changes;
    }

    const handleEdit = () => {
        let data = getChanges();
        //data optain the changes in the form
        console.log(data)
    }

    useEffect(() => {
        if(!sessionReady){
            processApiData();
            setSessionReady(true);
        }
        // eslint-disable-next-line
    },[sessionReady])

    return (
        <>
            {
                !sessionReady
                ?
                ''
                :
                <div className="modal is-active">
                    <div className="modal-background" onClick={() => props.setShow(false)}></div>
                    <div className="modal-card">
                        <header className="modal-card-head" style={{backgroundColor: '#30c5f3'}}>
                            <p
                                style={{ color: '#fff' }}
                                className="modal-card-title"
                                >Detalle de cliente - {props.user.role.label}</p>
                            <button
                                onClick={() => props.setShow(false)}
                                className="delete"
                                aria-label="close"></button>
                        </header>
                        <section className="modal-card-body">

                            <form>

                                {
                                    Object.keys( formData ? formData : {} ).map((keyName, index) => { 
                                        return  <div
                                                    key={index}
                                                    className="field is-horizontal">
                                                    <div className="field-label is-normal">
                                                        <label className="label">{formData[keyName].label}</label>
                                                    </div>
                                                    <div
                                                        style={{
                                                            flexGrow: '2'
                                                        }}
                                                        className="field-body">
                                                        <div className="field">
                                                            <div className="control">
                                                        

                                                                {
                                                                    formData[keyName].input === 'input'
                                                                    ?
                                                                    <InputField 
                                                                        type={formData[keyName].type}
                                                                        placeholder={ keyName }
                                                                        formData={formData}
                                                                        setFormData={setFormGenerator}
                                                                        keyName={keyName}
                                                                        value={formData[keyName].value}
                                                                        disabled={ 
                                                                            editOn
                                                                            ?
                                                                                !formData[keyName].editable 
                                                                                ?
                                                                                true
                                                                                :
                                                                                false
                                                                            :
                                                                            true
                                                                        }
                                                                        />
                                                                    :
                                                                        formData[keyName].input === 'select'
                                                                        ?
                                                                        <InputSelect
                                                                            text= {`Seleccione un ${keyName}`}
                                                                            options={formData[keyName].options}
                                                                            formData={formData}
                                                                            setFormData={setFormGenerator}
                                                                            value={formData[keyName].value}
                                                                            keyName={keyName}
                                                                            disabled={ 
                                                                                editOn
                                                                                ?
                                                                                    !formData[keyName].editable 
                                                                                    ?
                                                                                    true
                                                                                    :
                                                                                    false
                                                                                :
                                                                                true
                                                                            }
                                                                            />
                                                                        :
                                                                        ''
                                                                }
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                    })
                                }
                            </form>
                        </section>
                        <footer className="modal-card-foot">

                            {
                                !editOn
                                ?
                                <button
                                    onClick={() => setEditOn(true)}
                                    className="button is-info">
                                    Editar
                                </button>
                                :
                                <button
                                    onClick={() => handleEdit()}
                                    className="button is-success">
                                    Guardar Cambios
                                </button>
                            }

                            {
                                !editOn
                                ?
                                <button
                                    onClick={() => props.setShow(false)}
                                    className="button">
                                        Cancel
                                </button>
                                :
                                <button
                                    onClick={() => setEditOn(false)}
                                    className="button">
                                        Volver
                                </button>
                            }


                            

                        </footer>
                    </div>
                </div>
            }
        </>
    )
}