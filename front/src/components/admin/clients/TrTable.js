import React, { useEffect } from 'react';
import ButtonClientDetail from './ButtonClientDetail';

export default function TrTable(props) {

    useEffect(() => {
        if(props.isSearch){
            props.setPage(props.users.length);
        }
    })

    return (
        <>
           {
                props.users?.slice(props.page*props.rowPerPage, (props.page+1)*props.rowPerPage).map((user, index) => {
                    return  <tr key={index} style={{fontSize:'1rem', height:'50px', whiteSpace: 'nowrap'}}>
    
                                <td>
                                    <div style={props.styles.thDiv}>
                                        {index + 1 + (props.page*props.rowPerPage)}
                                    </div>
                                </td>
    
                                <td>
                                    <div style={props.styles.thDiv}>
                                        {user.name}
                                    </div>
                                </td>
    
                                <td>
                                    <div style={props.styles.thDiv}>
                                        {user.email}
                                    </div>
                                </td>

                                <td>
                                    <div style={props.styles.thDiv}>
                                        {user.dni}
                                    </div>
                                </td>
    
                                <td>
                                    <div style={props.styles.thDiv}>
                                        {user.role.label}
                                    </div>
                                </td>
    
                                <td>
                                    <div style={{ ...props.styles.thDiv, justifyContent:'flex-end', paddingRight:'0' }}>

                                        <ButtonClientDetail user={user} />

                                    </div>
                                </td>
                            </tr>
                })    
           }
        </>
    )
}