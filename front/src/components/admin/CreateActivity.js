import React, { useState, useEffect, useReducer } from 'react';
import ActivitiesModalCreationPreview from './activities/ActivitiesModalCreationPreview';
import ActivityCreationForm from './activities/ActivityCreationForm';
import { createActivityQuery } from '../../services/AdminService';
import { axiosReducer } from '../../reducers/axiosReducer';
import LoadingGif from '../custom/LoadingGif';
import PopUpMessage from '../custom/PopUpMessage';

export default function CreateActivity(props) {
    const [ axiosDp, dispatchAxios ] = useReducer(axiosReducer, { data: [], isLoading: true, isError: false });

    const [ formData, setFormData ] = useState({
        title: null,
        subtitle: null,
        description: null,
        startDate: null,
        finishDate: null,
        address: null,
        country: null,
        province: null,
        postalCode: null,
        doctors: null,
        type: null,
        mode: null,
        access: null,
        visibility: null,
        status: null,
        price: null,
        discount: null,
        accessDiscount: null,
        image: {
            imageToView: null,
            imageToSend: null
        }
    });

    const clearForm = () => {
        setFormData({
            title: null,
            subtitle: null,
            description: null,
            startDate: null,
            finishDate: null,
            address: null,
            country: null,
            province: null,
            postalCode: null,
            doctors: null,
            type: null,
            mode: null,
            access: null,
            visibility: null,
            status: null,
            price: null,
            discount: null,
            accessDiscount: null,
            image: {
                imageToView: null,
                imageToSend: null
            }
        })
    }
    
    const delay = ms => new Promise(res => setTimeout(res, ms));

    const createActivity = async (e) => {
        e.preventDefault();
        const formDataToSend = new FormData();
        formDataToSend.append('title', formData.title);
        formDataToSend.append('subtitle', formData.subtitle);
        formDataToSend.append('description', formData.description);
        formDataToSend.append('startDate', formData.startDate);
        formDataToSend.append('finishDate', formData.finishDate);
        formDataToSend.append('address', formData.address);
        formDataToSend.append('country', formData.country);
        formDataToSend.append('province', formData.province);
        formDataToSend.append('postalCode', formData.postalCode);
        formDataToSend.append('doctors', formData.doctors);
        formDataToSend.append('type', formData.type);
        formDataToSend.append('mode', formData.mode);
        formDataToSend.append('access', formData.access);
        formDataToSend.append('visibility', formData.visibility);
        formDataToSend.append('status', formData.status);
        formDataToSend.append('price', formData.price);
        formDataToSend.append('discount', formData.discount);
        formDataToSend.append('accessDiscount', formData.accessDiscount);
        formDataToSend.append('quotas', formData.quotas);
        formDataToSend.append('image', formData.image.imageToSend);

        dispatchAxios({ type: 'FETCH_INIT' })
        try{
            let created = await createActivityQuery(formDataToSend);
            await delay(2000)
            if(created.status !== 'success'){
                return dispatchAxios({ type: 'FETCH_FAILURE', payload: { message: 'Ups! no se ha creado la actividad.'} })
            }
            clearForm();
            return dispatchAxios({ type: 'FETCH_SUCCESS', payload: { message: 'Actividad creada con éxito' } })
        }
        catch(e){
            console.error(e);
            return dispatchAxios({ type: 'FETCH_FAILURE', payload: e.message})
        }
    }

    const activity = {
        status: {
            name: 'pending' // pending, process, finished, cancelled
        },
        type: {
            name: 'free' // free, paid
        },
        mode: {
            name: 'online' // online, face-to-face
        },
        access: ['user'],
        paymentData: {
            price: 130.55,
            withDiscount: true,
            discount: 20,
            accessDiscount: [
                {
                    name: 'paidUser',
                },
                {
                    name: 'user',
                }
            ],
        },
        visibility: [
            {
                name: 'paidUser',
            },
            {
                name: 'user',
            }
        ],
        title: 'Nombre de la actividad 001',
        subtitle: 'Subtitulo de a actividad 001',
        description: "Ingrese la descripcion aqui!",
        imgPath: "asd.jpg",
        startDate: new Date(),
        finishDate: new Date(),
        address: {
            _id: 1,
            address: "Calle falsa 4444",
            country: "Anoneeee",
            province: "Springfield",
            postalCode: "567123"
        },
        registeredUsers: [
            //Ids de usuarios registrados
        ],
        attendedUsers: 2, //usuarios que asistieron (se actualiza al escanear un QR de usuario)
        confirmedQty: 1, //cantidad de usuarios confirmados
        quotas: 5, //Cantidad de usuarios que pueden asistir
        doctors: [
            'Fulanito', 'Fulanito2'
        ]
    }

    useEffect(() => {
        dispatchAxios({ type: 'FETCH_EMPTY' })
    }, [])


    return (
        <>

            <section className='columns' style={{ height: '100%', minHeight:'590px'}}> 

                {
                    axiosDp.isLoading
                    ?
                    <div style={{margin:'auto'}}>
                        <LoadingGif />
                    </div>
                    :
                    <>
                        <ActivityCreationForm formData={formData} setFormData={setFormData} createActivity={createActivity} />

                        <div className='column is-8' >
                            <h3>Vista previa</h3>
                            <div
                                style={{
                                    display: 'flex',
                                    width: '100%',
                                    height: '100%',
                                }}>
                                <ActivitiesModalCreationPreview activity={activity} formData={formData} />
                            </div>
                        </div>
                    </>
                }

                {
                    axiosDp.isError
                    ?
                    <PopUpMessage title={'Error'} type={'danger'} message={axiosDp.message} />
                    :
                    null
                }

                {
                    axiosDp.isSuccess
                    ?
                    <PopUpMessage title={':D'} type={'success'} message={axiosDp.message} />
                    :
                    null
                }


            </section>
        </>
    )
}


