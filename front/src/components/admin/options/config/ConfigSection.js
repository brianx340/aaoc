export default function ConfigSection(props) {
    return (
        <>
            <div
                style={{height: '40px', padding: 0, paddingRight: '20px'}}
                className='column is-3'>
                
                <div
                    style={{height: '100%'}}
                    className='card is-flex is-justify-content-center is-align-items-center'>
                    Banner Home
                </div>

            </div>

            <div
                style={{
                    backgroundColor: 'white',
                    paddingRight: '20px',
                    padding: 0,
                }}
                className='column is-8'>
                
            </div>
        </>
    )
}