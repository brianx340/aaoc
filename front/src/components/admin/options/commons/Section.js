import React, { useState } from 'react';
import ActualView from './ActualView';
import SubOption from './SubOption';

export default function Section(props) {
    const [ actualView, setActualView ] = useState(null);

    return (
        <>
            <div className='columns px-3'>

                <div
                    style={{ height: '40px', padding: 0, paddingRight: '20px' }}
                    className='column is-3'>

                    {
                        props.data.map((item, index) => {
                            return <SubOption key={index} name={item.name} setActualView={setActualView} viewData={item.viewData} />
                        })
                    }
                    
                </div>

                <ActualView view={actualView} />

            </div>
        </>
    )
}