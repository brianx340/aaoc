export default function SubOption(props) {
    return (
        <>
            <div
                onClick={() => props.setActualView(props.viewData)}
                style={{ height: '100%' }}
                className='card is-flex is-justify-content-center is-align-items-center'>
                {props.name}
            </div>
        </>
    )
}