
export default function InputSelect(props) {
    return (
        <>
        <div className="select" style={{width:'100%'}}>

            <select style={{width:'100%'}}
                onChange={(e) => props.setFormData({
                        ...props.formData,
                        [props.input.name]: e.target.value
                })}
                >
                
                <option value="" hidden>Seleccione una opción</option>

                {
                    props.input.options.map((option, index) => {
                        return <option key={index} value={option.value}>{option.label}</option>
                    })
                }

            </select>

        </div>
        </>
    )
}