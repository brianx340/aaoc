import React, { useState } from 'react';
export default function MultiCheck(props) {
    const [ selected, setSelecteds ] = useState([]);

    const toggleOption = (e, option) => {
        let actualSelected = [];
        
        //Si se selecciona la opcion "all" se añaden al array todas las opciones y se setea en el estado [ 'all ]
        if(option === 'all') {
            actualSelected = [];
            for (let opcion of props.input.options){
                actualSelected.push(opcion.value);
            }
            setSelecteds(['all'])
            props.setFormData({
                ...props.formData,
                [props.input.name]: actualSelected
            })
            return
        }
        //Si se selecciona una opcion que no sea la de "all" se añade o se elimina de la lista de seleccionadas
        else {
            if(selected.includes('all')) {
                actualSelected = [];
            } else {
                actualSelected = [...selected];
            }

            if(actualSelected.includes(option)) {
                actualSelected = actualSelected.filter(item => item !== option);
            }else {
                actualSelected.push(option);
            }
            props.setFormData({
                ...props.formData,
                [props.input.name]: actualSelected
            })
            setSelecteds(actualSelected)
        }


    }

    return (
        <>
            <div
                className="input"
                style={{
                    width: '100%',
                    display: 'flex',
                    alignItems: 'center',
                    justifyContent: 'space-evenly',
                }}
                >

                <span
                    style={{
                        color: selected.includes('all') ? '#fff' : '#000',
                        fontSize:'.8rem',
                        fontWeight:'bold',
                        padding:'.4rem',
                        borderRadius:'.5rem',
                        cursor:'pointer',
                        boxSizing:'border-box',
                        backgroundColor: selected.includes('all') ? '#30c5f3' : 'unset',
                    }}
                    onClick={(e) => toggleOption(e, 'all')}
                    >
                    Todos
                </span>

                {
                    props.input.options.map((option, index) => {
                        return  <span
                                    key={index}
                                    style={{
                                        color: selected.includes(option.value) ? '#fff' : '#000',
                                        fontSize:'.8rem',
                                        fontWeight:'bold',
                                        padding:'.4rem',
                                        borderRadius:'.5rem',
                                        boxSizing:'border-box',
                                        cursor:'pointer',
                                        backgroundColor: selected.includes(option.value) ? '#30c5f3' : 'unset',
                                    }}
                                    onClick={(e) => toggleOption(e, option.value)}
                                    >
                                    {option.label}
                                </span>
                    })
                }


            </div>
        </>
    )
}