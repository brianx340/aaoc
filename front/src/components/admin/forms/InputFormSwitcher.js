import InputText from './InputText';
import InputSelect from './InputSelect';
import MultiCheck from './MultiCheck';
import InputFullTime from './InputFullTime';
import InputImage from './InputImage';

export default function InputFormSwitcher(props) {
    switch (props.input.type) {
        case 'text':
            return <InputText
                        setFormData={props.setFormData}
                        formData={props.formData}
                        input={props.input} />
    
        case 'select':
            return <InputSelect
                        setFormData={props.setFormData}
                        formData={props.formData}
                        input={props.input}/>

        case 'multiSelect':
            return <MultiCheck
                        setFormData={props.setFormData}
                        formData={props.formData}
                        input={props.input}/>

        case 'datetime-local':
            return <InputFullTime
                        setFormData={props.setFormData}
                        formData={props.formData}
                        input={props.input}/>

        case 'file':
            return <InputImage
                        setImageToView={(image)=>{
                            props.formData[props.input.name].imageToView = image;
                            props.setFormData(props.formData);
                        }}
                        setImageToSend={(image)=>{
                            props.formData[props.input.name].imageToSend = image;
                            props.setFormData(props.formData);
                        }}
                        setFormData={props.setFormData}
                        formData={props.formData}
                        input={props.input}/>

        default:
            return <input
                        onChange={(e) => props.setFormData({
                            ...props.formData,
                            [props.input.name]: props.input.type === 'file' ? URL.createObjectURL(e.target.files[0]) : e.target.value
                        })}
                        value={props.formData[props.input.name] ? props.formData[props.input.name] : ''}
                        className="input"
                        type={props.input.type}
                        placeholder={props.input.label}
                        />
    }
}