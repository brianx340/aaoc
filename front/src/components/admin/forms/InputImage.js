
export default function InputImage(props) {

    const onImageChange = (event) => {
        if (event.target.files && event.target.files[0]) {
            props.setImageToView(URL.createObjectURL(event.target.files[0]));
            props.setImageToSend(event.target.files[0]);
        }
    }

    return (
        <>
            <input
                type="file"
                onChange={onImageChange}
            />
        </>
    )
}