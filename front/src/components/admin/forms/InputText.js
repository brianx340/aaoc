
export default function InputText(props) {
    return (
        <>
            <input
                onChange={(e) => props.setFormData({
                    ...props.formData,
                    [props.input.name]: e.target.value
                })}
                value={props.formData[props.input.name] ? props.formData[props.input.name] : ''}
                className="input"
                type={props.input.type}
                placeholder={props.input.label}
                />
        </>
    )
}