export default function Title(props) {
    return (
        <>
            <div className='columns mt-1'>
                <div className='column is-12' style={{fontSize:'1.4rem'}}>
                    {props.title}
                </div>
            </div>
        </>
    )
}