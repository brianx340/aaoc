import React, { useState, useEffect, useReducer } from 'react';
import { getAllActivitiesQuery } from '../../services/AdminService';
import { axiosReducer } from '../../reducers/axiosReducer';
import LoadingGif from '../custom/LoadingGif';
import PopUpMessage from '../custom/PopUpMessage';
import TableAdminListActivities from './list/TableAdminListActivities';

export default function ListActivity(props) {
    const [ activities, setActivities ] = useState([]);
    const [ sessionReady, setSessionReady ] = useState(false);
    const [ axiosDp, dispatchAxios ] = useReducer(axiosReducer, { data: [], isLoading: true, isError: false });

    const processView = async () => {
        dispatchAxios({ type: 'FETCH_INIT' })
        try {
            let response = await getAllActivitiesQuery();
            if(response.status !== 'success'){
                return dispatchAxios({ type: 'FETCH_FAILURE', payload: { message: 'Ups! no se han podido obtener los datos.'} })
            }
            setActivities(response.activities)
            setSessionReady(true);
            return dispatchAxios({ type: 'FETCH_SUCCESS', payload: response.activities })
        }
        catch(e){
            console.error(e);
            return dispatchAxios({ type: 'FETCH_FAILURE', payload: {message:e.message}})
        }
    }

    useEffect(() => {
        if(!sessionReady){
            processView();
            dispatchAxios({ type: 'FETCH_EMPTY' })
        }
    })


    return (
        <>

            <section className='columns' style={{ height: '100%' }}>

                {
                    axiosDp.isLoading
                    ?
                    <div style={{margin:'auto'}}>
                        <LoadingGif />
                    </div>
                    :
                    <TableAdminListActivities activities={activities} setActivities={setActivities} />
                }

                {
                    axiosDp.isError
                    ?
                    <PopUpMessage title={'Error'} type={'danger'} message={axiosDp.message} />
                    :
                    null
                }



            </section>
        </>
    )
}


