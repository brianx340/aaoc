import React, { useState, useEffect, useReducer } from 'react';
import { deleteCountryQuery } from '../../services/AdminService';
import { getCountries } from '../../services/ApiService';
import { axiosReducer } from '../../reducers/axiosReducer';

export default function AdmitedCountries(props) {
    const [ axiosDp, dispatchAxios ] = useReducer(axiosReducer, { data: [], isLoading: true, isError: false });
    const [ sessionReady, setSessionReady ] = useState(false);
    //eslint-disable-next-line
    const [ deleteStatus, setDeleteStatus ] = useState(false);

    const delay = ms => new Promise(res => setTimeout(res, ms));

    const processSession = async () => {
        dispatchAxios({ type: 'FETCH_INIT' })
        setSessionReady(true);
        try{
            let countries = await getCountries();
            await delay(2000)
            if(!countries){
                return dispatchAxios({ type: 'FETCH_FAILURE', payload: { message: 'No se encontraron países disponibles'} })
            }
            return dispatchAxios({ type: 'FETCH_SUCCESS', payload: { data: { countries } } })
        }
        catch(e){
            console.error(e);
            return dispatchAxios({ type: 'FETCH_FAILURE', payload: e.message})
        }
    }

    const deleteCountry = async (countryId) => {
        try{
            let response = await deleteCountryQuery(countryId);
            await delay(2000)
            if(response.status !== 'success'){
                return setDeleteStatus({ success: false, message: 'No se pudo eliminar el pais.' });
            }
            return processSession();
        }
        catch(e){
            console.error(e);
            return setDeleteStatus({ success: false, message: 'No se pudo eliminar el pais.' });
        }
    }

    useEffect(() => {
        dispatchAxios({ type: 'FETCH_EMPTY' })
        if(!sessionReady){
            processSession();
        }
        // eslint-disable-next-line
    },[]);

    return (
        <>
            <section className='' style={{ height: '100%', minHeight:'590px'}}>
                <table style={{width:'70%'}} className='m-auto table'>

                    <thead>
                        <tr>
                            <th>#</th>
                            <th>Country</th>
                            <th>
                                <div className='is-flex is-justify-content-center is-align-items-center'>
                                    <span>
                                        Remove
                                    </span>
                                </div>
                            </th>
                        </tr>
                    </thead>
                    <tbody>
                    {
                        axiosDp.isLoading
                        ?
                        <tr>
                            <td>
                                <div style={{margin:'auto'}}>
                                    Cargando información...
                                </div>
                            </td>
                        </tr>
                        :
                            axiosDp.isError
                            ?
                            <tr>
                                <div>No se encontraron paises</div>
                            </tr>
                            :
                            <>
                                
                                {
                                    axiosDp.data.countries?.map((country, index) => {
                                        return  <tr key={index}>
                                                    <td>
                                                        {index+1}
                                                    </td>
                                                    <td>
                                                        {country.name}
                                                    </td>
                                                    <td>
                                                        <div className='is-flex is-justify-content-center is-align-items-center'>
                                                            <button
                                                                onClick={() => deleteCountry(country._id)}
                                                                className='button is-small'>
                                                                <i className='fas fa-trash-alt'></i>
                                                            </button>
                                                        </div>
                                                    </td>
                                                </tr>
                                    })
                                }
                            </>
                    }
                        
                    </tbody>


                </table>
            </section>
        </>
    )
}