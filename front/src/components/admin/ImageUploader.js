import FontAwesomeIconSolid from '../custom/FontAwesomeIconSolid';

export default function ImageUploader(props) {

    const onImageChange = (event) => {
        if (event.target.files && event.target.files[0]) {
            props.setImage(URL.createObjectURL(event.target.files[0]));
            props.setImageToSend(event.target.files[0]);
        }
       }

    return (
        <>
            <div
                className='column is-6'
                style={{position:'relative'}}
                >
                <figure
                    style={{
                        height: '100%',
                        width: '100%',
                        overflow: 'hidden',
                    }}
                    className='image is-flex is-justify-content-center is-align-items-center'>

                    <img
                        src={ props.image ? props.image : props.imageUrl }
                        alt=""
                        />

                </figure>
                <div
                    style={{
                        backgroundColor: '#0a0a0a63',
                        position: 'absolute',
                        top: '0',
                        left: '0',
                        width: '100%',
                        height: '100%',
                        display: 'flex',
                        justifyContent: 'center',
                        alignItems: 'center',
                    }}
                    >
                    <FontAwesomeIconSolid styles={{fontSize:'4rem', color:'white'}} icon={'upload'} />

                    <input
                        style={{
                            width: '100%',
                            height: '100%',
                            position: 'absolute',
                            opacity: '0',
                            cursor: 'pointer',
                        }}
                        onChange={onImageChange}
                        type="file" />
                </div>
            </div>
        </>
    )

}