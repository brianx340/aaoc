
import React, { useState, useEffect, useReducer } from 'react';
import { getAllUsersQuery } from '../../services/AdminService';
import { axiosReducer } from '../../reducers/axiosReducer';
import LoadingGif from '../custom/LoadingGif';
import PopUpMessage from '../custom/PopUpMessage';

import TableAdminListClients from './clients/TableAdminListClients';

export default function ListClients(props) {
    const [ users, setUsers ] = useState([]);
    const [ sessionReady, setSessionReady ] = useState(false);
    const [ axiosDp, dispatchAxios ] = useReducer(axiosReducer, { data: [], isLoading: true, isError: false });

    const processView = async () => {
        dispatchAxios({ type: 'FETCH_INIT' })
        try {
            let response = await getAllUsersQuery();
            if(response.status !== 'success'){
                return dispatchAxios({ type: 'FETCH_FAILURE', payload: { message: 'Ups! no se han podido obtener los datos.'} })
            }
            setUsers(response.users)
            setSessionReady(true);
            return dispatchAxios({ type: 'FETCH_SUCCESS', payload: response.users })
        }
        catch(e){
            console.error(e);
            return dispatchAxios({ type: 'FETCH_FAILURE', payload: {message:e.message}})
        }
    }

    useEffect(() => {
        if(!sessionReady){
            processView();
            dispatchAxios({ type: 'FETCH_EMPTY' })
        }
    },[sessionReady])

    return (
        <>
            <section className='columns' style={{ height: '100%' }}>

                {
                    axiosDp.isLoading
                    ?
                    <div style={{margin:'auto'}}>
                        <LoadingGif />
                    </div>
                    :
                    <TableAdminListClients users={users} setUsers={setUsers} />
                }

                {
                    axiosDp.isError
                    ?
                    <PopUpMessage title={'Error'} type={'danger'} message={axiosDp.message} />
                    :
                    null
                }



            </section>
        </>
    )
}