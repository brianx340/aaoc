import DuoDate from "../custom/DuoDate";
import SeeMoreButton from '../custom/SeeMoreButton'
import DownloadCertificateButton from '../custom/DownloadCertificateButton'
import FontAwesomeIconRegular from '../custom/FontAwesomeIconRegular'
import CertificateImage from './CertificateImage'

export default function Certificate(props) {

    const seeMore = () => {
        console.log('Open see more...')
    }

    const downloadCertificate = () => {
        console.log('Downloading certificate...')
    }

    return (
        <>
            <article
                style={{
                    minWidth: '350px'
                }}
                className="column is-4">
                <div className="cont">

                    <CertificateImage src={props.certificate.imgPath} />

                    <div className="is-flex is-flex-direction-column is-justify-content-space-between px-4 pb-4">

                        <h4 className='subtitle is-5 blue'>
                            {props.certificate.name}
                            <FontAwesomeIconRegular icon={"bookmark"} />
                        </h4>

                        <DuoDate date={props.certificate.date} colorDates={'gray'} />

                        <hr className="bg-gray mt-5"></hr>

                        <div className="field is-grouped is-flex is-justify-content-space-between">

                            <p className="control">
                                <SeeMoreButton
                                    styles={{fontSize: '.9rem'}}
                                    onClick={seeMore} />
                            </p>

                            <p className="control">
                                <DownloadCertificateButton
                                    styles={{fontSize: '.9rem'}}
                                    onClick={downloadCertificate} />
                            </p>

                        </div>

                    </div>

                </div>
            </article>
        </>
    )

}