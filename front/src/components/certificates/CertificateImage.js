export default function CertificateImage(props) {
    return (
        <>
            <figure className='image is-fluid'>
                <img src={props.src} alt="" />
            </figure>
        </>
    )
}