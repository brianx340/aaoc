import React, { useState, useEffect } from 'react'
import ImageContainerActivity from '../custom/ImageContainerActivity';
import DuoDate from "../custom/DuoDate";
import ReadMoreActivity from './ReadMoreActivity';

export default function ActivityPreview(props) {
    const [sessionReady, setSessionReady] = useState(false)
    const [activity, setActivity] = useState({})



    useEffect(() => {
        const processSession = async () => {
            if (!props.activity) {
                return ''
            }
            setActivity(props.activity)
            setSessionReady(true)
        }
        processSession()
        // eslint-disable-next-line
    },[sessionReady]);

    return (
        <>
            {
                !sessionReady
                    ?
                    ''
                    :
                    <article className="column is-12">
                        <div className='is-flex is-flex-direction-row'>
                            <ImageContainerActivity styles={{ width: '35%' }} src={activity.imgPath} />

                            <div className="is-flex is-flex-direction-column is-justify-content-space-between pl-4">
                                <h4 className='subtitle is-5 blue'>{activity.title}</h4>
                                <DuoDate date={activity.startDate} colorDates={'gray'} />
                                <ReadMoreActivity refreshSession={props.refreshSession} activity={activity} />
                            </div>
                        </div>
                        <hr className="bg-gray mt-5"></hr>
                    </article>
            }
        </>
    )

}