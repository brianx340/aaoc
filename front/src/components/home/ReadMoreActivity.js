import React, { useState, useEffect } from 'react'
import FontAwesomeIconSolid from '../custom/FontAwesomeIconSolid'
import ArticleModal from '../myActivities/ArticleModal'
export default function ReadMoreActivity(props) {
    const [sessionReady, setSessionReady] = useState(false)
    const [showReadMore, setShowReadMore] = useState(false)

    const processSession = async () => {
        setSessionReady(true)
    }

    useEffect(() => {
        processSession()
    }, [sessionReady]);

    return (
        <>
            <span className="skyBlue icon-text">
                <span
                    onClick={() => setShowReadMore(true)}
                    style={{
                        cursor: 'pointer',
                        ...props.styles
                    }}
                    >Leer más</span>
                <span className="icon">
                    <FontAwesomeIconSolid icon={'arrow-right-long'} />
                </span>
            </span>

            <ArticleModal refreshSession={props.refreshSession} isSubscribed={0} activity={props.activity} showModal={showReadMore} setShowModal={setShowReadMore} />

        </>
    )

}