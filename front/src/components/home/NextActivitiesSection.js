import React, { useState, useEffect } from 'react';
import NextActivities from './NextActivities'
import { getNextActivities } from '../../services/UserService';

export default function NextActivitiesSection(props) {
    const [nextActivities, setNextActivities] = useState([])
    const [sessionReady, setSessionReady] = useState(false)

    const processSession = async () => {
        let data = await getNextActivities()
        if (data.status !== 'success') {
            return ''
        }
        setNextActivities(data.nextActivities)
        setSessionReady(true)
    }

    useEffect(() => {
        if(!sessionReady){
            processSession()
        }
        // eslint-disable-next-line
    }, [nextActivities]);

    return (
        <>
            <div id='listPost' className="column is-6">
                <p className='blue subtitle is-4'>Próximas actividades</p>

                <div className="columns is-multiline is-gapless">

                    {
                        !nextActivities
                            ?
                            ''
                            :
                            <NextActivities refreshSession={processSession} activities={nextActivities} />
                    }

                </div>
            </div>
        </>
    )

}