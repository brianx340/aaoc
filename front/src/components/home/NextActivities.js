import ActivityPreview from './ActivityPreview';

export default function NextActivities(props) {
    return (
        <>
            {
                !props.activities
                    ?
                    ''
                    :
                    props.activities.map((activity, index) => {
                        return <ActivityPreview  refreshSession={props.refreshSession} key={index} activity={activity} />
                    })
            }
        </>
    )
}