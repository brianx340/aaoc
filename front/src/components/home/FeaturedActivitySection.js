import React, { useState, useEffect } from 'react';
import DuoDate from "../custom/DuoDate";
import { API_URL } from '../../config/index';
import { getFeaturedActivity } from '../../services/UserService';
import ArticleModal from '../myActivities/ArticleModal'

export default function FeaturedActivitySection(props) {
    const [featuredActivity, setFeaturedActivity] = useState([])
    const [sessionReady, setSessionReady] = useState(false)
    const [showInscribeMe, setShowInscribeMe] = useState(false)

    const processSession = async () => {
        if(sessionReady){
            return
        }
        let data = await getFeaturedActivity()
        setSessionReady(true)
        if (data.status !== 'success') {
            return ''
        }
        setFeaturedActivity(data.featuredActivity)
    }

    useEffect(() => {
        if(!sessionReady){
            processSession()
        }
        // eslint-disable-next-line
    }, [sessionReady]);

    return (
        <>
            <div id='home' className={ props.adminPreview ? "post column is-12" : "post column is-6" }>
                <p className='blue subtitle is-4'>Actividad destacada</p>

                <figure className='image is-2by1'>
                    <img src={ featuredActivity?.imgPath ? `${API_URL}/public/img/${featuredActivity.imgPath}` : ''} alt="" />
                </figure>

                <div className="bg-blue p-5">
                    <h3 className='subtitle is-5 has-text-white'>
                        {featuredActivity.title}
                    </h3>
                    <p className='white'>
                        {featuredActivity.description}
                    </p>

                    <div id='btnPost' className="field is-grouped is-justify-content-space-between is-align-items-center">

                        <DuoDate
                            style={{
                                width: '50%'
                            }}
                            date={featuredActivity.startDate} colorDates={'yellow'} />

                        <span className="control btn-inscribirse">
                            <span className="button is-normal bg-white">
                                <span
                                    onClick={() => setShowInscribeMe(true)}
                                    className='blue'
                                    >
                                        Inscribirse
                                </span>
                            </span>
                        </span>
                    </div>
                </div>
            </div>

            <ArticleModal refreshSession={processSession} isSubscribed={0} activity={featuredActivity} showModal={showInscribeMe} setShowModal={setShowInscribeMe} />

        </>
    )

}