export default function ButtonBlue(props) {
    return (
        <>
            <button
                style={{
                    borderRadius: '5px',
                    height: '30px',
                    display: 'flex',
                    justifyContent: 'center',
                    alignItems: 'center',
                    border: 'none',
                    paddingLeft: '3rem',
                    paddingRight: '3rem',
                    marginTop: '1rem',
                    marginBottom: '1rem',
                    //background: 'rgb(2, 88, 160)',
                    background: 'linear-gradient(90deg, rgb(2, 88, 160) 0%, rgb(0, 144, 255) 55%)',
                    color: '#ffffff',
                }}
                onClick={(e) => {
                    e.preventDefault();
                    if(props.onClick){
                        props.onClick();
                    }
                 }}
                >
                {props.text}
            </button>
        </>
    )
}