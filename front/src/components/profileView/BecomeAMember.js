import React, { useState, useEffect, useContext } from 'react';
import { getSubscriptionData } from '../../services/UserService';
import PaymentModal from '../payments/PaymentModal';
import { AppContext } from "../../context/AppContext";
import ButtonBlue from './ButtonBlue';

export default function BecomeAMember(props) {
    const [ sessionReady, setSessionReady ] = useState(false)
    const [ subscription, setSubscription ] = useState(false)
    const [ showConfirmPayment, setShowConfirmPayment ] = useState(false)
    const { app, setApp } = useContext(AppContext);

    const processSubscribeData = async () => {
        try {
            let response = await getSubscriptionData()
            let { status, data } = response

            if(status !== 'success'){
                throw new Error("Error al obtener información.")
            }
            if(data.paymentInfo?.status === 'approved' && app.role.type !== 'paidUser' ){
                setApp({ 
                    ...app,
                    role: 'paidUser',
                })
            }
            setSubscription(data)
            setSessionReady(true)
        }
        catch(e) {
            console.error(e)
        }
    }

    const showSuscribeModal = () => {
        setShowConfirmPayment(true)
    }

    useEffect(() => {
        if(!sessionReady){
            // eslint-disable-next-line
            processSubscribeData()
        }
        // eslint-disable-next-line
    },[sessionReady])

    return (
        <>
            <div className="field column is-12 header-form">
                <div className="control">
                    <div className="radio mb-3">
                        {
                            ( subscription && !subscription.paymentInfo ) || ( app.role.type === 'user' && !subscription.paymentInfo )
                            ?
                            <ButtonBlue
                                text={
                                    `Deseo hacerme socio (costo ${subscription.price} $${subscription.moneyType} por mes)`
                                }
                                onClick={showSuscribeModal}
                                />
                            :
                                subscription.paymentInfo?.status === 'pending' || subscription.paymentInfo?.status === 'in_process'
                                ?
                                <>
                                    <ButtonBlue text={'La subscripcion se esta procesando.'} />
                                    <span
                                        className='mt-3 is-block'
                                        style={{color:'white', textAlign:'center', cursor:'default'}}>
                                        No he recibido un link de pago?
                                    </span>
                                    <span
                                        className='mt-2 is-block'
                                        style={{color:'white', textAlign:'center', cursor:'default'}}>
                                        Click <strong style={{color:'white', cursor:'pointer'}}>Aqui</strong>.
                                    </span>
                                </>
                                :
                                    subscription.paymentInfo?.status === 'approved' || app?.role?.type === 'paidUser'
                                    ?
                                    <ButtonBlue text={'Ya esta suscrito!'} />
                                    :  
                                        subscription.paymentInfo?.status === 'rejected'
                                        ?
                                        <ButtonBlue text={'El pago fue rechazado.'} />
                                        :
                                            null
                                    
                        }
                    </div>
                </div>
            </div>

            {
                showConfirmPayment
                ?
                <PaymentModal refreshOnClose={processSubscribeData} setShow={setShowConfirmPayment} />
                :
                null
            }
        </>
    )
}