import React, { useState, useEffect, useReducer } from 'react';

import { getUserActivities, getUserActivitiesIsNot } from '../../services/UserService'
import { axiosReducer } from '../../reducers/axiosReducer';

import UserActivities from './UserActivities'
import OtherActivities from './OtherActivities'

export default function UserAndOtherActivities(props) {
    const [ axiosDpUsr, dispatchAxiosUsr ] = useReducer(axiosReducer, { data: [], isLoading: true, isError: false });
    const [ axiosDpNot, dispatchAxiosNot ] = useReducer(axiosReducer, { data: [], isLoading: true, isError: false });
    const [ sessionReady, setSessionReady ] = useState(false);

    const delay = ms => new Promise(res => setTimeout(res, ms));

    const processUserActivities = async () => {
        try{
            let userActivities = await getUserActivities();
            await delay(2000);
            if(!userActivities){
                return dispatchAxiosUsr({ type: 'FETCH_FAILURE', payload: { message: ''} })
            }
            return dispatchAxiosUsr({ type: 'FETCH_SUCCESS', payload: { message: '', data: userActivities } })
        }
        catch(e){
            console.error(e);
            return dispatchAxiosUsr({ type: 'FETCH_FAILURE', payload: e.message})
        }
    }

    const processUserActivitiesIsNot = async () => {
        try{
            let otherActivities = await getUserActivitiesIsNot();
            await delay(2000);
            if(!otherActivities){
                return dispatchAxiosNot({ type: 'FETCH_FAILURE', payload: { message: ''} })
            }
            return dispatchAxiosNot({ type: 'FETCH_SUCCESS', payload: { message: '', data: otherActivities } })
        }
        catch(e){
            console.error(e);
            return dispatchAxiosNot({ type: 'FETCH_FAILURE', payload: e.message})
        }
    }

    const processSession = async () => {
        dispatchAxiosUsr({ type: 'FETCH_INIT' })
        dispatchAxiosNot({ type: 'FETCH_INIT' })
        
        await processUserActivities()
        await processUserActivitiesIsNot()
        setSessionReady(true)
    }

    useEffect(() => {
        if(!sessionReady){
            dispatchAxiosUsr({ type: 'FETCH_EMPTY' })
            dispatchAxiosNot({ type: 'FETCH_EMPTY' })
            processSession()
        }
        // eslint-disable-next-line
    }, [sessionReady]);

    return (
        <>
            <UserActivities axiosDp={axiosDpUsr} refreshSession={()=>{processSession()}} />

            <OtherActivities axiosDp={axiosDpNot} refreshSession={()=>{processSession()}} />
        </>
    )
}