import React, { useState, useEffect } from 'react';
import moment from 'moment'

export default function DateTitleActivityModal(props) {
    const [ dateString, setDateString ] = useState('');

    const makeDate = () => {
        let startDay = moment(props.startDate).get('date');
        let finishDay = moment(props.finishDate).get('date');
        let month = moment(props.startDate).get('month')+1;

        if(!startDay && !finishDay) {
            return
        }
        
        if(startDay === finishDay || !finishDay ) {
            setDateString(`${startDay} de ${getMonth(month)}`);
        }else{
            setDateString(`${startDay} A ${finishDay} de ${getMonth(month)}`);
        }
    }

    const getMonth = (monthNumber) => {
        switch (monthNumber) {
            case '1':
            case '01':
                return 'Enero';

            case '2':
            case '02':
                return 'Febrero';
            
            case '3':
            case '03':
                return 'Marzo';

            case '4':
            case '04':
                return 'Abril';

            case '5':
            case '05':
                return 'Mayo';

            case '6':
            case '06':
                return 'Junio';
                
            case '7':
            case '07':
                return 'Julio';

            case '8':
            case '08':
                return 'Agosto';

            case '9':
            case '09':
                return 'Septiembre';

            case '10':
                return 'Octubre';

            case '11':
                return 'Noviembre';

            case '12':
                return 'Diciembre';

            default:
                return ''
        }
    }

    useEffect(() => {
        makeDate()
    })

    return (
        <>
            <p className="modal-card-title bg-skyBlue">
                {
                    dateString
                    ?
                    dateString
                    :
                    `26 Y 27 DE NOVIEMBRE`
                }    
            </p> <br />
        </>
    )
}