import Article from './Article'
import EmptyArticle from './EmptyArticle'
import lineBlue from '../../assets/img/home/bg_line-100.jpg'
import LoadingText from '../custom/LoadingText';

export default function UserActivities(props) {

    return (
        <>
            <section
                id='sectionSecondary'
                className="section">

                <h1 className="titleProp">
                    <div className='text'>

                        <span
                            className='title-border-blue'>
                            ACTIVIDADES
                        </span>

                        <br />

                        <span className='title-bold-blue'>
                            EN LAS QUE
                            <img src={lineBlue} alt="" />
                            <br />
                            <span className='textLineYelow'> ESTOY </span> SUSCRIPTO
                        </span>
                    </div>

                </h1>

                        
                <div className="columns is-multiline is-centered">
                    {
                        props.axiosDp.isLoading
                        ?
                        <LoadingText text={'Cargando...'} />
                        :
                            props.axiosDp.isSuccess && props.axiosDp.data.length > 0
                            ?
                            
                                props.axiosDp.data.map((userActivity, index) => {
                                    console.log('asd');
                                    return (
                                        <Article refreshSession={props.refreshSession} key={index} isSubscribed={1} activity={userActivity} />
                                    )
                                })
                            
                            :
                            <>
                                <EmptyArticle text={'No se ha suscrito a ninguna actividad.'} color={1} colorText={2} />
                            </>
                    }
                </div>

               
            </section>
        </>
    )

}