import "../../assets/css/modalActivities.scss"
import FontAwesomeIconSolid from '../custom/FontAwesomeIconSolid'
import FontAwesomeIconRegular from '../custom/FontAwesomeIconRegular'
import DateTitleActivityModal from '../myActivities/DateTitleActivityModal'
import ButtonPaymentSwicher from '../payments/ButtonPaymentSwicher';

export default function ArticleModal(props) {

    return (
        <>
                <div className={props.showModal ? "modal is-active" : "modal"}>
                    <div className="modal-background" onClick={() => props.setShowModal(!props.showModal)}></div>
                    <div className="modal-card">
                        <header className="modal-card-head activity">
                            <DateTitleActivityModal startDate={props.activity.startDate} finishDate={props.activity.finishDate} />
                            <h1 className='title-bold-white is-centered'> {props.activity.title} </h1>

                        </header>
                        <section className="modal-card-body">
                            <div className="columns is-mobile">
                                <div className="column is-7"> <h2 className='blue pb-4'> {props.activity.subtitle}</h2>
                                    <div className="field is-grouped mt-2">
                                        <div className="control">
                                            <button className="button blue is-small">
                                                {
                                                    props.activity.type === 'free'
                                                        ?
                                                        'EVENTO GRATIS'
                                                        :
                                                        'EVENTO PAGO'
                                                }
                                            </button>
                                        </div>

                                        <div className="control">
                                            <button className="button bg-skyBlue white is-small">
                                                ${props.activity.price}
                                            </button>
                                        </div>
                                    </div>

                                    <div className="is-2 skyBlue py-4">
                                        <FontAwesomeIconSolid icon={'location-dot mr-1'} />
                                        <span>
                                            {
                                                props.activity.mode === 'online'
                                                    ?
                                                    'Evento Online'
                                                    :
                                                    'Evento presencial'
                                            }
                                            <br />
                                            {
                                                ` ${props.activity.province}, ${props.activity.address}`
                                            }
                                        </span>
                                        <div className="control my-2">
                                            <button className="btn-y yelow is-small">
                                                {
                                                    props.activity.quotas === 0
                                                        ?
                                                        'CUPOS ILIMITADOS'
                                                        :
                                                        `CUPO LIMITADO ${props.activity.quotas}`
                                                }
                                            </button>
                                        </div>

                                    </div>
                                    <div className="is-2 blue">
                                        
                                        {
                                            !props.activity.doctors || props.activity.doctors.length === 0
                                            ?
                                            null
                                            :
                                            props.activity.doctors.map( (doctor, index) => {
                                                return  <span key={index}>
                                                            <FontAwesomeIconRegular icon={'circle-user mr-1'} /> {doctor}
                                                            <br />
                                                        </span>
                                            } )
                                        }
                                        
                                    </div>
                                </div>
                            </div>

                            <div className="columns is-mobile">
                                <div className="column is-5">
                                    <div className="field">
                                        <div className="control is-flex is-justify-content-flex-end">
                                            <div className="button is-normal is-info is-outlined bg-white">
                                                <span className='blue'>
                                                    <FontAwesomeIconSolid icon={'share-nodes mr-2'} />
                                                    Compartir evento
                                                </span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <button onClick={() => props.setShowModal(!props.showModal)} className="modal-close is-large" aria-label="close"></button>
                        </section>
                        <footer className="modal-card-foot">
                            <ButtonPaymentSwicher refreshSession={props.refreshSession} activity={props.activity} setShow={props.setShowModal}/>
                        </footer>
                    </div>
                </div>
        </>
    )

}




