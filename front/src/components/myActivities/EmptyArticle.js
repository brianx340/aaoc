export default function EmptyArticle(props) {
    let colorBg;
    let colorText;

    switch (props.colorBg) {
        case 1:
            colorBg = '#0080ff'
            break;

        case 2:
            colorBg = '#fff'
            break;
    
        default:
            break;
    }

    switch (props.colorBg) {
        case 1:
            colorText = '#fff'
            break;
        
        case 2:
            colorText = '#0080ff'
            break;
        
        default:
            break;
    }

    return (
        <>
            <article className="column is-4"
                style={{
                    width: '100%',
                }}
                >
                <div
                    style={{
                        backgroundColor: colorBg,
                        color: colorText,

                    }}
                    className="notification is-info">
                    {props.text}
                </div>
            </article>
        </>
    )

}