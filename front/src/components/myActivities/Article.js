import React, { useState } from 'react'
import DuoDate from "../custom/DuoDate";
import ArticleModal from './ArticleModal'

import certificate1 from '../../assets/img/certificados/cert_1.png'

export default function Article(props) {
    const [showModal, setShowModal] = useState(false)

    return (
        <>
            <ArticleModal refreshSession={props.refreshSession} isSubscribed={props.isSubscribed} activity={props.activity} showModal={showModal} setShowModal={setShowModal} />

            <article className="column is-4">
                <div className="cont">
                    <figure className='image is-fluid'>
                        <img src={certificate1} alt="" />
                    </figure>
                    <div className={`is-flex is-flex-direction-column is-justify-content-space-between px-4 pb-4 ${props.isSubscribed ? 'bg-blue' : 'bg-white'}`}>
                        <h4 className={`subtitle is-5 ${props.isSubscribed ? 'white' : 'blue'}`}>{props.activity?.title}</h4>
                        <p className={`mb-3 ${props.isSubscribed ? 'white' : 'blue'}`}>
                            {props.activity?.description}
                        </p>

                        <DuoDate date={props.activity?.startDate} colorDates={props.isSubscribed ? 'yellow' : 'skyBlue'} />

                        <hr className="bg-gray mt-5"></hr>
                        <div className="field is-flex is-justify-content-center">
                            <p className="control">

                                <button onClick={() => setShowModal(!showModal)} className={`button ${props.isSubscribed ? 'blue' : 'bg-blue white'}`} >
                                    {
                                        props.isSubscribed
                                            ?
                                            'Ver más'
                                            :
                                            'Inscribirme'
                                    }
                                </button>

                            </p>
                        </div>
                    </div>
                </div>
            </article>
        </>
    )

}