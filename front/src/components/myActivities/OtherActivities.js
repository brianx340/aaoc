import Article from './Article'
import EmptyArticle from './EmptyArticle'
import LoadingText from '../custom/LoadingText';

export default function OtherActivities(props) {
    return (
        <>
            <section
                id='sectionSecondary'
                className="section bg-skyBlue">

                <h1 className="titleProp">
                    <div className='text'>

                        <span
                            className='title-border-white'>
                            ACTIVIDADES
                        </span>

                        <br />

                        <span className='title-bold-white'>
                            EN LAS QUE
                            <>
                                {` NO`}
                            </>
                            <br />
                            <span className='textLineBlue2'> ESTOY </span> SUSCRIPTO
                        </span>
                    </div>

                </h1>

                        
                <div className="columns is-multiline is-centered">
                    {
                        props.axiosDp.isLoading
                        ?
                        <LoadingText text={'Cargando...'} />
                        :
                            0
                            ?
                            
                                props.axiosDp.data.map((otherActivity, index) => {
                                    return (
                                        <Article refreshSession={props.refreshSession} key={index} isSubscribed={1} activity={otherActivity} />
                                    )
                                })
                            
                            :
                            <EmptyArticle text={'No se hay actividades a las cual suscribirse.'} colorBg={2} colorText={2} />
                    }
                </div>

               
            </section>
        </>
    )

}