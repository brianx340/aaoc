export default function Button(props) {
    return (
        <>
            <button
                onClick={props.onClick ? props.onClick : ''}
                style={{ ...props.styles }}
                className={`button ${props.className ? props.className : ''}`}
            >
                {props.name ? props.name : ''}
            </button>
        </>
    )
}