export default function Input(props) {

    const regexInput = (value) => {
		//eslint-disable-next-line
		let regexValue = props.regexValidation
		return regexValue.test(value)
	}

    const onChangeInput = (value) => {
        if(props.regexValidation){
            props.setValue({
                value,
                validationStatus: regexInput(value)
            })
            return
        }
        props.setValue({
            value,
            validationStatus: 'none'
        })
	}

    return (
        <>
            <input
                style={props.stylesUnset ? null : 
                    {
                        width: '100%',
                        minHeight: '50px',
                        justifyContent: 'center',
                        backgroundColor: 'white',
                        border: 'none',
                        borderBottom: props.value.validationStatus === 'unset' ? '1px solid #ffffff36' : props.value.validationStatus ? '2px solid rgb(0 255 107 / 52%)' : '2px solid rgb(255 0 0 / 52%)',
                        fontSize: '1.3rem',
                        outline: 'none',
                        color:'#5d677f',
                        paddingLeft: '10px',
                        ...props.styles
                    }}
                type={props.type}
                placeholder={props.placeholder}
                value={props.value.value}
                onChange={(e)=>onChangeInput(e.target.value)}
                spellCheck={props.spellcheck ? props.spellcheck : false}
                disabled={props.disabled ? props.disabled : false}
                />
        </>
    );
}
