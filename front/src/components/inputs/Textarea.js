export default function Textarea(props) {
    const onChangeInput = (value) => {
        if(props.maxLength){
            if(value.length > props.maxLength){
                props.setValue({ value: value.substring(0, props.maxLength), validationStatus: false });
            }else{
                props.setValue({ value: value, validationStatus: true });
            }
        }else{
            props.setValue({ value: value, validationStatus: 'unset' });
        }
	}

    return (
        <>
            <div
                style={{
                    position: 'relative',
                    ...props.divStyles
                }}
                >

                <textarea
                    style={{
                        outline: 'none',
                        ...props.styles
                    }}
                    name=""
                    id=""
                    cols="30"
                    rows="9"
                    onChange={(e)=>onChangeInput(e.target.value)}
                    placeholder={ props.placeholder ? props.placeholder : '' }
                    value={ props.value.value ? props.value.value : '' }
                    />
                
                
                <div
                    style={{
                        position: 'absolute',
                        bottom: '4%',
                        right: '2%',
                    }}
                    >
                    <span
                        style={{
                            fontSize: '1.2rem',
                        }}
                        >
                        Quedan:
                        <span
                            className="mx-3"
                            style={{
                                color: props.value.validationStatus === 'unset' ? '#000' : props.value.validationStatus ? 'rgb(0 255 107 / 52%)' : 'rgb(255 0 0 / 52%)'
                            }}>
                            { props.value.value ? props.maxLength - props.value.value?.length : 200 }
                        </span>
                        caracteres...
                    </span>
                </div>

            </div>
        </>
    );
}

/*
 <input
    style={props.stylesUnset ? null : 
        {
            width: '100%',
            minHeight: '50px',
            justifyContent: 'center',
            backgroundColor: 'white',
            border: 'none',
            borderBottom: props.value.validationStatus === 'unset' ? '1px solid #ffffff36' : props.value.validationStatus ? '2px solid rgb(0 255 107 / 52%)' : '2px solid rgb(255 0 0 / 52%)',
            fontSize: '1.3rem',
            outline: 'none',
            color:'#5d677f',
            paddingLeft: '10px',
            ...props.styles
        }}
    type={props.type}
    placeholder={props.placeholder}
    value={props.value.value}
    onChange={(e)=>onChangeInput(e.target.value)}
    spellCheck={props.spellcheck ? props.spellcheck : false}
    disabled={props.disabled ? props.disabled : false}
    />
*/