import React, { useContext } from 'react';
import { AppContext } from "../../context/AppContext";
import BecomeAMember from '../profileView/BecomeAMember'

export default function ButtonPayCancelSubscription(props) {
    const { app } = useContext(AppContext);

    return (
        <>
            {
                app.user.role.type === 'user'
                ?
                <BecomeAMember />
                :
                    app.user.role.type === 'paidUser'
                    ?
                    <div className="field column is-12 header-form">
                        <div className="control">
                            <label className="radio mb-3">
                                Cancelar subscription
                            </label>
                        </div>
                    </div>
                    :
                        app.user.role.type === 'defaulter'
                        ?
                        <div className="field column is-12 header-form">
                            <div className="control">
                                <label className="radio mb-3">
                                    Tiene una deuda pendiente
                                </label>
                            </div>
                        </div>
                        :
                        ''
            }
        </>
    )
}