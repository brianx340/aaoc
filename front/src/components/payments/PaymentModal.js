import React, { useState, useReducer, useEffect, useContext } from 'react';
import { getSubscriptionQuery, getUser } from '../../services/UserService';
import { axiosReducer } from '../../reducers/axiosReducer';
import { AppContext } from "../../context/AppContext";

export default function PaymentModal(props) {
    const { app, setApp } = useContext(AppContext);
    const [ paymentInitialized, setPaymentStatus ] = useState(false);
    const [ axiosDp, dispatchAxios ] = useReducer(axiosReducer, { data: [], isLoading: true, isError: false });

    const suscribeMe = async () => {
        dispatchAxios({ type: 'FETCH_INIT' })
        try {
            let data = (await getSubscriptionQuery())
            let { status, linkCheckout } = data
            if(status !== 'success'){
                return dispatchAxios({ type: 'FETCH_FAILURE', payload: { message: data.message} })
            }
            window.open(linkCheckout, '_blank');
            setPaymentStatus(true);
            return dispatchAxios({ type: 'FETCH_SUCCESS', payload: { data: '' } })
        }
        catch(e) {
            console.error(e)
        }
    }

    const closeAndCheckPayment = async () => {
        await checkPaymentStatus()
        if (props.refreshOnClose){
            props.refreshOnClose()
        }
        props.setShow(0)
    }

    const checkPaymentStatus = async () => {
        //aqui debemos consultar antes de cerrar la ventana si se ha realizado el pago
        //si se pago cambiamos el estado de la aplicacion
        dispatchAxios({ type: 'FETCH_INIT' })
        try {
            let response = (await getUser())
            let { status, user } = response
            if(status !== 'success'){
                return dispatchAxios({ type: 'FETCH_FAILURE', payload: { message: response.message} })
            }
            if(user){
                setApp({ 
                    ...app,
                    user,
                    role: user.role.type,
                })
            }
            return dispatchAxios({ type: 'FETCH_SUCCESS', payload: { data: '' } })
        }
        catch(e) {
            console.error(e)
        }
    }

    useEffect(() => {
        dispatchAxios({ type: 'FETCH_EMPTY' })
    }, [])

    return (
        <>
            <div className="modal is-active">
                <div className="modal-background"></div>
                <div className="modal-content" style={{maxWidth:'530px'}}>

                    <article className={`message is-info`}>
                        <div className="message-header">

                            <p>Click en continuar para comenzar el proceso de subscription.</p>

                            <span
                                onClick={() => {
                                    closeAndCheckPayment()
                                }}
                                className="delete" aria-label="delete"></span>

                        </div>
                        <div className="message-body is-flex is-flex-direction-column is-justify-content-center is-align-items-center">
                            
                            <img src="https://http2.mlstatic.com/frontend-assets/ui-navigation/5.18.7/mercadopago/logo__large.png" alt="" />


                            {
                                !paymentInitialized
                                ?
                                <>
                                    {
                                        axiosDp.isLoading
                                        ?
                                        <span className="button is-info mt-4">
                                            Procesando...
                                        </span>
                                        :
                                            axiosDp.isError
                                            ?
                                            <>
                                                <span className="button is-danger mt-4">
                                                    Error en la solicitud
                                                </span>
                                                <span
                                                    style={{textAlign: 'center', color:'#b72020', marginTop:'10px'}}
                                                    >
                                                    {axiosDp.message}
                                                </span>
                                            </>
                                            :
                                            <span
                                                onClick={() => {
                                                    suscribeMe()
                                                }}
                                                className="button is-info mt-4">
                                                Suscribirme
                                            </span>
                                    }
                                </>
                                :
                                <span
                                    onClick={() => {
                                        closeAndCheckPayment()
                                    }}
                                    className="button is-success mt-4">
                                    Volver
                                </span>
                            }

                        </div>
                    </article>
                </div>
            </div>
        </>
    )
}