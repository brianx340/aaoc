import React, { useContext, useState } from 'react';
import { AppContext } from "../../context/AppContext";
import SuscribeMeButton from '../custom/SuscribeMeButton'
import PaymentModal from './PaymentModal';

export default function ButtonPaymentSwicher(props) {
    const { app } = useContext(AppContext);
    const userRole = app?.user?.role._id
    const [ showConfirmPayment, setShowConfirmPayment ] = useState(false)   
    
    return (
        <>
            {
                props.activity.access?.map(role => role._id).includes(userRole)
                ?
                <>
                    <SuscribeMeButton refreshSession={props.refreshSession} activityId={props.activity._id} />
                </>
                :
                <>
                    <div style={{display:'flex', flexDirection:'column', width:'100%'}}>

                        <span
                            style={{textAlign:'center', fontSize:'1rem'}}
                            >Necesita una subscripcion para agendar esta actividad.</span>
                        <button
                            style={{maxWidth:'200px', marginTop:'10px'}}
                            onClick={() => {
                                setShowConfirmPayment(true)
                            }}
                            className='button is-info is-small mx-auto'>
                            Suscribirme
                        </button>
                    </div>
                </>
            }

            {
                showConfirmPayment
                ?
                <PaymentModal refreshSession={props.refreshSession} setShow={setShowConfirmPayment} />
                :
                null
            }
        </>
    )

}