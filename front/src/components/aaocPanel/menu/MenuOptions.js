import React, { useState, useContext, useEffect } from 'react';
import Option from './Option';
import { AppContext } from "../../../context/AppContext";
import { panelAaocOptions } from "../../../config/index";

export default function MenuOptions(props) {
    const { app } = useContext(AppContext);
    const [ sessionReady, setSessionReady ] = useState(false)
    const [ menuOptions, setMenuOptions ] = useState([]);

    const processSession = async () => {
        let role = app.user.role.type
        
        switch(role) {
            case 'admin':
                setMenuOptions(panelAaocOptions.admin)
                break;

            case 'employee':
                setMenuOptions(panelAaocOptions.employee)
                break;
                
            default:
                setMenuOptions([])
                break;
        }
    }

    useEffect(() => {
        if(!sessionReady){
            // eslint-disable-next-line
            processSession()
            setSessionReady(true)
        }
    // eslint-disable-next-line
    },[sessionReady])

    return (
        <>
            <aside
                style={{ backgroundColor: 'white', padding: '1rem' }}
                className="column is-3 is-narrow-mobile is-fullheight section is-hidden-mobile">
                <ul className="menu-list">

                    {
                        menuOptions.map((option, index) => {
                            return <Option key={index} option={option} />
                        })
                    }

                </ul>
            </aside>
        </>
    )
}

/*

            <div
                style={{ backgroundColor: 'white', height: '100%', }}
                className="column is-2 pt-6">

                <div>
                    <span className='ml-5'>
                        Main
                    </span>
                    {
                        menuOptions.map((option, index) => {
                            return <div
                                key={index}
                                style={{
                                    boxShadow: 'unset'
                                }}
                                className="card">
                                <div className="card-content">
                                    <Link to={option.url}> 
                                        <p className="title is-5" style={{ whiteSpace: 'nowrap' }}>
                                            <span className="icon">
                                                <i className={`fa fa-${option.icon}`}></i>
                                            </span>

                                            <span className="ml-3">
                                                {option.label}
                                            </span>
                                        </p>
                                    </Link>
                                </div>
                            </div>
                        })
                    }
                </div>
            </div>
*/
