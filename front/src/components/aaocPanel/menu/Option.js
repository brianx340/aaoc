import { Link } from 'react-router-dom';

export default function Option(props) {


    return (
        <>
           <li>
               {
                    !props.option.subOptions
                    ?
                    <Link to={props.option.url}  style={{ whiteSpace: 'nowrap' }}>
                        <span className="icon"><i className="fa fa-home"></i></span> {props.option.label}
                    </Link>
                    :
                    <>
                        <Link to={'#'}  style={{ whiteSpace: 'nowrap' }}>
                            <span className="icon"><i className="fa fa-table"></i></span> {props.option.label}
                        </Link>

                        <ul>

                            {
                                props.option.subOptions.map((subOption, index) => {
                                    return  <li key={index} style={{ whiteSpace: 'nowrap' }}>
                                                <Link to={subOption.url}>
                                                    <span className="icon is-small"><i className="fa fa-link"></i></span> {subOption.label}
                                                </Link>
                                            </li>
                                })
                            }
                            
                        </ul>
                    </>
               }
            </li>
        </>
    )
}
