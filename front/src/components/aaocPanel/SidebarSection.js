import MenuOptions from './menu/MenuOptions';
export default function SidebarSection(props) {

    return (
        <>

            <section style={{ backgroundColor: '#e8e8e8', margin: '0px', minHeight:'90vh' }} className="main-content columns is-fullheight">

                <MenuOptions />

                <div className="container column is-9">
                    <div className="section" style={{height:'100%', padding: '2rem 2rem'}}>
                        {props.children}
                    </div>
                </div>

            </section>
        </>
    )
}