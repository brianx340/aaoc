import BillingTableRow from './BillingTableRow';

export default function BillingTable(props) {
    return (
        <>
            <table className="table is-striped is-hoverable is-fullwidth">
                <thead className='bgBlue'>
                    <tr>
                        <th>N° de comprobante</th>
                        <th>Periodo</th>
                        <th>Estado</th>
                        <th>Fecha de vencimiento</th>
                        <th>Fecha de Emisión</th>
                        <th>Acción</th>
                    </tr>
                </thead>
                <tbody>

                    {
                        !props.vouchers
                        ?
                        null
                        :
                        props.vouchers.map((voucher, index) => {
                            return <BillingTableRow className={ index % 2 === 0 ? 'bg-lig' : 'bg-dark'} key={index} data={voucher} />
                        })
                    }

                </tbody>

            </table>
        </>
    )
}