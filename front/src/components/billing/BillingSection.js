import React, { useState, useEffect } from 'react';
import arrowBtn from '../../assets/img/home/arrow_bottom.svg'
import lineBlue from '../../assets/img/home/bg_line-100.jpg'
import BillingRow from './BillingRow'
import ButtonPayCancelSubscription from '../payments/ButtonPayCancelSubscription'
import { getSubscriptionData } from '../../services/UserService';

export default function BillingSection(props) {
    const [ membershipCost, setMembershipCost ] = useState(null)
    //const [ debts, setDebts ] = useState(null)
    const [ sessionReady, setSessionReady ] = useState(false)

    const [ user, setUser ] = useState(null)

    const processSession = async () => {
        let membershipCost = (await getSubscriptionData()).data
        setUser(props.user)
        setMembershipCost(membershipCost)
        setSessionReady(true)
    }

    useEffect(() => {
        if(!sessionReady) {
            processSession()
        }
    });

    return (
        <>
            <section className='section sectionSecondary bg-blue'>
                <div className="columns is-multiline">
                    <div className="column is-12">
                        <h1 className="titleProp">
                            <div className='text'>
                                <span className='title-border-white'>DATOS </span>
                                <br />
                                <span className='title-bold-white'>DE FACTURACIÓN </span><br />
                                <img src={lineBlue} alt="" />
                            </div>
                            <div className='btn-title'>
                                <img src={arrowBtn} alt="" />
                            </div>
                        </h1>
                    </div>

                    <div className="column is-12 ">

                        {
                            !membershipCost
                            ?
                                ''
                            :
                            <>
                                <BillingRow name={'Tipo de membresía:'} value={user?.role?.label ? user?.role?.label : ''} />
                                <BillingRow name={'Costo de membresía:'} value={ membershipCost ? `$${membershipCost.price} ${membershipCost.moneyType}` : ''} />
                            </>
                        }
                        {
                            /*
                                <BillingRow name={'Tipo de Pago:'} value={payMethod} />
                                <BillingRow name={'Deuda:'} value={debts} />
                            */
                        }

                    </div>

                    <div className="column is-5 field is-grouped is-flex is-justify-content-space-between">
                        {
                            /*
                                <p className="control">
                                    <button className="button bg-blue white">
                                        Cambiar método de pago
                                    </button>
                                </p>
                            */
                        }

                        <div className="control">
                            <ButtonPayCancelSubscription />
                        </div>
                    </div>
                </div>
            </section>
        </>
    )
}