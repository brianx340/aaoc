import { Link } from 'react-router-dom';
import FontAwesomeIconSolid from '../custom/FontAwesomeIconSolid'

export default function BillingTableRow(props) {
    return (
        <>
             <tr>
                <td className={props.className}>{props.data._id}</td>
                <td className={props.className}>{props.data.month}</td>
                <td className={props.className}>{props.data.status}</td>
                <td className={props.className}>{props.data.dateOfIssue}</td>
                <td className={props.className}>{props.data.dueDate}</td>
                <td className={props.className}>
                    <Link to={props.data.urlPath}>
                        Descargar
                    </Link>
                    <FontAwesomeIconSolid icon="print" />
                </td>
            </tr>
        </>
    )
}

/*
dateOfIssue: "11-2-2021"
dueDate: "11-2-2021"
month: "Enero"
status: "Pago"
urlPath: "http://www.asd.com/asd.jpg"
_id: "525528"
*/