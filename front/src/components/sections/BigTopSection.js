import arrowTitle from '../../assets/img/certificados/arrow_yellow.svg'
import activityImage from '../../assets/img/actividades/bg_actividades.png'
import certificateImage from '../../assets/img/certificados/bg_pricipal_1-100.jpg'

export default function BigTopSection(props) {
    return (
        <>
            <section style={{ position: 'relative', padding: 0, margin: 0 }} id='sectionPrimary' className='section mb-4'>
                <div style={{ maxHeight: '710px', overflow: 'hidden' }}>
                    <div style={{ position: 'absolute', bottom: '0', right: '9px', zIndex: '1' }} className="columns is-mobile">
                        <div className="column is-6">
                            <h1 className="titleProp">
                                <span className='title-border-white'>MIS</span> <br />
                                <span className='title-bold-white'><span className='textLineBlue'>{props.type === 'activity' ? 'ACTIVIDADES' : 'CERTIFICADOS'} </span> <br /> </span>
                            </h1>
                            <img src={arrowTitle} alt="" />
                        </div>
                    </div>
                    <figure
                        style={{
                            width: '100%',
                            height: '100%',
                            margin: '0',
                            padding: '0',
                        }}
                        className='image'
                    >
                        <img
                            style={{
                                height: 'fit-content',
                                width: '100%',
                                margin: '0',
                                padding: '0',
                            }}
                            src={props.type === 'activity' ? activityImage : certificateImage} alt="" />
                    </figure>
                </div>
            </section>
        </>
    )
}