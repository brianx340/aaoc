import React, { useEffect, useState } from 'react';
//import { axiosReducer } from '../../reducers/axiosReducer';
import { getGenres } from '../../services/ApiService';

export default function SelectGenres(props) {
    //const [ axiosDp, dispatchAxios ] = useReducer(axiosReducer, { data: [], isLoading: true, isError: false });
    const [ genres, setGenres ] = useState([]);
    const [ sessionReady, setSessionReady ] = useState(false);

    const loadGenres = async () => {
        //dispatchAxios({ type: 'FETCH_INIT' })
        try{
            let genres = await getGenres();
            if(!genres){
                //return dispatchAxios({ type: 'FETCH_FAILURE', payload: { message: ''} })
            };
            setGenres(genres);
            setSessionReady(true);
            //return dispatchAxios({ type: 'FETCH_SUCCESS', payload: { message: '' } })
        }
        catch(e){
            console.error(e);
            //return dispatchAxios({ type: 'FETCH_FAILURE', payload: e.message})
        }
    }

    useEffect(() => {
        //dispatchAxios({ type: 'FETCH_EMPTY' })
        if(!sessionReady){
            loadGenres();
        }
    }, [sessionReady])

    return (
        <>
            <select
                style={{
                        width: '100%',
                        minHeight: '50px',
                        justifyContent: 'center',
                        backgroundColor: 'white',
                        border: 'none',
                        fontSize: '1.3rem',
                        outline: 'none',
                        color:'#5d677f',
                        paddingLeft: '10px',
                    }}
                onClick={(e) => props.setValue({value: e.target.value, validationStatus: 'unset'})}
                >
                <option hidden>Genero</option>
                {
                    !genres
                    ?
                    <option>Loading...</option>
                    :
                    genres.map((genre, index) => {
                        return <option key={index} value={genre.value}>{genre.label}</option>
                    })
                }
            </select>
        </>
    )
}