import React, { useEffect, useState } from 'react';
//import { axiosReducer } from '../../reducers/axiosReducer';
import { getCountries } from '../../services/ApiService';

export default function SelectCountries(props) {
    //const [ axiosDp, dispatchAxios ] = useReducer(axiosReducer, { data: [], isLoading: true, isError: false });
    const [ countries, setCountries ] = useState([]);
    const [ sessionReady, setSessionReady ] = useState(false);

    const loadCountries = async () => {
        //dispatchAxios({ type: 'FETCH_INIT' })
        try{
            let countries = await getCountries();
            if(!countries){
            //    return dispatchAxios({ type: 'FETCH_FAILURE', payload: { message: ''} })
            };
            setCountries(countries);
            setSessionReady(true);
            //return dispatchAxios({ type: 'FETCH_SUCCESS', payload: { message: '' } })
        }
        catch(e){
            console.error(e);
            //return dispatchAxios({ type: 'FETCH_FAILURE', payload: e.message})
        }
    }

    useEffect(() => {
        //dispatchAxios({ type: 'FETCH_EMPTY' })
        if(!sessionReady){
            loadCountries();
        }
    }, [sessionReady])

    return (
        <>
            <select
                style={{
                        width: '100%',
                        minHeight: '50px',
                        justifyContent: 'center',
                        backgroundColor: 'white',
                        border: 'none',
                        fontSize: '1.3rem',
                        outline: 'none',
                        color:'#5d677f',
                        paddingLeft: '10px',
                    }}
                onClick={(e) => props.setValue({value: e.target.value, validationStatus: 'unset'})}
                >
                <option hidden>País</option>
                {
                    !countries
                    ?
                    <option>Loading...</option>
                    :
                    countries.map((country, index) => {
                        return <option key={index} value={country.value}>{country.label}</option>
                    })
                }
            </select>
        </>
    )
}