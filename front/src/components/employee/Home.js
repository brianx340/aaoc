import AaocLogo from '../custom/AaocLogo';

export default function Home(props) {

    return (
        <>
            <div
                style={{
                    width: '100%',
                    height: '300px',
                    display: 'flex',
                    justifyContent: 'center',
                    alignItems: 'center',
                }}
                >
                
                <AaocLogo styles={{
                        width: '400px',
                        height: '400px',
                    }} />

            </div>
        </>
    )
}