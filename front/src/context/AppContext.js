import { createContext } from 'react';

export const AppContext = createContext({
    isAuthenticated: false,
    user: null,
    viewsData: null,
    role: null,
});
