import React, { useState } from 'react';
import { Routes, Route } from "react-router-dom";
import WithoutNav from './components/navbar/WithoutNav'
import CertificatesView from './views/user/CertificatesView'
import ActivitiesView from './views/user/ActivitiesView'
import { AppContext } from "./context/AppContext";
import WithNav from './components/navbar/WithNav'
import RegisterView from './views/user/RegisterView'
import BillingView from './views/user/BillingView'
import ProfileView from './views/user/ProfileView'
import LoginView from './views/user/LoginView'
import HomeView from './views/user/HomeView'
import Logout from './views/user/Logout'

import WithAdminNav from './components/admin/navbar/WithAdminNav'
import AdminHome from './views/admin/AdminHome'
import AdminListClients from './views/admin/AdminListClients'
import AdminModifyBanner from './views/admin/AdminModifyBanner'
import AdminCreateActivity from './views/admin/AdminCreateActivity'
import AdminListActivities from './views/admin/AdminListActivities'
import AdminAddAdmitCountries from './views/admin/AdminAddAdmitCountries'

import EmployeeHome from './views/employee/EmployeeHome'
import EmployeeQrChecker from './views/employee/EmployeeQrChecker'

import TestView from './views/TestView'

import "./App.scss"
import "./assets/css/svg.css"

export default function App() {
	const [app, setApp] = useState(null);
	const DEV_MODE = true;

	return (
		<>
			<AppContext.Provider value={{ app, setApp }}>

				<Routes>

					<Route element={<WithNav />}>
						<Route path="/" element={<HomeView />} />
						<Route path="/profile" element={<ProfileView />} />
						<Route path="/billing" element={<BillingView />} />
						<Route path="/activities" element={<ActivitiesView />} />
						<Route path="/certificates" element={<CertificatesView />} />
					</Route>

					<Route element={<WithoutNav />}>
						<Route path="/register" element={<RegisterView />} />
						<Route path="/login" element={<LoginView devMode={DEV_MODE} />} />
						<Route path="/logout" element={<Logout />} />
					</Route>

					<Route element={<WithAdminNav />}>
						<Route path="/admin" element={<AdminHome />} />
						<Route path="/admin/clients/list" element={<AdminListClients />} />
						<Route path="/admin/config/modifyBanner" element={<AdminModifyBanner />} />
						<Route path="/admin/config/addAdmitCountries" element={<AdminAddAdmitCountries />} />
						<Route path="/admin/activities/create" element={<AdminCreateActivity />} />
						<Route path="/admin/activities/list" element={<AdminListActivities />} />
					</Route>

					<Route element={<WithAdminNav />}>
						<Route path="/employee" element={<EmployeeHome />} />
						<Route path="/employee/checkQr" element={<EmployeeQrChecker />} />
					</Route>

					<Route path="/test" element={<TestView />} />

				</Routes>

			</AppContext.Provider>
		</>
	)
}