## Frontend

Para correr el front debe primero que nada verificar el archivo index.js de la carpeta src/config, veremos algo como esto
```
module.exports = {
    API_URL: 'http://localhost:3001'
}
```
la uri debe ser la de la API del proyecto

Bien ya tenemos todo listo, vamos al directorio donde queremos almacenar nuestro proyecto y hacemos (Si no tienes llaves ssh puedes revisar [Configurar llaves ssh](https://www.notion.so/Configurar-llaves-SSH-f0b331af74ac491dab58f2af2a5b3c8e))
```
git clone git@gitlab.com:brian-dev/frontaaoc.git
cd frontaaoc
npm i
npm start
```
Listo! ya tienes el front corriendo!